FROM openjdk:8-jre
MAINTAINER seykron <seykron@rlab.be>

RUN useradd \
        --user-group \
        --no-create-home \
        --uid 1000 \
        --shell /bin/false \
        app

ARG JAR_FILE
# Add the service itself
ADD target/$JAR_FILE /usr/share/domino/domino.jar

# Drop privs
USER app

ENTRYPOINT ["java", "-jar", "/usr/share/domino/domino.jar"]
