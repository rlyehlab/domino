package be.rlab.domino.domain.model

import java.util.*

data class TestAccount(
    val id: UUID = UUID.randomUUID(),
    val initialSignature: String = UUID.randomUUID().toString(),
    val group: String = "rlyeh",
    val owner: Agent = TestAgent().new(),
    val debt: Value = Value.ZERO,
    val default: Boolean = true
) {
    fun new(): Account = Account(
        id = id,
        initialSignature = initialSignature,
        group = group,
        owner = owner,
        debt = debt,
        default = default
    )
}
