package be.rlab.domino.domain.model

import be.rlab.domino.domain.model.BlockSignature.Companion.INITIAL_SIGNATURE
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*

data class TestBill(
    val id: UUID = UUID.randomUUID(),
    val chargedAccount: Account = TestAccount().new(),
    val description: String = "test bill",
    val credit: Credit = TestCredit().new(),
    val debt: Transaction = TestTransaction(type = TransactionType.DEBT).new(),
    val priority: Int = 100,
    val dueDate: DateTime? = DateTime.now().withZone(DateTimeZone.UTC).plusDays(28),
    val pending: Boolean = true
) {
    fun new(): Bill = Bill.new(
        previousSignature = INITIAL_SIGNATURE,
        chargedAccount = chargedAccount,
        description = description,
        credit = credit,
        debt = debt,
        priority = priority,
        dueDate = dueDate,
        pending = pending
    )
}
