package be.rlab.domino.domain.model

import be.rlab.domino.util.SignatureUtils.buildSignature
import be.rlab.domino.util.SignatureUtils.calculateSignature
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*

data class TestActivity(
    override val id: UUID,
    override val signature: BlockSignature,
    override val creationDate: DateTime,
    override val account: Account,
    override val description: String
) : Activity {

    companion object {
        fun new(
            previousSignature: String,
            account: Account,
            description: String
        ): TestActivity = new(
            previousSignature = previousSignature,
            creationDate = DateTime.now().withZone(DateTimeZone.UTC),
            account = account,
            description = description
        )

        fun new(
            previousSignature: String,
            creationDate: DateTime,
            account: Account,
            description: String
        ): TestActivity {
            val id: UUID = UUID.randomUUID()

            return TestActivity(
                id = id,
                signature = calculateSignature(
                    previousSignature, id, creationDate, account.id, description
                ),
                creationDate = creationDate,
                account = account,
                description = description
            )
        }
    }

    override fun blockId(signatureVersion: Int): String = buildSignature(
        fields = listOf()
    )
}
