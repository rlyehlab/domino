package be.rlab.domino.domain.model

import be.rlab.domino.domain.model.BlockSignature.Companion.INITIAL_SIGNATURE
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

data class TestCredit(
    val account: Account = TestAccount().new(),
    val description: String = "test credit",
    val dueDate: DateTime? = DateTime.now().withZone(DateTimeZone.UTC).plusDays(45),
    val debtor: Agent = TestAgent().new(),
    val income: Transaction = TestTransaction().new(),
    val pending: Boolean = true
) {
    fun new(): Credit = Credit.new(
        previousSignature = INITIAL_SIGNATURE,
        account = account,
        description = description,
        dueDate = dueDate,
        debtor = debtor,
        income = income,
        pending = pending
    )
}
