package be.rlab.domino.domain.model

import be.rlab.domino.domain.model.BlockSignature.Companion.INITIAL_SIGNATURE
import be.rlab.domino.domain.model.Value.Companion.ZERO

data class TestTransaction(
    val account: Account = TestAccount().new(),
    val description: String = "test transaction",
    val type: TransactionType = TransactionType.DEPOSIT,
    val ack: Boolean = false,
    val value: Value = ZERO
) {
    fun new(): Transaction = Transaction.new(
        previousSignature = INITIAL_SIGNATURE,
        account = account,
        description = description,
        transactionType = type,
        value = value
    )
}
