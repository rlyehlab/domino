package be.rlab.domino.domain.model

import java.util.*

data class TestAgent(
    val id: UUID = UUID.randomUUID(),
    val priority: Int = 10,
    val userId: Long = 1234,
    val userName: String = "pampa",
    val fullName: String? = "Herminio Lopez"
) {
    fun new(): Agent = Agent(
        id = id,
        priority = priority,
        userName = userName,
        fullName = fullName,
        userId = userId
    )
}
