package be.rlab.domino.domain.model

import be.rlab.domino.domain.model.BlockSignature.Companion.INITIAL_SIGNATURE
import org.joda.time.DateTime

data class TestBalance(
    val account: Account = TestAccount().new(),
    val description: String = "Balance del mes",
    val from: DateTime = DateTime.now().minusDays(60),
    val to: DateTime = DateTime.now().minusDays(30),
    val transactions: List<Transaction> = emptyList(),
    val bills: List<Bill> = emptyList(),
    val previousBalance: Value = Value.ZERO,
    val debtsTotal: Value = Value.ZERO,
    val debtsPending: Value = Value.ZERO,
    val income: Value = Value.ZERO,
    val monthlyBalance: Value = Value.ZERO,
    val totalBalance: Value = Value.ZERO
) {
    fun new(): Balance = Balance.new(
        previousSignature = INITIAL_SIGNATURE,
        account = account,
        description = description,
        from = from,
        to = to,
        transactions = transactions,
        bills = bills,
        previousBalance = previousBalance,
        debtsTotal = debtsTotal,
        debtsPending = debtsPending,
        income = income,
        monthlyBalance = monthlyBalance,
        totalBalance = totalBalance
    )
}
