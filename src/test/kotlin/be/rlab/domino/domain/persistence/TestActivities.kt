package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.TestActivity
import be.rlab.domino.util.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.EntityID
import java.util.*

object TestActivities : Activities("test_treasury_activities")

class TestActivityEntity(id: EntityID<UUID>) : ActivityEntity<TestActivity>(
    table = TestActivities,
    id = id,
    type = TestActivity::class
) {
    companion object : AbstractEntityClass<TestActivity, TestActivityEntity>(TestActivities)

    override fun initEntity(
        entity: TestActivity
    ): TestActivity = entity.copy(
        account = account.toDomainType()
    )
}
