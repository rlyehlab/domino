package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.model.Value.Companion.valueOf
import be.rlab.domino.util.ActivityDAOTestSupport
import be.rlab.domino.util.TestDataSource.initTransaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.Test

class BillDAOTest : ActivityDAOTestSupport() {

    private val transactionDAO: TransactionDAO by lazy {
        initTransaction(TransactionDAO())
    }
    private val creditDAO: CreditDAO by lazy {
        initTransaction(CreditDAO())
    }
    private val billDAO: BillDAO by lazy {
        initTransaction(BillDAO())
    }

    @Test
    fun saveOrUpdate() {
        val chargedAccount: Account = createAccount("rlyeh")
        val creditorAccount: Account = createAccount("edesur")
        val debt: Transaction = transactionDAO.saveOrUpdate(Transaction.debt(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = chargedAccount,
            description = "factura de servicios",
            value = valueOf(Currency.ARS, 100.0)
        ))
        val credit: Transaction = transactionDAO.saveOrUpdate(Transaction.credit(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = creditorAccount,
            description = "crédito",
            value = valueOf(Currency.ARS, 100.0)
        ))

        val bill: Bill = Bill.unpaid(
            chargedAccount = chargedAccount,
            description = "factura de luz",
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            credit = creditDAO.saveOrUpdate(Credit.new(
                previousSignature = BlockSignature.INITIAL_SIGNATURE,
                account = creditorAccount,
                description = "depósito",
                debtor = chargedAccount.owner,
                income = credit,
                dueDate = DateTime.now().withZone(DateTimeZone.UTC)
            )),
            debt = debt,
            priority = 10,
            dueDate = DateTime.now().plusDays(20).withZone(DateTimeZone.UTC)
        )

        assert(billDAO.saveOrUpdate(bill) == bill)
    }

    @Test
    fun listPending() {
        val account: Account = createAccount()
        val creditorAccount: Account = createAccount()
        val bill1: Bill = billDAO.saveOrUpdate(TestBill(
            chargedAccount = account,
            credit = TestCredit(
                account = creditorAccount
            ).new(),
            pending = false
        ).new())
        val bill2: Bill = billDAO.saveOrUpdate(TestBill(
            chargedAccount = account,
            credit = TestCredit(
                account = creditorAccount
            ).new(),
            pending = true
        ).new())
        val bills: List<Bill> = billDAO.listPending(account.id)
        assert(bills.size == 1)
        assert(bills[0] == bill2)
    }

    @Test
    fun listPending_byAgent() {
        val chargedAccount: Account = createAccount()
        val creditorAccount1: Account = createAccount()
        val creditorAccount2: Account = createAccount()
        val bill1: Bill = billDAO.saveOrUpdate(TestBill(
            chargedAccount = chargedAccount,
            credit = creditDAO.saveOrUpdate(TestCredit(
                account = creditorAccount1
            ).new()),
            pending = true
        ).new())
        val bill2: Bill = billDAO.saveOrUpdate(TestBill(
            chargedAccount = chargedAccount,
            credit = creditDAO.saveOrUpdate(TestCredit(
                account = creditorAccount2
            ).new()),
            pending = true
        ).new())
        val bills1: List<Bill> = billDAO.listPending(chargedAccount.id, creditorAccount1.owner.id)
        assert(bills1.size == 1)
        assert(bills1[0] == bill1)

        val bills2: List<Bill> = billDAO.listPending(chargedAccount.id, creditorAccount2.owner.id)
        assert(bills2.size == 1)
        assert(bills2[0] == bill2)
    }

    @Test
    fun payBill() {
        val chargedAccount: Account = createAccount("rlyeh")
        val creditorAccount: Account = createAccount("edesur")
        val debt: Transaction = transactionDAO.saveOrUpdate(Transaction.debt(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = chargedAccount,
            description = "factura de servicios",
            value = valueOf(Currency.ARS, 100.0)
        ))
        val credit: Transaction = transactionDAO.saveOrUpdate(Transaction.credit(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = creditorAccount,
            description = "crédito",
            value = valueOf(Currency.ARS, 100.0)
        ))

        val bill: Bill = Bill.unpaid(
            chargedAccount = chargedAccount,
            description = "factura de luz",
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            credit = creditDAO.saveOrUpdate(Credit.new(
                previousSignature = BlockSignature.INITIAL_SIGNATURE,
                account = creditorAccount,
                description = "depósito",
                debtor = chargedAccount.owner,
                income = credit,
                dueDate = DateTime.now().withZone(DateTimeZone.UTC)
            )),
            debt = debt,
            priority = 10,
            dueDate = DateTime.now().plusDays(20).withZone(DateTimeZone.UTC)
        )

        val paidBill: Bill = bill.paid(bill.debt.value)
        billDAO.saveOrUpdate(paidBill)

        val result: Bill = billDAO.findById(bill.id)
        assert(!result.pending)
    }
}
