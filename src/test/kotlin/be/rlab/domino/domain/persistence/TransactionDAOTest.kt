package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.*
import be.rlab.domino.util.ActivityDAOTestSupport
import be.rlab.domino.util.TestDataSource.initTransaction
import be.rlab.domino.util.TestDataSource.transaction
import org.jetbrains.exposed.sql.deleteAll
import org.joda.time.DateTime
import org.junit.Test
import java.math.BigDecimal

class TransactionDAOTest : ActivityDAOTestSupport() {

    private val transactionDAO: TransactionDAO by lazy {
        initTransaction(TransactionDAO())
    }

    @Test
    fun saveOrUpdate_deposit() {
        val deposit: Transaction = Transaction.deposit(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = createAccount("rlyeh"),
            description = "depósito",
            value = Value.ZERO
        )
        assert(transactionDAO.saveOrUpdate(deposit) == deposit)
    }

    @Test
    fun saveOrUpdate_transfer() {
        val transfer: Transaction = Transaction.transfer(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = createAccount("rlyeh"),
            description = "transferencia",
            value = Value.ZERO
        )
        assert(transactionDAO.saveOrUpdate(transfer) == transfer)
    }

    @Test
    fun saveOrUpdate_debt() {
        val debt: Transaction = Transaction.debt(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = createAccount("rlyeh"),
            description = "nueva factura",
            value = Value.ZERO
        )
        assert(transactionDAO.saveOrUpdate(debt) == debt)
    }

    @Test
    fun saveOrUpdate_credit() {
        val creditorAccount: Account = createAccount("aysa")
        val credit: Transaction = Transaction.credit(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = creditorAccount,
            description = "nueva factura",
            value = Value(Currency.ARS, BigDecimal(100))
        )
        assert(transactionDAO.saveOrUpdate(credit) == credit)
    }

    @Test
    fun invalidate() {
        val debt: Transaction = Transaction.debt(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = createAccount("rlyeh"),
            description = "nueva factura",
            value = Value.ZERO
        )
        assert(transactionDAO.saveOrUpdate(debt) == debt)
        assert(transactionDAO.saveOrUpdate(debt.invalidate()) == debt.invalidate())
    }

    @Test
    fun findValidByPeriod() = transaction {
        Transactions.deleteAll()

        val debt1: Transaction = Transaction.debt(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = createAccount("rlyeh"),
            description = "nueva factura 1",
            value = Value.ZERO
        ).invalidate()
        val debt2: Transaction = Transaction.debt(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = createAccount("rlyeh"),
            description = "nueva factura 2",
            value = Value.ZERO
        )

        transactionDAO.saveOrUpdate(debt1)
        transactionDAO.saveOrUpdate(debt2)

        val transactions: List<Transaction> = transactionDAO.findValidByPeriod(
            group = "rlyeh",
            from = DateTime.now().minusDays(1),
            to = DateTime.now()
        )
        assert(transactions.size == 1)
        assert(transactions[0] == debt2)
    }
}
