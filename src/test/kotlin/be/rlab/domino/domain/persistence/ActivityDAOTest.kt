package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.*
import be.rlab.domino.util.TestDataSource.initTransaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.Test

class ActivityDAOTest {

    private val accountDAO: AccountDAO by lazy {
        initTransaction(AccountDAO())
    }
    private val agentDAO: AgentDAO by lazy {
        initTransaction(AgentDAO())
    }
    private val activityDAO: ActivityDAO<TestActivity, TestActivityEntity> by lazy {
        initTransaction(object : ActivityDAO<TestActivity, TestActivityEntity>(
            table = TestActivities,
            entity = TestActivityEntity
        ){})
    }
    private val blockSignatureDAO: BlockSignatureDAO by lazy {
        initTransaction(BlockSignatureDAO())
    }

    @Test
    fun saveOrUpdate() {
        val activity: TestActivity = TestActivity.new(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = createAccount(),
            description = "depósito"
        )
        assert(activityDAO.saveOrUpdate(activity) == activity)
    }

    @Test
    fun lastSignature() {
        val account: Account = createAccount()

        val deposit1: TestActivity = activityDAO.saveOrUpdate(TestActivity.new(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = account,
            description = "depósito 1"
        ))
        val deposit2: TestActivity = activityDAO.saveOrUpdate(TestActivity.new(
            previousSignature = blockSignatureDAO.lastSignature(account.id),
            account = account,
            description = "depósito 2"
        ))
        val deposit3: TestActivity = activityDAO.saveOrUpdate(TestActivity.new(
            previousSignature = blockSignatureDAO.lastSignature(account.id),
            account = account,
            description = "depósito 3"
        ))
        val deposit4: TestActivity = activityDAO.saveOrUpdate(TestActivity.new(
            previousSignature = blockSignatureDAO.lastSignature(account.id),
            account = account,
            description = "depósito 4"
        ))

        assert(deposit1.signature.previous == BlockSignature.INITIAL_SIGNATURE)
        assert(deposit2.signature.previous == deposit1.signature.signature)
        assert(deposit3.signature.previous == deposit2.signature.signature)
        assert(deposit4.signature.previous == deposit3.signature.signature)
    }

    @Test
    fun findById() {
        val deposit: TestActivity = activityDAO.saveOrUpdate(TestActivity.new(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = createAccount(),
            description = "depósito"
        ))
        activityDAO.saveOrUpdate(deposit)
        assert(activityDAO.findById(deposit.id) == deposit)
    }

    @Test
    fun findByPeriod_account() {
        val account: Account = createAccount()
        val referenceDate: DateTime = DateTime.now().withZone(DateTimeZone.UTC)
        val deposit: TestActivity = activityDAO.saveOrUpdate(TestActivity.new(
            creationDate = referenceDate,
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = account,
            description = "depósito nuevo"
        ))

        activityDAO.saveOrUpdate(TestActivity.new(
            creationDate = deposit.creationDate.minusMonths(1),
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = account,
            description = "depósito viejo"
        ))

        val activities: List<TestActivity> = activityDAO.findByPeriod("rlyeh", referenceDate, referenceDate.plusDays(1))
        assert(activities.size == 1)
        assert(activities[0] == deposit)
    }

    @Test
    fun findByPeriod_group() {
        val account1: Account = createAccount()
        val account2: Account = createAccount()
        val deposit1: TestActivity = activityDAO.saveOrUpdate(TestActivity.new(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = account1,
            description = "depósito nuevo"
        ))
        val deposit2: TestActivity = activityDAO.saveOrUpdate(TestActivity.new(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = account2,
            description = "depósito viejo"
        ))

        val activities: List<TestActivity> = activityDAO.findByPeriod(
            "rlyeh", deposit1.creationDate.minusDays(2), deposit1.creationDate.plusDays(10))
        assert(activities.size == 2)
        assert(activities[0] == deposit1)
        assert(activities[1] == deposit2)
    }

    private fun createAccount(): Account {
        val agent: Agent = agentDAO.saveOrUpdate(TestAgent().new())

        return accountDAO.saveOrUpdate(TestAccount(
            group = "rlyeh",
            owner = agent
        ).new())
    }
}
