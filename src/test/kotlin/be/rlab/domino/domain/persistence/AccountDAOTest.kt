package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.model.Value.Companion.valueOf
import be.rlab.domino.util.TestDataSource.initTransaction
import org.junit.Test

class AccountDAOTest {

    private val accountDAO: AccountDAO by lazy {
        initTransaction(AccountDAO())
    }
    private val agentDAO: AgentDAO by lazy {
        initTransaction(AgentDAO())
    }

    @Test
    fun findDefaultByAgent() {
        val agent: Agent = TestAgent().new()
        agentDAO.saveOrUpdate(agent)

        val account: Account = TestAccount(
            owner = agent
        ).new()

        accountDAO.saveOrUpdate(account)
        assert(accountDAO.findDefaultByAgent(agent.id) ==
                account.copy(debt = valueOf(Currency.ARS, 0.000)))
    }

    @Test
    fun findByAgent() {
        val agent: Agent = TestAgent().new()
        agentDAO.saveOrUpdate(agent)

        val account1: Account = TestAccount(
            owner = agent
        ).new()
        val account2: Account = TestAccount(
            owner = agent
        ).new()

        accountDAO.saveOrUpdate(account1)
        accountDAO.saveOrUpdate(account2)
        val accounts: List<Account> = accountDAO.findByAgent(agent.id)

        assert(accounts.size == 2)
        assert(accounts[0] == account1)
        assert(accounts[1] == account2)
    }

    @Test
    fun findById() {
        val agent: Agent = TestAgent().new()
        agentDAO.saveOrUpdate(agent)

        val account: Account = TestAccount(
            owner = agent
        ).new()

        accountDAO.saveOrUpdate(account)
        assert(accountDAO.findById(account.id) == account)
    }

    @Test
    fun findByGroup() {
        val agent: Agent = TestAgent().new()
        agentDAO.saveOrUpdate(agent)

        val account: Account = TestAccount(
            owner = agent,
            group = "infra"
        ).new()

        accountDAO.saveOrUpdate(account)
        assert(accountDAO.findByGroup("infra").single() == account)
    }
}
