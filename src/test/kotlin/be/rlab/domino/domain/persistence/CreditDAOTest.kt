package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.model.Value.Companion.valueOf
import be.rlab.domino.util.ActivityDAOTestSupport
import be.rlab.domino.util.TestDataSource.initTransaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.Test

class CreditDAOTest : ActivityDAOTestSupport() {

    private val transactionDAO: TransactionDAO by lazy {
        initTransaction(TransactionDAO())
    }
    private val creditDAO: CreditDAO by lazy {
        initTransaction(CreditDAO())
    }

    @Test
    fun saveOrUpdate() {
        val debtor: Agent = agentDAO.saveOrUpdate(TestAgent().new())
        val account: Account = createAccount("rlyeh")
        val income: Transaction = transactionDAO.saveOrUpdate(
            Transaction.credit(
                previousSignature = BlockSignature.INITIAL_SIGNATURE,
                account = account,
                description = "depósito",
                value = valueOf(Currency.ARS, 100.0)
            )
        )
        val credit: Credit = Credit.new(
            previousSignature = BlockSignature.INITIAL_SIGNATURE,
            account = account,
            description = "depósito",
            debtor = debtor,
            income = income,
            dueDate = DateTime.now().withZone(DateTimeZone.UTC)
        )
        assert(creditDAO.saveOrUpdate(credit) == credit)
    }
}
