package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.model.Value.Companion.valueOf
import be.rlab.domino.util.ActivityDAOTestSupport
import be.rlab.domino.util.TestDataSource.initTransaction
import org.junit.Test

class CreditNoteDAOTest : ActivityDAOTestSupport() {

    private val transactionDAO: TransactionDAO by lazy {
        initTransaction(TransactionDAO())
    }
    private val creditNoteDAO: CreditNoteDAO by lazy {
        initTransaction(CreditNoteDAO())
    }

    @Test
    fun saveOrUpdate() {
        val account: Account = createAccount("rlyeh")
        val debt: Transaction = transactionDAO.saveOrUpdate(
            Transaction.debt(
                previousSignature = BlockSignature.INITIAL_SIGNATURE,
                account = account,
                description = "factura de luz",
                value = valueOf(Currency.ARS, 100.0)
            )
        )
        val creditNote: CreditNote = CreditNote.new(
            description = "depósito",
            transaction = debt
        )
        assert(creditNoteDAO.saveOrUpdate(creditNote) == creditNote)
    }
}
