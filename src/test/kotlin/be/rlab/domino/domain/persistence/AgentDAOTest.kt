package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Agent
import be.rlab.domino.domain.model.TestAgent
import be.rlab.domino.util.TestDataSource.initTransaction
import org.junit.Test

class AgentDAOTest {

    private val agentDAO: AgentDAO by lazy {
        initTransaction(AgentDAO())
    }

    @Test
    fun findById() {
        val agent: Agent = TestAgent().new()
        agentDAO.saveOrUpdate(agent)
        assert(agentDAO.findById(agent.id) == agent)
    }

    @Test
    fun findByUserId() {
        val agent: Agent = TestAgent(
            userId = 1337
        ).new()
        agentDAO.saveOrUpdate(agent)
        assert(agentDAO.findByUserId(1337) == agent)
    }

    @Test
    fun findByUserName() {
        val agent: Agent = TestAgent(
            userName = "romeo1"
        ).new()
        agentDAO.saveOrUpdate(agent)
        assert(agentDAO.findByUserName("romeo1") == agent)
    }

    @Test
    fun exists_userId() {
        val agent: Agent = TestAgent(
            userName = "romeo2",
            userId = 1338
        ).new()
        agentDAO.saveOrUpdate(agent)
        assert(agentDAO.exists(1338))
    }

    @Test
    fun exists_userName() {
        val agent: Agent = TestAgent(
            userName = "romeo3",
            userId = 1338
        ).new()
        agentDAO.saveOrUpdate(agent)
        assert(agentDAO.exists("romeo3"))
    }

    @Test
    fun findByPriority() {
        val agent: Agent = TestAgent(
            userName = "romeo4",
            priority = 348
        ).new()
        agentDAO.saveOrUpdate(agent)
        assert(agentDAO.findByPriority(348).single() == agent)
    }
}
