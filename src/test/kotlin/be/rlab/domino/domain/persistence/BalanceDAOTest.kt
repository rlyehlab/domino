package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.model.BlockSignature.Companion.INITIAL_SIGNATURE
import be.rlab.domino.util.ActivityDAOTestSupport
import be.rlab.domino.util.TestDataSource.initTransaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.Test

class BalanceDAOTest : ActivityDAOTestSupport() {

    private val balanceDAO: BalanceDAO by lazy {
        initTransaction(BalanceDAO())
    }

    @Test
    fun saveOrUpdate() {
        val account: Account = createAccount("rlyeh")
        val from = DateTime(2019, 6, 1, 0, 0, DateTimeZone.UTC)
        val to = from.plusMonths(1)
        val transaction: Transaction = TestTransaction().new()
        val bill: Bill = TestBill().new()
        val balance: Balance = Balance.new(
            previousSignature = INITIAL_SIGNATURE,
            account = account,
            description = "test balance",
            from = from,
            to = to,
            transactions = listOf(transaction),
            bills = listOf(bill),
            previousBalance = Value.ZERO,
            debtsTotal = Value.ZERO,
            debtsPending = Value.ZERO,
            income = Value.ZERO,
            monthlyBalance = Value.ZERO,
            totalBalance = Value.ZERO
        )

        assert(balanceDAO.saveOrUpdate(balance) == balance)
    }
}
