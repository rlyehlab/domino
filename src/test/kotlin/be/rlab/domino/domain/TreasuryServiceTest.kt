package be.rlab.domino.domain

import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.model.Value.Companion.valueOf
import be.rlab.domino.util.TestDataSource.initTransaction
import be.rlab.domino.util.mock.TestAccountService
import be.rlab.domino.util.mock.TestBillDAO
import be.rlab.domino.util.mock.TestBlockSignatureDAO
import be.rlab.domino.util.mock.TestTransactionService
import com.nhaarman.mockitokotlin2.mock
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.Test

class TreasuryServiceTest {

    private val accountService: TestAccountService = TestAccountService()
    private val transactionService: TestTransactionService = TestTransactionService()
    private val billDAO: TestBillDAO = TestBillDAO()
    private val blockSignatureDAO: TestBlockSignatureDAO = TestBlockSignatureDAO()

    @Test
    fun newDonation() {
        val donor: Agent = TestAgent().new()
        val receiver: Agent = TestAgent().new()
        val treasuryAccount: Account = TestAccount().new()
        val sourceAccount: Account = TestAccount().new()
        val destinationAccount: Account = TestAccount().new()

        val service = initTransaction(TreasuryService(
            signatureDAO = blockSignatureDAO.instance,
            billDAO = billDAO.instance,
            accountService = accountService
                .findTreasuryAccount(treasuryAccount)
                .findDefaultByAgent(donor.id, sourceAccount)
                .findDefaultByAgent(receiver.id, destinationAccount)
                .instance,
            transactionService = transactionService
                .newDeposit(sourceAccount, "Pago de bono", valueOf(Currency.ARS, 60.0), mock())
                .newTransfer(
                    sourceAccount, treasuryAccount, "Transferencia de bono", valueOf(Currency.ARS, 60.0), mock()
                )
                .newTransfer(
                    treasuryAccount, destinationAccount, "Transferencia de bono", valueOf(Currency.ARS, 60.0), mock()
                )
                .instance
        ))
        service.newDonation(
            donor = donor,
            receiver = receiver,
            value = valueOf(Currency.ARS, 60.0)
        )
    }

    @Test
    fun newTransfer() {
        val source: Agent = TestAgent().new()
        val destination: Agent = TestAgent().new()
        val sourceAccount: Account = TestAccount().new()
        val destinationAccount: Account = TestAccount().new()

        val service = initTransaction(TreasuryService(
            signatureDAO = blockSignatureDAO.instance,
            billDAO = billDAO.instance,
            accountService = accountService
                .findDefaultByAgent(source.id, sourceAccount)
                .findDefaultByAgent(destination.id, destinationAccount)
                .instance,
            transactionService = transactionService
                .newTransfer(
                    sourceAccount, destinationAccount, "Transferencia entre cuentas", valueOf(Currency.ARS, 60.0), mock()
                )
                .instance
        ))
        service.newTransfer(
            source = source,
            destination = destination,
            value = valueOf(Currency.ARS, 60.0)
        )
    }

    @Test
    fun newDeposit() {
        val target: Agent = TestAgent().new()
        val targetAccount: Account = TestAccount().new()

        val service = initTransaction(TreasuryService(
            signatureDAO = blockSignatureDAO.instance,
            billDAO = billDAO.instance,
            accountService = accountService
                .findDefaultByAgent(target.id, targetAccount)
                .instance,
            transactionService = transactionService
                .newDeposit(
                    targetAccount, "Regalo de Batman", valueOf(Currency.ARS, 60.0), mock()
                )
                .instance
        ))
        service.newDeposit(
            target = target,
            description = "Regalo de Batman",
            value = valueOf(Currency.ARS, 60.0)
        )
        transactionService.verifyAll()
    }

    @Test
    fun newBill() {
        val creditor: Agent = TestAgent().new()
        val chargedAccount: Account = TestAccount().new()
        val creditorAccount: Account = TestAccount().new()
        val newCredit: Credit = TestCredit().new()
        val newDebt: Transaction = TestTransaction().new()
        val dueDate: DateTime = DateTime.now().plusDays(28).withZone(DateTimeZone.UTC)
        val newBill: Bill = mock()

        val service = initTransaction(TreasuryService(
            signatureDAO = blockSignatureDAO
                .lastSignature(chargedAccount.id, "test1")
                .lastSignature(creditorAccount.id, "test2")
                .instance,
            billDAO = billDAO
                .saveOrUpdate(newBill)
                .instance,
            accountService = accountService
                .findTreasuryAccount(chargedAccount)
                .findDefaultByAgent(creditor.id, creditorAccount)
                .instance,
            transactionService = transactionService
                .newDebt(
                    chargedAccount,
                    "Nueva factura de: ${creditor.fullName}",
                    valueOf(Currency.ARS, 60.0), newDebt
                )
                .newCredit(
                    account = creditorAccount,
                    description = "Crédito por prestación de servicios",
                    debtor = chargedAccount.owner,
                    dueDate = dueDate,
                    total = valueOf(Currency.ARS, 60.0),
                    result = newCredit
                )
                .instance
        ))
        service.newBill(
            service = creditor,
            description = "factura de luz",
            value = valueOf(Currency.ARS, 60.0),
            dueDate = dueDate
        )

        billDAO.verifyAll()

        val capturedBill: Bill = billDAO.capturedValue("saveOrUpdate")
        assert(capturedBill.pending)
        assert(capturedBill.account == chargedAccount)
        assert(capturedBill.description == "factura de luz")
        assert(capturedBill.credit == newCredit)
        assert(capturedBill.debt == newDebt)
        assert(capturedBill.dueDate == dueDate)
        assert(capturedBill.priority == creditor.priority)
    }

    @Test
    fun newBill_replace() {
        val creditor: Agent = TestAgent().new()
        val chargedAccount: Account = TestAccount().new()
        val creditorAccount: Account = TestAccount(
            owner = creditor
        ).new()
        val newCredit: Credit = mock()
        val newDebt: Transaction = mock()
        val dueDate: DateTime = DateTime.now().plusDays(28).withZone(DateTimeZone.UTC)
        val newBill: Bill = mock()
        val pendingBill: Bill = TestBill(
            credit = TestCredit(
                account = creditorAccount
            ).new()
        ).new()

        val service = initTransaction(TreasuryService(
            signatureDAO = blockSignatureDAO
                .lastSignature(chargedAccount.id, "test1")
                .lastSignature(creditorAccount.id, "test2")
                .instance,
            billDAO = billDAO
                .listPending(chargedAccount.id, creditor.id, listOf(pendingBill))
                .saveOrUpdate(newBill)
                .instance,
            accountService = accountService
                .findTreasuryAccount(chargedAccount)
                .findDefaultByAgent(creditor.id, creditorAccount)
                .findById(pendingBill.debt.account.id, pendingBill.debt.account)
                .instance,
            transactionService = transactionService
                .invalidate(
                    transaction = pendingBill.debt,
                    reason = "Cancelación de factura ${pendingBill.id} del servicio: ${pendingBill.creditor().userName}",
                    result = pendingBill.debt.invalidate()
                )
                .invalidate(
                    transaction = pendingBill.credit.income,
                    reason = "Cancelación de factura ${pendingBill.id} del usuario ${chargedAccount.owner.userName}",
                    result = pendingBill.credit.income.invalidate()
                )
                .newDebt(
                    chargedAccount,
                    "Nueva factura de: ${creditor.fullName}",
                    valueOf(Currency.ARS, 60.0), newDebt
                )
                .newCredit(
                    account = creditorAccount,
                    description = "Crédito por prestación de servicios",
                    debtor = chargedAccount.owner,
                    dueDate = dueDate,
                    total = valueOf(Currency.ARS, 60.0),
                    result = newCredit
                )
                .instance
        ))
        service.newBill(
            service = creditor,
            description = "factura de luz",
            value = valueOf(Currency.ARS, 60.0),
            dueDate = dueDate,
            replace = true
        )

        billDAO.verifyAll()
        transactionService.verifyAll()

        val capturedCancelledBill: Bill = billDAO.capturedValues<Bill>("saveOrUpdate")[0]
        assert(capturedCancelledBill == pendingBill.invalidate(
            invalidDebt = pendingBill.debt.invalidate(),
            invalidCredit = pendingBill.credit.invalidate(pendingBill.credit.income.invalidate())
        ))

        val capturedBill: Bill = billDAO.capturedValues<Bill>("saveOrUpdate")[1]
        assert(capturedBill.pending)
        assert(capturedBill.account == chargedAccount)
        assert(capturedBill.description == "factura de luz")
        assert(capturedBill.credit == newCredit)
        assert(capturedBill.debt == newDebt)
        assert(capturedBill.dueDate == dueDate)
        assert(capturedBill.priority == creditor.priority)
    }

    @Test
    fun listPendingBills() {
        val bills: List<Bill> = emptyList()
        val treasuryAccount: Account = TestAccount().new()
        val service = initTransaction(TreasuryService(
            signatureDAO = mock(),
            billDAO = billDAO
                .listPending(treasuryAccount.id, bills)
                .instance,
            accountService = accountService
                .findTreasuryAccount(treasuryAccount)
                .instance,
            transactionService = transactionService.instance
        ))
        assert(service.listPendingBills() == bills)
    }

    @Test
    fun listBills() {
        val from = DateTime(2019, 6, 1, 0, 0, DateTimeZone.UTC)
        val to = from.plusMonths(1)
        val bills: List<Bill> = emptyList()
        val treasuryAccount: Account = TestAccount().new()
        val service = initTransaction(TreasuryService(
            signatureDAO = mock(),
            billDAO = billDAO
                .findByPeriod(treasuryAccount.id, from, to, bills)
                .instance,
            accountService = accountService
                .findTreasuryAccount(treasuryAccount)
                .instance,
            transactionService = transactionService.instance
        ))
        assert(service.listBills(from.year, from.monthOfYear) == bills)
    }

    @Test
    fun payBill() {
        val result: Bill = mock()
        val payerAccount: Account = TestAccount().new()
        val treasuryAccount: Account = TestAccount().new()
        val payer: Agent = TestAgent().new()
        val bill: Bill = TestBill().new()
        val paidBill: Bill = bill.paid(valueOf(Currency.ARS, 500.0))
        val service = initTransaction(TreasuryService(
            signatureDAO = mock(),
            billDAO = billDAO
                .saveOrUpdate(result)
                .instance,
            accountService = accountService
                .findDefaultByAgent(payer.id, payerAccount)
                .findTreasuryAccount(treasuryAccount)
                .instance,
            transactionService = transactionService
                .newTransfer(payerAccount, treasuryAccount,
                    "Transferencia en concepto de pago de factura: ${bill.creditor().fullName}",
                    valueOf(Currency.ARS, 500.0), mock())
                .instance
        ))
        assert(service.payBill(payer, bill, valueOf(Currency.ARS, 500.0)) == result)

        billDAO.verifyAll()
        transactionService.verifyAll()

        val savedBill: Bill = billDAO.capturedValue("saveOrUpdate")
        assert(bill.pending)
        assert(!savedBill.pending)
        assert(savedBill.copy(credit = paidBill.credit) == paidBill)
    }
}
