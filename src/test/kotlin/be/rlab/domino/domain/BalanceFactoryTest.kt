package be.rlab.domino.domain

import be.rlab.domino.application.model.BillDTO
import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.model.Value.Companion.valueOf
import be.rlab.domino.util.mock.TestBlockSignatureDAO
import org.joda.time.DateTime
import org.junit.Test

class BalanceFactoryTest {

    private val blockSignatureDAO: TestBlockSignatureDAO = TestBlockSignatureDAO()

    @Test
    fun create() {
        val previousBalance: Balance = TestBalance(
            transactions = emptyList(),
            bills = emptyList(),
            monthlyBalance = valueOf(Currency.ARS, 100.0)
        ).new()
        val transactions: List<Transaction> = listOf(
            debt(300.0), debt(150.0), income(500.0), income(200.0)
        )
        val bills: List<Bill> = listOf(
            TestBill(
                debt = debt(200.0).invalidate(),
                description = "factura de luz 1"
            ).new(),
            TestBill(
                debt = debt(300.0),
                credit = TestCredit(
                    account = TestAccount(owner = TestAgent(userName = "foo").new()).new(),
                    income = income(300.0)
                ).new().cancel(valueOf(Currency.ARS, 50.0)),
                description = "factura de luz 2",
                dueDate = DateTime.parse("2019-07-30")
            ).new(),
            TestBill(
                debt = debt(150.0),
                credit = TestCredit(
                    account = TestAccount(owner = TestAgent(userName = "bar").new()).new(),
                    income = income(150.0)
                ).new(),
                description = "factura de agua",
                dueDate = DateTime.parse("2019-07-28")
            ).new()
        )
        val account: Account = TestAccount().new()
        val factory = BalanceFactory(
            blockSignatureDAO = blockSignatureDAO
                .lastSignature(account.id, "signature")
                .instance
        )
        val to: DateTime = DateTime.now()
        val from: DateTime = to.minusMonths(1)
        val balance: Balance = factory.create(
            account = account,
            from = from,
            to = to,
            description = "test balance",
            previousBalance = previousBalance,
            transactions = transactions,
            bills = bills
        )

        assert(balance.previousBalance == valueOf(Currency.ARS, 100.0))
        assert(balance.monthlyBalance == valueOf(Currency.ARS, -250.0))
        assert(balance.debtsPending == valueOf(Currency.ARS, 400.0))
        assert(balance.debtsTotal == valueOf(Currency.ARS, 450.0))
        assert(balance.income == valueOf(Currency.ARS, -700.0))
        assert(balance.totalBalance == valueOf(Currency.ARS, -150.0))
        assert(balance.bills.size == 2)

        val bill1: BillDTO = BillDTO.from(balance.bills[0])
        val bill2: BillDTO = BillDTO.from(balance.bills[1])
        assert(bill1.pending)
        assert(bill1.debt == valueOf(Currency.ARS, 250.0))
        assert(bill1.dueDate == DateTime.parse("2019-07-30"))
        assert(bill1.total == valueOf(Currency.ARS, 300.0))
        assert(bill1.serviceName == "foo")
        assert(bill2.pending)
        assert(bill2.debt == valueOf(Currency.ARS, 150.0))
        assert(bill2.dueDate == DateTime.parse("2019-07-28"))
        assert(bill2.total == valueOf(Currency.ARS, 150.0))
        assert(bill2.serviceName == "bar")
    }

    private fun debt(value: Double): Transaction {
        return TestTransaction(
            type = TransactionType.DEBT,
            value = valueOf(Currency.ARS, value)
        ).new()
    }

    private fun income(value: Double): Transaction {
        return TestTransaction(
            type = TransactionType.DEPOSIT,
            value = valueOf(Currency.ARS, -value)
        ).new()
    }
}