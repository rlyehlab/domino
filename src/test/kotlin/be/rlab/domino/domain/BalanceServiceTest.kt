package be.rlab.domino.domain

import be.rlab.domino.domain.model.*
import be.rlab.domino.util.TestDataSource.initTransaction
import be.rlab.domino.util.mock.*
import com.nhaarman.mockitokotlin2.mock
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.Test

class BalanceServiceTest {

    private val transactionDAO: TestTransactionDAO = TestTransactionDAO()
    private val balanceDAO: TestBalanceDAO = TestBalanceDAO()
    private val billDAO: TestBillDAO = TestBillDAO()
    private val accountService: TestAccountService = TestAccountService()
    private val balanceFactory: TestBalanceFactory = TestBalanceFactory()

    @Test
    fun find_exists() {
        val from = DateTime(2019, 6, 1, 0, 0, DateTimeZone.UTC)
        val to = from.plusMonths(1)
        val account: Account = TestAccount().new()
        val balance: Balance = mock()
        val service = initTransaction(BalanceService(
            balanceDAO = balanceDAO
                .getByPeriod(account.id, from, to, balance)
                .instance,
            transactionDAO = transactionDAO.instance,
            billDAO = billDAO.instance,
            treasuryConfig = mock(),
            accountService = accountService.instance,
            balanceFactory = balanceFactory.instance
        ))
        assert(service.find(account, 2019, 6) == balance)
    }

    @Test
    fun find_createNew() {
        val from = DateTime(2019, 5, 1, 0, 0, DateTimeZone.UTC)
        val previousDate: DateTime = DateTime(2019, 5, 1, 0, 0, DateTimeZone.UTC).minusMonths(1)
        val to = from.plusMonths(1)
        val account: Account = TestAccount().new()
        val balance: Balance = TestBalance().new()
        val previousBalance: Balance = TestBalance().new()
        val bills: List<Bill> = emptyList()
        val transactions: List<Transaction> = emptyList()
        val service = initTransaction(BalanceService(
            balanceDAO = balanceDAO
                .getByPeriod(account.id, from, to, null)
                .getByPeriod(account.id, previousDate, previousDate.plusMonths(1), previousBalance)
                .saveOrUpdate(balance)
                .instance,
            transactionDAO = transactionDAO
                .findByPeriod(account.id, from, to, transactions)
                .instance,
            billDAO = billDAO
                .findByPeriod(account.id, from, to, bills)
                .instance,
            treasuryConfig = mock(),
            accountService = accountService.instance,
            balanceFactory = balanceFactory
                .create(account, from, to, "Balance del período 2019/5", previousBalance, transactions, bills, balance)
                .instance
        ))
        assert(service.find(account, 2019, 5) == balance)

        billDAO.verifyAll()
        transactionDAO.verifyAll()
        balanceDAO.verifyAll()

        assert(balance == balanceDAO.capturedValue("saveOrUpdate"))
    }

    @Test
    fun findForTreasury_exists() {
        val from = DateTime(2019, 5, 1, 0, 0, DateTimeZone.UTC)
        val to = from.plusMonths(1)
        val treasuryAccount: Account = TestAccount().new()
        val balance: Balance = mock()
        val bills: List<Bill> = emptyList()
        val transactions: List<Transaction> = emptyList()
        val service = initTransaction(BalanceService(
            balanceDAO = balanceDAO
                .getByPeriod(treasuryAccount.id, from, to, balance)
                .instance,
            transactionDAO = transactionDAO
                .findByPeriod("rlyeh", from, to, transactions)
                .instance,
            billDAO = billDAO
                .findByPeriod("rlyeh", from, to, bills)
                .instance,
            treasuryConfig = TreasuryConfig("rlyeh", 100),
            accountService = accountService
                .findTreasuryAccount(treasuryAccount)
                .instance,
            balanceFactory = balanceFactory.instance
        ))
        assert(service.findForTreasury(2019, 5) == balance)

        billDAO.verifyAll()
        transactionDAO.verifyAll()
        balanceDAO.verifyAll()
    }

    @Test
    fun findForTreasury_new() {
        val from = DateTime(2019, 5, 1, 0, 0, DateTimeZone.UTC)
        val previousDate: DateTime = DateTime(2019, 5, 1, 0, 0, DateTimeZone.UTC).minusMonths(1)
        val to = from.plusMonths(1)
        val treasuryAccount: Account = TestAccount().new()
        val balance: Balance = TestBalance().new()
        val previousBalance: Balance = TestBalance().new()
        val bills: List<Bill> = emptyList()
        val transactions: List<Transaction> = emptyList()
        val service = initTransaction(BalanceService(
            balanceDAO = balanceDAO
                .getByPeriod(treasuryAccount.id, from, to, null)
                .getByPeriod(treasuryAccount.id, previousDate, previousDate.plusMonths(1), previousBalance)
                .saveOrUpdate(balance)
                .instance,
            transactionDAO = transactionDAO
                .findByPeriod("rlyeh", from, to, transactions)
                .instance,
            billDAO = billDAO
                .findByPeriod("rlyeh", from, to, bills)
                .instance,
            treasuryConfig = TreasuryConfig("rlyeh", 100),
            accountService = accountService
                .findTreasuryAccount(treasuryAccount)
                .instance,
            balanceFactory = balanceFactory
                .create(treasuryAccount, from, to, "Balance del período 2019/5",
                    previousBalance, transactions, bills, balance
                )
                .instance
        ))
        assert(service.findForTreasury(2019, 5) == balance)

        billDAO.verifyAll()
        transactionDAO.verifyAll()
        balanceDAO.verifyAll()

        assert(balance == balanceDAO.capturedValue("saveOrUpdate"))
    }

    @Test
    fun findForTreasury_currentMonth() {
        val from = DateTime.now()
            .withZone(DateTimeZone.UTC)
            .withDayOfMonth(1)
            .withTimeAtStartOfDay()
        val previousDate: DateTime = DateTime(from.year, from.monthOfYear, 1, 0, 0, DateTimeZone.UTC)
            .minusMonths(1)
            .withTimeAtStartOfDay()
        val to = from.plusMonths(1)
        val treasuryAccount: Account = TestAccount().new()
        val bills: List<Bill> = emptyList()
        val transactions: List<Transaction> = emptyList()
        val previousBalance: Balance = TestBalance().new()
        val balance: Balance = TestBalance().new()
        val service = initTransaction(BalanceService(
            balanceDAO = balanceDAO
                .getByPeriod(treasuryAccount.id, from, to, null)
                .getByPeriod(treasuryAccount.id, previousDate, previousDate.plusMonths(1), previousBalance)
                .instance,
            transactionDAO = transactionDAO
                .findByPeriod("rlyeh", from, to, transactions)
                .instance,
            billDAO = billDAO
                .findByPeriod("rlyeh", from, to, bills)
                .instance,
            treasuryConfig = TreasuryConfig("rlyeh", 100),
            accountService = accountService
                .findTreasuryAccount(treasuryAccount)
                .instance,
            balanceFactory = balanceFactory
                .create(treasuryAccount, from, to, "Balance del período ${from.year}/${from.monthOfYear}",
                    previousBalance, transactions, bills, balance
                )
                .instance
        ))

        val result: Balance = service.findForTreasury(from.year, from.monthOfYear)
        billDAO.verifyAll()
        transactionDAO.verifyAll()
        balanceDAO.verifyAll()

        assert(result == balance)
    }
}