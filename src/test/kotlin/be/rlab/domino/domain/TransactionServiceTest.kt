package be.rlab.domino.domain

import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.model.Value.Companion.valueOf
import be.rlab.domino.util.TestDataSource.initTransaction
import be.rlab.domino.util.mock.*
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.joda.time.DateTime
import org.junit.Test

class TransactionServiceTest {

    private val accountDAO: TestAccountDAO = TestAccountDAO()
    private val transactionDAO: TestTransactionDAO = TestTransactionDAO()
    private val creditDAO: TestCreditDAO = TestCreditDAO()
    private val creditNoteDAO: TestCreditNoteDAO = TestCreditNoteDAO()
    private val blockSignatureDAO: TestBlockSignatureDAO = TestBlockSignatureDAO()

    @Test
    fun newDeposit() {
        val account: Account = TestAccount(
            debt = valueOf(Currency.ARS, 30.0)
        ).new()
        val result: Transaction = mock()

        val transactionService = initTransaction(TransactionService(
            accountDAO = accountDAO
                .findById(account.id, account)
                .saveOrUpdate(
                    account.income(valueOf(Currency.ARS, 50.0))
                )
                .instance,
            transactionDAO = transactionDAO
                .saveOrUpdate(result)
                .instance,
            creditDAO = creditDAO.instance,
            blockSignatureDAO = blockSignatureDAO
                .lastSignature(account.id, BlockSignature.INITIAL_SIGNATURE)
                .instance,
            creditNoteDAO = creditNoteDAO.instance
        ))

        val deposit: Transaction = transactionService.newDeposit(
            account = account,
            description = "testing deposit",
            value = valueOf(Currency.ARS, 50.0)
        )

        assert(deposit == result)

        transactionDAO.verifyAll()

        val capturedDeposit: Transaction = transactionDAO.capturedValue("saveOrUpdate")
        assert(capturedDeposit.value == valueOf(Currency.ARS, -50.0))
        assert(capturedDeposit.account.debt == valueOf(Currency.ARS, -20.0))
        assert(capturedDeposit.description == "testing deposit")
    }

    @Test
    fun newTransfer() {
        val sourceAccount: Account = TestAccount(
            debt = valueOf(Currency.ARS, -70.0)
        ).new()
        val destinationAccount: Account = TestAccount(
            debt = valueOf(Currency.ARS, 20.0),
            owner = TestAgent(
                fullName = "John Smith"
            ).new()
        ).new()
        val result: Transaction = mock()

        val transactionService = initTransaction(TransactionService(
            accountDAO = accountDAO
                .findById(sourceAccount.id, sourceAccount)
                .findById(destinationAccount.id, destinationAccount)
                .saveOrUpdate(
                    sourceAccount.outcome(valueOf(Currency.ARS, 50.0)),
                    destinationAccount.income(valueOf(Currency.ARS, 50.0))
                )
                .instance,
            transactionDAO = transactionDAO
                .saveOrUpdate(result)
                .instance,
            creditDAO = creditDAO.instance,
            blockSignatureDAO = blockSignatureDAO
                .lastSignature(sourceAccount.id, BlockSignature.INITIAL_SIGNATURE)
                .lastSignature(destinationAccount.id, BlockSignature.INITIAL_SIGNATURE)
                .instance,
            creditNoteDAO = creditNoteDAO.instance
        ))

        val transfer: Transaction = transactionService.newTransfer(
            sourceAccount = sourceAccount,
            destinationAccount = destinationAccount,
            description = "testing deposit",
            value = valueOf(Currency.ARS, 50.0)
        )

        assert(transfer == result)

        transactionDAO.verifyAll()

        val sourceTransfer: Transaction = transactionDAO.capturedValues<Transaction>("saveOrUpdate")[1]
        assert(sourceTransfer.value == valueOf(Currency.ARS, 50.0))
        assert(sourceTransfer.account.debt == valueOf(Currency.ARS, -20.0))
        assert(sourceTransfer.description == "Transferencia a John Smith: testing deposit")

        val destinationTransfer: Transaction = transactionDAO.capturedValues<Transaction>("saveOrUpdate")[0]
        assert(destinationTransfer.value == valueOf(Currency.ARS, -50.0))
        assert(destinationTransfer.account.debt == valueOf(Currency.ARS, -30.0))
        assert(destinationTransfer.description == "Transferencia de Herminio Lopez: testing deposit")
    }

    @Test
    fun newCredit() {
        val debtor: Agent = TestAgent(
            fullName = "John Smith"
        ).new()
        val account: Account = TestAccount(
            debt = valueOf(Currency.ARS, -10.0)
        ).new()
        val result: Credit = mock()
        val income: Transaction = mock {
            on { value } doReturn valueOf(Currency.ARS, -60.0)
        }

        val transactionService = initTransaction(TransactionService(
            accountDAO = accountDAO
                .findById(account.id, account)
                .saveOrUpdate(
                    account.income(valueOf(Currency.ARS, 60.0))
                )
                .instance,
            transactionDAO = transactionDAO
                .saveOrUpdate(income)
                .instance,
            creditDAO = creditDAO
                .saveOrUpdate(result)
                .instance,
            blockSignatureDAO = blockSignatureDAO
                .lastSignature(account.id, BlockSignature.INITIAL_SIGNATURE)
                .instance,
            creditNoteDAO = creditNoteDAO.instance
        ))

        val dueDate: DateTime = DateTime.now().plusDays(45)
        val credit: Credit = transactionService.newCredit(
            account = account,
            description = "testing credit",
            debtor = debtor,
            total = valueOf(Currency.ARS, 60.0),
            dueDate = dueDate
        )

        assert(credit == result)

        transactionDAO.verifyAll()
        creditDAO.verifyAll()

        val capturedIncome: Transaction = transactionDAO.capturedValue("saveOrUpdate")
        assert(capturedIncome.value == valueOf(Currency.ARS, -60.0))
        assert(capturedIncome.account.debt == valueOf(Currency.ARS, -70.0))
        assert(capturedIncome.description == "testing credit")

        val capturedCredit: Credit = creditDAO.capturedValue("saveOrUpdate")
        assert(capturedCredit.income == income)
        assert(capturedCredit.pending)
        assert(capturedCredit.debt == valueOf(Currency.ARS, 60.0))
        assert(capturedCredit.description == "testing credit")
    }

    @Test
    fun invalidate() {
        val debt: Transaction = TestTransaction().new()
        val result: Transaction = mock()
        val transactionService = initTransaction(TransactionService(
            accountDAO = accountDAO.instance,
            transactionDAO = transactionDAO
                .saveOrUpdate(result)
                .instance,
            creditDAO = creditDAO.instance,
            blockSignatureDAO = blockSignatureDAO.instance,
            creditNoteDAO = creditNoteDAO
                .saveOrUpdate()
                .instance
        ))

        assert(transactionService.invalidate(
            transaction = debt,
            reason = "cancelación de factura"
        ) == result)

        transactionDAO.verifyAll()
        creditNoteDAO.verifyAll()

        val savedCreditNote: CreditNote = creditNoteDAO.capturedValue("saveOrUpdate")
        assert(savedCreditNote.transaction == debt)
        assert(savedCreditNote.description == "cancelación de factura")

        val savedDebt: Transaction = transactionDAO.capturedValue("saveOrUpdate")
        assert(savedDebt == debt.invalidate())
    }
}
