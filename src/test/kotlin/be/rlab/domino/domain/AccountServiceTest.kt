package be.rlab.domino.domain

import be.rlab.domino.domain.model.*
import be.rlab.domino.util.SignatureUtils.buildSignature
import be.rlab.domino.util.TestDataSource.initTransaction
import be.rlab.domino.util.mock.TestAccountDAO
import be.rlab.domino.util.mock.TestAgentService
import be.rlab.domino.util.mock.TestTransactionDAO
import org.junit.Test
import java.util.*

class AccountServiceTest {

    private val agentService: TestAgentService = TestAgentService()
    private val accountDAO: TestAccountDAO = TestAccountDAO()
    private val transactionDAO: TestTransactionDAO = TestTransactionDAO()

    @Test
    fun findById() {
        val accountId: UUID = UUID.randomUUID()
        val account: Account = TestAccount().new()
        val accountService = AccountService(
            transactionDAO = transactionDAO.instance,
            accountDAO = accountDAO
                .findById(accountId, account)
                .instance,
            agentService = agentService.instance
        )

        assert(accountService.findById(accountId) == account)
    }

    @Test
    fun findDefaultByAgent() {
        val agentId: UUID = UUID.randomUUID()
        val account: Account = TestAccount().new()
        val accountService = AccountService(
            transactionDAO = transactionDAO.instance,
            accountDAO = accountDAO
                .findDefaultByAgent(agentId, account)
                .instance,
            agentService = agentService.instance
        )

        assert(accountService.findDefaultByAgent(agentId) == account)
    }

    @Test
    fun findDefaultByUserId() {
        val owner: Agent = TestAgent(
            userId = 1983
        ).new()
        val account: Account = TestAccount().new()
        val accountService = AccountService(
            transactionDAO = transactionDAO.instance,
            accountDAO = accountDAO
                .findDefaultByAgent(owner.id, account)
                .instance,
            agentService = agentService
                .findByUserId(1983, owner)
                .instance
        )

        assert(accountService.findDefaultByUserId(1983) == account)
    }

    @Test
    fun findByGroup() {
        val account: Account = TestAccount(
            group = "infra"
        ).new()
        val accountService = AccountService(
            transactionDAO = transactionDAO.instance,
            accountDAO = accountDAO
                .findByGroup("infra", account)
                .instance,
            agentService = agentService.instance
        )

        assert(accountService.findByGroup("infra").single() == account)
    }

    @Test
    fun findTreasuryAccount() {
        val account: Account = TestAccount().new()
        val owner: Agent = TestAgent().new()
        val accountService = AccountService(
            transactionDAO = transactionDAO.instance,
            accountDAO = accountDAO
                .findDefaultByAgent(owner.id, account)
                .instance,
            agentService = agentService
                .findByPriority(TreasuryConfig.TREASURY_PRIORITY, owner)
                .instance
        )

        assert(accountService.findTreasuryAccount() == account)
    }

    @Test
    fun create() {
        val owner: Agent = TestAgent(
            userId = 1478
        ).new()
        val treasuryOwner: Agent = TestAgent().new()
        val treasuryAccount: Account = TestAccount().new()
        val account: Account = TestAccount().new()
        val accountService = initTransaction(AccountService(
            transactionDAO = transactionDAO
                .saveOrUpdate()
                .instance,
            accountDAO = accountDAO
                .findDefaultByAgent(treasuryOwner.id, treasuryAccount)
                .saveOrUpdate(account)
                .instance,
            agentService = agentService
                .findByPriority(TreasuryConfig.TREASURY_PRIORITY, treasuryOwner)
                .findByUserId(1478, owner)
                .instance
        ))

        accountService.create(
            group = "rlyeh",
            userId = 1478
        )

        accountDAO.verifyAll()
        transactionDAO.verifyAll()

        val initialSignature: String = buildSignature(
            listOf(treasuryAccount.initialSignature, "rlyeh", 1478),
            hash = true
        )
        val capturedAccount: Account = accountDAO.capturedValue("saveOrUpdate")
        assert(capturedAccount.initialSignature == initialSignature)
        assert(capturedAccount.owner == owner)
        assert(capturedAccount.group == "rlyeh")
        assert(capturedAccount.default)

        val capturedTransaction: Transaction = transactionDAO.capturedValue("saveOrUpdate")
        assert(capturedTransaction.value == Value.ZERO)
        assert(capturedTransaction.type == TransactionType.DEPOSIT)
        assert(capturedTransaction.signature.previous == capturedAccount.initialSignature)
    }

    @Test
    fun createTreasuryAccount() {
        val owner: Agent = TestAgent(
            userId = 1478
        ).new()
        val account: Account = TestAccount().new()
        val accountService = initTransaction(AccountService(
            transactionDAO = transactionDAO
                .saveOrUpdate()
                .instance,
            accountDAO = accountDAO
                .saveOrUpdate(account)
                .instance,
            agentService = agentService
                .create(
                    priority = TreasuryConfig.TREASURY_PRIORITY,
                    userId = 1234,
                    userName = "cthulhu",
                    fullName = "Hacklab",
                    result = owner
                )
                .instance
        ))

        accountService.createTreasuryAccount(
            group = "rlyeh",
            userId = 1234,
            userName = "cthulhu",
            fullName = "Hacklab"
        )

        accountDAO.verifyAll()
        transactionDAO.verifyAll()

        val capturedAccount: Account = accountDAO.capturedValue("saveOrUpdate")
        assert(capturedAccount.owner == owner)
        assert(capturedAccount.group == "rlyeh")
        assert(capturedAccount.default)

        val capturedTransaction: Transaction = transactionDAO.capturedValue("saveOrUpdate")
        assert(capturedTransaction.value == Value.ZERO)
        assert(capturedTransaction.type == TransactionType.DEPOSIT)
        assert(capturedTransaction.signature.previous == capturedAccount.initialSignature)
    }
}
