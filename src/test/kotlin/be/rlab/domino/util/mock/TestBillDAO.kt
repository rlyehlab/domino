package be.rlab.domino.util.mock

import be.rlab.domino.domain.model.Bill
import be.rlab.domino.domain.persistence.BillDAO
import be.rlab.domino.util.VerifySupport
import com.nhaarman.mockitokotlin2.*
import org.joda.time.DateTime
import java.util.*

class TestBillDAO : VerifySupport() {

    val instance: BillDAO = mock()

    fun findById(
        billId: UUID,
        result: Bill
    ): TestBillDAO {
        whenever(instance.findById(billId)).thenReturn(result)
        return this
    }

    fun findByPeriod(
        accountId: UUID,
        from: DateTime,
        to: DateTime,
        result: List<Bill>
    ): TestBillDAO {
        whenever(instance.findByPeriod(accountId, from, to)).thenReturn(result)
        return this
    }

    fun findByPeriod(
        group: String,
        from: DateTime,
        to: DateTime,
        result: List<Bill>
    ): TestBillDAO {
        whenever(instance.findByPeriod(group, from, to)).thenReturn(result)
        return this
    }

    fun listPending(
        accountId: UUID,
        result: List<Bill>
    ): TestBillDAO {
        whenever(instance.listPending(accountId)).thenReturn(result)
        return this
    }

    fun listPending(
        accountId: UUID,
        agentId: UUID,
        result: List<Bill>
    ): TestBillDAO {
        whenever(instance.listPending(accountId, agentId)).thenReturn(result)
        return this
    }

    fun saveOrUpdate(bill: Bill): TestBillDAO {
        whenever(instance.saveOrUpdate(any())).thenReturn(bill)
        verify("saveOrUpdate") {
            val capturedTransaction = argumentCaptor<Bill>()
            verify(instance, atLeastOnce()).saveOrUpdate(capturedTransaction.capture())
            capturedTransaction.allValues
        }
        return this
    }
}
