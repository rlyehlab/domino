package be.rlab.domino.util.mock

import be.rlab.domino.domain.AgentService
import be.rlab.domino.domain.model.Agent
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever

class TestAgentService {

    val instance: AgentService = mock()

    fun findByPriority(
        priority: Int,
        vararg result: Agent
    ): TestAgentService {
        whenever(instance.findByPriority(priority)).thenReturn(result.toList())
        return this
    }

    fun findByUserId(
        userId: Long,
        result: Agent
    ): TestAgentService {
        whenever(instance.findByUserId(userId)).thenReturn(result)
        return this
    }

    fun create(
        priority: Int,
        userId: Long,
        userName: String,
        fullName: String?,
        result: Agent
    ): TestAgentService {
        whenever(instance.create(
            priority = priority,
            userName = userName,
            userId = userId,
            fullName = fullName
        )).thenReturn(result)

        return this
    }
}
