package be.rlab.domino.util.mock

import be.rlab.domino.domain.model.CreditNote
import be.rlab.domino.domain.persistence.CreditNoteDAO
import be.rlab.domino.util.VerifySupport
import com.nhaarman.mockitokotlin2.*
import java.util.*

class TestCreditNoteDAO : VerifySupport() {

    val instance: CreditNoteDAO = mock()

    fun findById(
        creditId: UUID,
        result: CreditNote
    ): TestCreditNoteDAO {
        whenever(instance.findById(creditId)).thenReturn(result)
        return this
    }

    fun saveOrUpdate(creditNote: CreditNote? = null): TestCreditNoteDAO {
        whenever(instance.saveOrUpdate(any())).thenReturn(creditNote)

        verify("saveOrUpdate") {
            val capturedTransaction = argumentCaptor<CreditNote>()
            verify(instance, atLeastOnce()).saveOrUpdate(capturedTransaction.capture())
            capturedTransaction.allValues
        }
        return this
    }
}
