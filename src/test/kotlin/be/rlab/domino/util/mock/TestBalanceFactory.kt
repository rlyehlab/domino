package be.rlab.domino.util.mock

import be.rlab.domino.domain.BalanceFactory
import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Balance
import be.rlab.domino.domain.model.Bill
import be.rlab.domino.domain.model.Transaction
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.joda.time.DateTime

class TestBalanceFactory {

    val instance: BalanceFactory = mock()


    fun create(
        account: Account,
        from: DateTime,
        to: DateTime,
        description: String,
        previousBalance: Balance?,
        transactions: List<Transaction>,
        bills: List<Bill>,
        result: Balance
    ): TestBalanceFactory {
        whenever(instance.create(
            account, from, to, description, previousBalance,
            transactions, bills
        )).thenReturn(result)
        return this
    }
}
