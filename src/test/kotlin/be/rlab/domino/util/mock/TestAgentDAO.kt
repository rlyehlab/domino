package be.rlab.domino.util.mock

import be.rlab.domino.domain.model.Agent
import be.rlab.domino.domain.persistence.AgentDAO
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever

class TestAgentDAO {

    val instance: AgentDAO = mock()

    fun findByPriority(
        priority: Int,
        vararg result: Agent
    ): TestAgentDAO {
        whenever(instance.findByPriority(priority)).thenReturn(result.toList())
        return this
    }
}
