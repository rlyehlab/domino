package be.rlab.domino.util.mock

import be.rlab.domino.domain.model.Balance
import be.rlab.domino.domain.persistence.BalanceDAO
import be.rlab.domino.util.VerifySupport
import com.nhaarman.mockitokotlin2.*
import org.joda.time.DateTime
import java.util.*

class TestBalanceDAO : VerifySupport() {

    val instance: BalanceDAO = mock()

    fun findById(
        balanceId: UUID,
        result: Balance
    ): TestBalanceDAO {
        whenever(instance.findById(balanceId)).thenReturn(result)
        return this
    }

    fun getByPeriod(
        accountId: UUID,
        from: DateTime,
        to: DateTime,
        result: Balance?
    ): TestBalanceDAO {
        whenever(instance.getByPeriod(accountId, from, to)).thenReturn(result)
        return this
    }

    fun saveOrUpdate(balance: Balance? = null): TestBalanceDAO {
        whenever(instance.saveOrUpdate(any())).thenReturn(balance)

        verify("saveOrUpdate") {
            val capturedBalance = argumentCaptor<Balance>()
            verify(instance, atLeastOnce()).saveOrUpdate(capturedBalance.capture())
            capturedBalance.allValues
        }

        return this
    }
}
