package be.rlab.domino.util.mock

import be.rlab.domino.util.VerifySupport
import be.rlab.domino.domain.model.Credit
import be.rlab.domino.domain.persistence.CreditDAO
import com.nhaarman.mockitokotlin2.*
import java.util.*

class TestCreditDAO : VerifySupport() {

    val instance: CreditDAO = mock()

    fun findById(
        creditId: UUID,
        result: Credit
    ): TestCreditDAO {
        whenever(instance.findById(creditId)).thenReturn(result)
        return this
    }

    fun saveOrUpdate(credit: Credit? = null): TestCreditDAO {
        whenever(instance.saveOrUpdate(any())).thenReturn(credit)
        verify("saveOrUpdate") {
            val capturedTransaction = argumentCaptor<Credit>()
            verify(instance, atLeastOnce()).saveOrUpdate(capturedTransaction.capture())
            capturedTransaction.allValues
        }
        return this
    }
}
