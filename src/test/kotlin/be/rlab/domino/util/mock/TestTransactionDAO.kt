package be.rlab.domino.util.mock

import be.rlab.domino.domain.model.Transaction
import be.rlab.domino.domain.persistence.TransactionDAO
import be.rlab.domino.util.VerifySupport
import com.nhaarman.mockitokotlin2.*
import org.joda.time.DateTime
import java.util.*

class TestTransactionDAO : VerifySupport() {

    val instance: TransactionDAO = mock()

    fun findById(
        transactionId: UUID,
        result: Transaction
    ): TestTransactionDAO {
        whenever(instance.findById(transactionId)).thenReturn(result)
        return this
    }

    fun findByPeriod(
        accountId: UUID,
        from: DateTime,
        to: DateTime,
        result: List<Transaction>
    ): TestTransactionDAO {
        whenever(instance.findByPeriod(accountId, from, to)).thenReturn(result)
        return this
    }

    fun findByPeriod(
        group: String,
        from: DateTime,
        to: DateTime,
        result: List<Transaction>
    ): TestTransactionDAO {
        whenever(instance.findByPeriod(group, from, to)).thenReturn(result)
        return this
    }

    fun saveOrUpdate(transaction: Transaction? = null): TestTransactionDAO {
        whenever(instance.saveOrUpdate(any())).thenReturn(transaction)

        verify("saveOrUpdate") {
            val capturedTransaction = argumentCaptor<Transaction>()
            verify(instance, atLeastOnce()).saveOrUpdate(capturedTransaction.capture())
            capturedTransaction.allValues
        }

        return this
    }
}
