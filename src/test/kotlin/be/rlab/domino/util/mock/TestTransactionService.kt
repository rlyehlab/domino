package be.rlab.domino.util.mock

import be.rlab.domino.util.VerifySupport
import be.rlab.domino.domain.TransactionService
import be.rlab.domino.domain.model.*
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.joda.time.DateTime

class TestTransactionService : VerifySupport() {

    val instance: TransactionService = mock()

    fun newDeposit(
        account: Account,
        description: String,
        value: Value,
        result: Transaction
    ): TestTransactionService {
        whenever(instance.newDeposit(account, description, value)).thenReturn(result)
        return this
    }

    fun newTransfer(
        sourceAccount: Account,
        destinationAccount: Account,
        description: String,
        value: Value,
        result: Transaction
    ): TestTransactionService {
        whenever(instance.newTransfer(sourceAccount, destinationAccount, description, value))
            .thenReturn(result)
        return this
    }

    fun newDebt(
        account: Account,
        description: String,
        value: Value,
        result: Transaction
    ): TestTransactionService {
        whenever(instance.newDebt(account, description, value))
            .thenReturn(result)
        return this
    }

    fun newCredit(
        account: Account,
        description: String,
        debtor: Agent,
        total: Value,
        dueDate: DateTime? = null,
        result: Credit
    ): TestTransactionService {
        whenever(instance.newCredit(account, description, debtor, total, dueDate))
            .thenReturn(result)
        return this
    }

    fun invalidate(
        transaction: Transaction,
        reason: String,
        result: Transaction
    ): TestTransactionService {
        whenever(instance.invalidate(transaction, reason))
            .thenReturn(result)
        return this
    }
}
