package be.rlab.domino.util.mock

import be.rlab.domino.util.VerifySupport
import be.rlab.domino.domain.persistence.BlockSignatureDAO
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import java.util.*

class TestBlockSignatureDAO : VerifySupport() {

    val instance: BlockSignatureDAO = mock()

    fun lastSignature(
        accountId: UUID,
        result: String
    ): TestBlockSignatureDAO {
        whenever(instance.lastSignature(accountId)).thenReturn(result)
        return this
    }
}
