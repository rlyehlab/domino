package be.rlab.domino.util.mock

import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.model.Account
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import java.util.*

class TestAccountService {

    val instance: AccountService = mock()

    fun findDefaultByAgent(
        agentId: UUID,
        result: Account
    ): TestAccountService {
        whenever(instance.findDefaultByAgent(agentId)).thenReturn(result)
        return this
    }

    fun findTreasuryAccount(
        result: Account
    ): TestAccountService {
        whenever(instance.findTreasuryAccount()).thenReturn(result)
        return this
    }

    fun findById(
        accountId: UUID,
        result: Account
    ): TestAccountService {
        whenever(instance.findById(accountId)).thenReturn(result)
        return this
    }
}
