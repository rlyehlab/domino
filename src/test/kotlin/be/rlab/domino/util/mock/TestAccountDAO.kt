package be.rlab.domino.util.mock

import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.persistence.AccountDAO
import be.rlab.domino.util.VerifySupport
import com.nhaarman.mockitokotlin2.*
import java.util.*

class TestAccountDAO : VerifySupport() {

    val instance: AccountDAO = mock()

    fun findByAgent(
        agentId: UUID,
        vararg result: Account
    ): TestAccountDAO {
        whenever(instance.findByAgent(agentId)).thenReturn(result.toList())
        return this
    }

    fun findDefaultByAgent(
        agentId: UUID,
        result: Account
    ): TestAccountDAO {
        whenever(instance.findDefaultByAgent(agentId)).thenReturn(result)
        return this
    }

    fun findById(
        accountId: UUID,
        result: Account
    ): TestAccountDAO {
        whenever(instance.findById(accountId)).thenReturn(result)
        return this
    }

    fun findByGroup(
        group: String,
        vararg result: Account
    ): TestAccountDAO {
        whenever(instance.findByGroup(group)).thenReturn(result.toList())
        return this
    }

    fun saveOrUpdate(vararg accounts: Account): TestAccountDAO {
        accounts.fold(
            whenever(instance.saveOrUpdate(any()))
        ) { matcher, account ->
            matcher.thenReturn(account)
        }

        verify("saveOrUpdate") {
            val capturedAccount = argumentCaptor<Account>()
            verify(instance, atLeastOnce()).saveOrUpdate(capturedAccount.capture())
            capturedAccount.allValues
        }

        return this
    }
}
