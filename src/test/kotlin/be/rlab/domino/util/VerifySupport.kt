package be.rlab.domino.util

abstract class VerifySupport {

    private val verifyHandlers: MutableMap<String, List<() -> Any>> = mutableMapOf()

    lateinit var capturedValues: Map<String, List<Any>>

    @Suppress("UNCHECKED_CAST")
    fun verifyAll() {
        capturedValues = verifyHandlers.map { (methodName, verifyCallbacks) ->
            val results: List<Any> = verifyCallbacks.flatMap { verifyCallback ->
                when (val result = verifyCallback()) {
                    is List<*> -> result
                    else -> listOf(result)
                } as List<Any>
            }
            methodName to results
        }.toMap()
    }

    @Suppress("UNCHECKED_CAST")
    fun<T> capturedValues(
        methodName: String
    ): List<T> {
        return capturedValues[methodName] as List<T>
    }

    @Suppress("UNCHECKED_CAST")
    fun<T> capturedValue(
        methodName: String
    ): T {
        return capturedValues[methodName]?.first() as T ?: throw RuntimeException("Nothing captured")
    }

    fun verify(
        methodName: String,
        handler: () -> Any
    ) {
        val handlers = verifyHandlers.getOrDefault(methodName, emptyList())
        verifyHandlers[methodName] = handlers + handler
    }
}
