package be.rlab.domino.util

import be.rlab.tehanu.store.DataSourceConfig
import be.rlab.tehanu.support.persistence.DataSourceInitializer
import be.rlab.domino.domain.persistence.*
import be.rlab.domino.util.persistence.TransactionSupport
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.test.util.ReflectionTestUtils
import java.lang.reflect.Field

object TestDataSource {
    private val config: DataSourceConfig = DataSourceConfig(
        url = "jdbc:h2:file:./db/test_db",
        user = "sa",
        password = "",
        driver = "org.h2.Driver",
        logStatements = true,
        drop = "no"
    )
    val db: Database by lazy {
        val connection = Database.connect(
            url = config.url,
            user = config.user,
            password = config.password,
            driver = config.driver
        )
        transaction(connection) {
            exec("DROP ALL OBJECTS")
        }
        val initializer = DataSourceInitializer(
            config = config,
            tables = listOf(Accounts, Agents, Credits, Bills,
                BlockSignatures, Transactions, TestActivities, Balances,
                CreditNotes)
        )
        val configField: Field = initializer.javaClass.superclass.getDeclaredField("config")
        configField.isAccessible = true
        configField.set(initializer, config)
        ReflectionTestUtils.setField(initializer, "db", connection)
        initializer.initAuto()

        connection
    }

    fun<T : TransactionSupport> initTransaction(transactionSupport: T): T {
        ReflectionTestUtils.setField(transactionSupport, "db", db)
        ReflectionTestUtils.setField(transactionSupport, "config", config)
        return transactionSupport
    }

    fun transaction(statement: Transaction.() -> Unit) {
        org.jetbrains.exposed.sql.transactions.transaction(db) {
            addLogger(StdOutSqlLogger)
            statement()
        }
    }
}
