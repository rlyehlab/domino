package be.rlab.domino.util

import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Agent
import be.rlab.domino.domain.model.TestAccount
import be.rlab.domino.domain.model.TestAgent
import be.rlab.domino.domain.persistence.AccountDAO
import be.rlab.domino.domain.persistence.AgentDAO
import be.rlab.domino.util.TestDataSource.initTransaction

abstract class ActivityDAOTestSupport {

    protected val accountDAO: AccountDAO by lazy {
        initTransaction(AccountDAO())
    }
    protected val agentDAO: AgentDAO by lazy {
        initTransaction(AgentDAO())
    }

    protected fun createAccount(): Account {
        return createAccount("rlyeh")
    }

    protected fun createAccount(group: String): Account {
        val agent: Agent = agentDAO.saveOrUpdate(TestAgent().new())

        return accountDAO.saveOrUpdate(TestAccount(
            group = group,
            owner = agent
        ).new())
    }
}
