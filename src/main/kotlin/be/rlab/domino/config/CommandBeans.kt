package be.rlab.domino.config

import be.rlab.domino.application.bot.bills.AddBill
import be.rlab.domino.application.bot.bills.Bills
import be.rlab.domino.application.bot.bills.CancelBill
import be.rlab.domino.application.bot.bills.PayBill
import be.rlab.domino.application.bot.donations.AddDonation
import be.rlab.domino.application.bot.donations.CreateSupporter
import be.rlab.domino.application.bot.donations.ListSupporters
import be.rlab.domino.application.bot.services.CreateService
import be.rlab.domino.application.bot.services.EditService
import be.rlab.domino.application.bot.services.ListServices
import be.rlab.domino.application.bot.treasury.*
import org.springframework.context.support.beans

object CommandBeans {
    fun beans() = beans {
        // Commands
        bean<CreateAccount>()
        bean<CreateService>()
        bean<EditService>()
        bean<ListServices>()
        bean<CreateSupporter>()
        bean<ListSupporters>()
        bean<InitTreasury>()
        bean<AddDonation>()
        bean<Transfer>()
        bean<Deposit>()
        bean<Balance>()
        bean<AddBill>()
        bean<CancelBill>()
        bean<Bills>()
        bean<PayBill>()
    }
}
