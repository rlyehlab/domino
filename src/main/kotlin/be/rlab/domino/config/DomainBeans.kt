package be.rlab.domino.config

import be.rlab.domino.domain.BalanceFactory
import be.rlab.domino.domain.*
import be.rlab.domino.domain.persistence.*
import be.rlab.domino.util.ObjectMapperFactory
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import org.springframework.context.support.beans
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

object DomainBeans {
    fun beans() = beans {
        val config: Config = ConfigFactory.defaultApplication().resolve()

        // Tables
        bean { Accounts }
        bean { Agents }
        bean { Credits }
        bean { Bills }
        bean { BlockSignatures }
        bean { Transactions }
        bean { Balances }
        bean { CreditNotes }

        // DAOs
        bean<AccountDAO>()
        bean<AgentDAO>()
        bean<BillDAO>()
        bean<BlockSignatureDAO>()
        bean<CreditDAO>()
        bean<TransactionDAO>()
        bean<BalanceDAO>()
        bean<CreditNoteDAO>()

        // Services
        bean<AgentService>()
        bean<AccountService>()
        bean<TransactionService>()
        bean<TreasuryService>()
        bean<BalanceService>()

        // Application components
        bean<BalanceFactory>()

        // Configs
        bean {
            val treasuryConfig: Config = config.getConfig("treasury")
            TreasuryConfig(
                group = treasuryConfig.getString("group"),
                priority = treasuryConfig.getInt("priority")
            )
        }

        bean {
            MappingJackson2HttpMessageConverter(ObjectMapperFactory.snakeCaseMapper)
        }
    }
}
