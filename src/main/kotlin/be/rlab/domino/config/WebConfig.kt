package be.rlab.domino.config

import be.rlab.domino.util.ObjectMapperFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.codec.ServerCodecConfigurer
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.http.codec.json.Jackson2JsonEncoder
import org.springframework.web.reactive.config.EnableWebFlux
import org.springframework.web.reactive.config.ResourceHandlerRegistry
import org.springframework.web.reactive.config.ViewResolverRegistry
import org.springframework.web.reactive.config.WebFluxConfigurer
import org.springframework.web.reactive.result.view.freemarker.FreeMarkerConfigurer
import java.util.*

@Configuration
@EnableWebFlux
@ComponentScan("be.rlab.domino.application")
open class WebConfig : WebFluxConfigurer {
    override fun configureHttpMessageCodecs(configurer: ServerCodecConfigurer) {
        configurer.defaultCodecs().jackson2JsonEncoder(Jackson2JsonEncoder(ObjectMapperFactory.snakeCaseMapper))
        configurer.defaultCodecs().jackson2JsonDecoder(Jackson2JsonDecoder(ObjectMapperFactory.snakeCaseMapper))
    }

    override fun configureViewResolvers(registry: ViewResolverRegistry) {
        registry.freeMarker()
    }

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        if (!registry.hasMappingForPattern("/webjars/**")) {
            registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/")
        }

        registry.addResourceHandler("/static/**")
            .addResourceLocations("classpath:/public/")
    }

    @Bean
    open fun freemarkerConfig(): FreeMarkerConfigurer {
        val freeMarkerConfigurer = FreeMarkerConfigurer()
        freeMarkerConfigurer.setFreemarkerSettings(Properties().apply {
            setProperty("number_format", "##0.00")
        })
        freeMarkerConfigurer.setTemplateLoaderPath("classpath:/views/")
        return freeMarkerConfigurer
    }
}
