package be.rlab.domino

import be.rlab.domino.config.CommandBeans
import be.rlab.domino.config.DomainBeans
import be.rlab.domino.config.WebConfig
import be.rlab.tehanu.SpringApplication
import be.rlab.tehanu.config.TelegramBeans
import be.rlab.tehanu.support.persistence.DataSourceInitializer
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.getBean
import org.springframework.http.server.reactive.HttpHandler
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter
import org.springframework.web.server.adapter.WebHttpHandlerBuilder
import reactor.netty.http.server.HttpServer

class Application(private val port: Int = 8080) : SpringApplication() {

    private val logger: Logger = LoggerFactory.getLogger(Application::class.java)

    override fun initialize() {
        logger.info("Initializing application context")

        applicationContext.apply {
            DomainBeans.beans().initialize(this)
            CommandBeans.beans().initialize(this)
            TelegramBeans.beans(resolveConfig()).initialize(this)
            register(WebConfig::class.java)
        }
    }

    override fun ready() {
        logger.info("Initializing data source")
        initDataSource(applicationContext.getBean())

        val httpHandler: HttpHandler = WebHttpHandlerBuilder
            .applicationContext(applicationContext)
            .build()

        logger.info("Starting web server")
        HttpServer.create()
            .port(port)
            .handle(ReactorHttpHandlerAdapter(httpHandler))
            .bindNow()
        logger.info("Application started at http://localhost:$port")
    }

    private fun initDataSource(dataSourceInitializer: DataSourceInitializer) {
        dataSourceInitializer.dropIfRequired(if (dataSourceInitializer.isTest) {
            "/db/drop.h2.sql"
        } else {
            "/db/drop.sql"
        })
        if (!dataSourceInitializer.isTest) {
            transaction {
                exec("USE domino;")
            }
        }
        if (dataSourceInitializer.isTest) {
            dataSourceInitializer.initAuto("/db/drop.h2.sql")
        } else {
            dataSourceInitializer.initIfRequired("classpath:/db/*.sql")
        }
    }
}

fun main(args: Array<String>) {
    Application().start()
}
