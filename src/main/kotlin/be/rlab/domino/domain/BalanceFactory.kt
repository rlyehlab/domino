package be.rlab.domino.domain

import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.model.Value.Companion.ZERO
import be.rlab.domino.domain.persistence.BlockSignatureDAO
import org.joda.time.DateTime

class BalanceFactory(
    private val blockSignatureDAO: BlockSignatureDAO
) {

    fun create(
        account: Account,
        from: DateTime,
        to: DateTime,
        description: String,
        previousBalance: Balance?,
        transactions: List<Transaction>,
        bills: List<Bill>
    ): Balance {
        val previousTotal: Value = previousBalance?.monthlyBalance ?: ZERO
        val total: Value = calculateTotal(transactions)
        val income: Value = calculateIncome(transactions)
        val debt: Value = calculateDebt(transactions)
        val validBills: List<Bill> = resolveValidBills(bills)
        val pendingDebt: Value = calculatePendingDebt(validBills)

        return Balance.new(
            previousSignature = blockSignatureDAO.lastSignature(account.id),
            account = account,
            description = description,
            from = from,
            to = to,
            transactions = transactions,
            bills = resolveValidBills(bills),
            previousBalance = previousTotal,
            debtsTotal = debt,
            debtsPending = pendingDebt,
            income = income,
            monthlyBalance = total,
            totalBalance = previousTotal + total
        )
    }

    private fun calculateDebt(transactions: List<Transaction>): Value {
        return transactions.fold(ZERO) { totalDebt, transaction ->
            if (transaction.type == TransactionType.DEBT) {
                totalDebt + transaction.value
            } else {
                totalDebt
            }
        }
    }

    private fun calculatePendingDebt(bills: List<Bill>): Value {
        return bills.fold(ZERO) { pendingDebt, billInfo ->
            if (billInfo.pending) {
                pendingDebt + billInfo.credit.debt
            } else {
                pendingDebt
            }
        }
    }

    private fun calculateIncome(transactions: List<Transaction>): Value {
        return transactions.fold(ZERO) { totalIncome, transaction ->
            if (transaction.type != TransactionType.DEBT) {
                totalIncome + transaction.value
            } else {
                totalIncome
            }
        }
    }

    private fun calculateTotal(transactions: List<Transaction>): Value {
        return transactions.fold(ZERO) { total, transaction ->
            total + transaction.value
        }
    }

    private fun resolveValidBills(
        bills: List<Bill>
    ): List<Bill> {
        return bills.filter { bill ->
            !bill.cancelled()
        }
    }
}
