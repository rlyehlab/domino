package be.rlab.domino.domain

import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.persistence.BillDAO
import be.rlab.domino.domain.persistence.BlockSignatureDAO
import be.rlab.domino.util.persistence.TransactionSupport
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

class TreasuryService(
    private val billDAO: BillDAO,
    private val accountService: AccountService,
    private val transactionService: TransactionService,
    private val signatureDAO: BlockSignatureDAO
): TransactionSupport() {

    fun newDonation(
        donor: Agent,
        receiver: Agent,
        value: Value
    ): Unit = transaction {
        val sourceAccount: Account = accountService.findDefaultByAgent(donor.id)
        val destinationAccount: Account = accountService.findDefaultByAgent(receiver.id)

        val deposit: Transaction = transactionService.newDeposit(sourceAccount, "Pago de bono", value)
        transactionService.newTransfer(deposit.account, treasuryAccount(), "Transferencia de bono", value)
        transactionService.newTransfer(treasuryAccount(), destinationAccount, "Transferencia de bono", value)
    }

    fun newTransfer(
        source: Agent,
        destination: Agent,
        value: Value
    ): Unit = transaction {
        val sourceAccount: Account = accountService.findDefaultByAgent(source.id)
        val destinationAccount: Account = accountService.findDefaultByAgent(destination.id)

        transactionService.newTransfer(sourceAccount, destinationAccount, "Transferencia entre cuentas", value)
    }

    /** Adds a value to the agent's default account.
     * @param target Agent that receives the value.
     * @param description Transaction description.
     * @param value Value to add to the agent's account.
     */
    fun newDeposit(
        target: Agent,
        description: String,
        value: Value
    ): Unit = transaction {
        val targetAccount: Account = accountService.findDefaultByAgent(target.id)
        transactionService.newDeposit(targetAccount, description, value)
    }

    /** Adds a new bill.
     *
     * Bills are charged to the treasury account. The service account generates a
     * credit for the amount of the bill, and the treasury must cancel the credit
     * before the bill's due date.
     *
     * A bill can be _replaced_. It means the previous bill from the same service is
     * cancelled and only the new bill remains valid.
     *
     * @param service Service that issued the bill.
     * @param description Bill description.
     * @param value Bill amount.
     * @param dueDate Due date to pay the bill.
     * @param replace true to replace the existing bill, false otherwise.
     */
    fun newBill(
        service: Agent,
        description: String,
        value: Value,
        dueDate: DateTime,
        replace: Boolean = false
    ): Bill = transaction {
        val chargedAccount: Account = treasuryAccount()
        val creditorAccount: Account = accountService.findDefaultByAgent(service.id)

        val resolvedValue: Value = if (replace) {
            val pendingBills: List<Bill> = billDAO.listPending(
                accountId = chargedAccount.id,
                agentId = service.id
            ).sortedBy { it.creationDate }

            pendingBills.forEach { bill ->
                if (!bill.partial()) {
                    cancelBill(bill)
                } else {
                    billDAO.saveOrUpdate(bill.invalidate())
                }
            }

            when {
                pendingBills.isNotEmpty() && pendingBills.last().partial() ->
                    value - pendingBills.last().totalPaid()
                else ->
                    value
            }
        } else {
            value
        }

        val debt: Transaction = transactionService.newDebt(
            account = treasuryAccount(),
            description = "Nueva factura de: ${service.fullName}",
            value = resolvedValue
        )

        val serviceCredit: Credit = transactionService.newCredit(
            account = creditorAccount,
            description = "Crédito por prestación de servicios",
            debtor = chargedAccount.owner,
            total = resolvedValue,
            dueDate = dueDate
        )

        billDAO.saveOrUpdate(Bill.unpaid(
            chargedAccount = treasuryAccount(),
            previousSignature = signatureDAO.lastSignature(treasuryAccount().id),
            description = description,
            credit = serviceCredit,
            debt = debt,
            priority = service.priority,
            dueDate = dueDate.withZone(DateTimeZone.UTC)
        ))
    }

    /** Pays a bill using the money from the specified account.
     *
     * All bills are charged to the treasury account, so a payment generates
     * a transfer to the treasury account and it cancels the credit in the service
     * account.
     *
     * @param payer Agent that's paying the bill.
     * @param bill Bill to pay.
     * @param value Amount to pay.
     */
    fun payBill(
        payer: Agent,
        bill: Bill,
        value: Value
    ): Bill = transaction {
        val payingAccount: Account = accountService.findDefaultByAgent(payer.id)
        val chargedAccount: Account = treasuryAccount()

        transactionService.newTransfer(
            sourceAccount = payingAccount,
            destinationAccount = chargedAccount,
            description = "Transferencia en concepto de pago de factura: ${bill.creditor().fullName}",
            value = value
        )

        billDAO.saveOrUpdate(bill.paid(value))
    }

    /** Lists all pending bills in the treasury account.
     */
    fun listPendingBills(): List<Bill> {
        return billDAO.listPending(treasuryAccount().id)
    }

    /** Lists all bills in the treasury account within the specified period.
     * @param year Period year.
     * @param month Period month.
     * @return the list of bills within the period.
     */
    fun listBills(
        year: Int,
        month: Int
    ): List<Bill> {
        val from = DateTime(year, month, 1, 0, 0, DateTimeZone.UTC)
        val to = from.plusMonths(1)
        return billDAO.findByPeriod(treasuryAccount().id, from, to)
    }

    /** Cancels a bill.
     * It generates a credit note and invalidates the underlying debt and credit.
     */
    fun cancelBill(bill: Bill) {
        val service: Agent = bill.creditor()

        val invalidDebt: Transaction = transactionService.invalidate(
            transaction = bill.debt,
            reason = "Cancelación de factura ${bill.id} del servicio: ${service.userName}"
        )
        val invalidCredit: Credit = bill.credit.invalidate(
            transactionService.invalidate(
                transaction = bill.credit.income,
                reason = "Cancelación de factura ${bill.id} del usuario ${bill.account.owner.userName}"
            )
        )

        val account: Account = accountService.findById(bill.debt.account.id)
        // Updates the account consolidated balance.
        accountService.saveOrUpdate(
            account.income(bill.credit.debt)
        )

        billDAO.saveOrUpdate(bill.invalidate(
            invalidDebt = invalidDebt,
            invalidCredit = invalidCredit
        ))
    }

    private fun treasuryAccount(): Account =
        accountService.findTreasuryAccount() ?:
        throw RuntimeException("The treasury account does not exist")
}
