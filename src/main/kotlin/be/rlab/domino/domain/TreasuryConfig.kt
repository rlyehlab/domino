package be.rlab.domino.domain

/** Treasury configuration.
 */
data class TreasuryConfig(
    /** Group the treasury belongs to. */
    val group: String,

    /** Default priority for treasury accounts. */
    val priority: Int
) {
    companion object {
        /** The treasury account has the max priority over any other accounts.
         */
        const val TREASURY_PRIORITY: Int = Int.MAX_VALUE
    }
}
