package be.rlab.domino.domain.model

/** Supported transactions type.
 * This enum is used to persist implementations of [Transaction] as an unstructured blob field.
 * Only generic properties are structured.
 */
enum class TransactionType {
    DEPOSIT,
    TRANSFER,
    CREDIT,
    DEBT
}
