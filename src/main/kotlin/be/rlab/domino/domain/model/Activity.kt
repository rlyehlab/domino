package be.rlab.domino.domain.model

import org.joda.time.DateTime
import java.util.*
import kotlin.math.absoluteValue

/** Represents a generic activity in an [Account].
 *
 * Activities are a blockchain within an account. Each activity has a signature
 * that is calculated using the blockId when it is created.
 */
interface Activity {
    val id: UUID
    val signature: BlockSignature
    val creationDate: DateTime
    val account: Account
    val description: String

    /** Returns the blockchain's block identity.
     * @param signatureVersion Version of the signature to generate the block id for.
     * @return a unique identity for this transaction.
     */
    fun blockId(signatureVersion: Int): String

    /** Returns a short identifier for this activity.
     * The short identifier is the absolute value of the [id] hashCode.
     */
    fun shortId(): Int = id.hashCode().absoluteValue
}
