package be.rlab.domino.domain.model

import be.rlab.domino.util.SignatureUtils.buildSignature
import be.rlab.domino.util.SignatureUtils.calculateSignature
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*

/** Represents a single transaction.
 *
 * Transactions must be acknowledged by the affected account. Transactions performed
 * within the commons bank are distributed, anyone could report a transaction that
 * affects an account, but the transaction must be acknowledged by the account owner.
 *
 * Transactions are a blockchain within an account. Each transaction has a signature
 * that's calculated using the blockId when it is created.
 */
data class Transaction(
    override val id: UUID,
    override val signature: BlockSignature,
    override val creationDate: DateTime,
    override val account: Account,
    override val description: String,
    val type: TransactionType,
    val value: Value,
    val ack: Boolean,
    val invalid: Boolean
) : Activity {

    companion object {
        fun deposit(
            previousSignature: String,
            account: Account,
            description: String,
            value: Value
        ): Transaction =
            new(previousSignature, account, TransactionType.DEPOSIT, description, -value)

        fun transfer(
            previousSignature: String,
            account: Account,
            description: String,
            value: Value
        ): Transaction =
            new(previousSignature, account, TransactionType.TRANSFER, description, value)

        fun credit(
            previousSignature: String,
            account: Account,
            description: String,
            value: Value
        ): Transaction =
            new(previousSignature, account, TransactionType.CREDIT, description, -value)

        fun debt(
            previousSignature: String,
            account: Account,
            description: String,
            value: Value
        ): Transaction =
            new(previousSignature, account, TransactionType.DEBT, description, value)

        fun new(
            previousSignature: String,
            account: Account,
            transactionType: TransactionType,
            description: String,
            value: Value
        ): Transaction {
            val creationDate: DateTime = DateTime.now().withZone(DateTimeZone.UTC)
            val id = UUID.randomUUID()

            return Transaction(
                id = id,
                signature = calculateSignature(
                    previousSignature, id, creationDate, account.id,
                    description, transactionType, value
                ),
                creationDate = creationDate,
                account = account,
                description = description,
                type = transactionType,
                value = value,
                ack = false,
                invalid = false
            )
        }
    }

    override fun blockId(signatureVersion: Int): String = buildSignature(
        fields = listOf(type, value)
    )

    /** Acknowledges this transaction.
     * It throws an error if the transaction is already acknowledged.
     */
    fun ack(): Transaction {
        require(!ack)
        return copy(
            ack = true
        )
    }

    /** Invalidates this transaction.
     *
     * There must exist am underlying [CreditNote] related to this transaction
     * explaining why it is invalidated.
     *
     * It throws an error if the transaction is already invalid.
     */
    fun invalidate(): Transaction {
        require(!invalid)
        return copy(
            invalid = true
        )
    }
}
