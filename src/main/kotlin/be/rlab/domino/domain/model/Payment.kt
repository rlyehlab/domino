package be.rlab.domino.domain.model

import org.joda.time.DateTime

/** Represents a payment on a specific date.
 */
data class Payment(
    val dateTime: DateTime,
    val value: Value
) {
    companion object {
        fun new(
            dateTime: DateTime,
            value: Value
        ): Payment = Payment(
            dateTime = dateTime,
            value = value
        )
    }
}
