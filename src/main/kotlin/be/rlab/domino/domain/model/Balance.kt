package be.rlab.domino.domain.model

import be.rlab.domino.util.SignatureUtils.buildSignature
import be.rlab.domino.util.SignatureUtils.calculateSignature
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*

/** Represents the balance of an [Account].
 *
 * The balance consist of all transactions processed in the account within
 * a period of time.
 *
 * The balance is treated as an [Activity] so it can be verified.
 */
data class Balance(
    override val id: UUID,
    override val signature: BlockSignature,
    override val creationDate: DateTime,
    override val account: Account,
    override val description: String,
    val from: DateTime,
    val to: DateTime,
    val transactions: List<Transaction>,
    val bills: List<Bill>,
    val previousBalance: Value,
    val debtsTotal: Value,
    val debtsPending: Value,
    val income: Value,
    val monthlyBalance: Value,
    val totalBalance: Value
) : Activity {

    companion object {
        private const val SIGNATURE_VERSION_1: Int = 1
        private const val SIGNATURE_VERSION_2: Int = 2

        fun new(
            previousSignature: String,
            account: Account,
            description: String,
            from: DateTime,
            to: DateTime,
            transactions: List<Transaction>,
            bills: List<Bill>,
            previousBalance: Value,
            debtsTotal: Value,
            debtsPending: Value,
            income: Value,
            monthlyBalance: Value,
            totalBalance: Value
        ): Balance {
            val id: UUID = UUID.randomUUID()
            val creationDate: DateTime = DateTime.now().withZone(DateTimeZone.UTC)

            return Balance(
                id = id,
                creationDate = creationDate,
                signature = calculateSignature(
                    SIGNATURE_VERSION_2, previousSignature, id, creationDate, account.id,
                    description, from, to, transactionsId(transactions), billsId(bills),
                    previousBalance, debtsTotal, debtsPending, income, monthlyBalance,
                    totalBalance
                ),
                description = description,
                account = account,
                from = from,
                to = to,
                transactions = transactions,
                bills = bills,
                previousBalance = previousBalance,
                debtsTotal = debtsTotal,
                debtsPending = debtsPending,
                income = income,
                monthlyBalance = monthlyBalance,
                totalBalance = totalBalance
            )
        }

        private fun transactionsId(transactions: List<Transaction>): String {
            return transactions.joinToString("|") { transaction ->
                transaction.signature.signature
            }
        }

        private fun billsId(bills: List<Bill>): String {
            return bills.joinToString("|") { bill ->
                bill.signature.signature
            }
        }
    }

    override fun blockId(signatureVersion: Int): String {
        return when (signatureVersion) {
            SIGNATURE_VERSION_1 -> buildSignature(
                fields = listOf(from, to, transactionsId(transactions), billsId(bills))
            )
            SIGNATURE_VERSION_2 -> buildSignature(
                fields = listOf(
                    from, to, transactionsId(transactions), billsId(bills),
                    previousBalance, debtsTotal, debtsPending, income, monthlyBalance,
                    totalBalance
                )
            )
            else -> throw IllegalArgumentException("Invalid signature version: $signatureVersion")
        }
    }
}
