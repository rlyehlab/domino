package be.rlab.domino.domain.model

import be.rlab.domino.util.SignatureUtils.buildSignature
import be.rlab.domino.util.SignatureUtils.calculateSignature
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*

/** Represents a service bill.
 *
 * A bill is associated to a [Credit] in the service account and a debt in the
 * treasury account.
 *
 * In order to cancel a bill, the underlying debt and credit must be invalidated.
 */
data class Bill(
    override val id: UUID,
    override val signature: BlockSignature,
    override val creationDate: DateTime,
    override val account: Account,
    override val description: String,
    val credit: Credit,
    val debt: Transaction,
    val priority: Int,
    val dueDate: DateTime?,
    val pending: Boolean
) : Activity {
    companion object {
        fun unpaid(
            previousSignature: String,
            chargedAccount: Account,
            description: String,
            credit: Credit,
            debt: Transaction,
            priority: Int,
            dueDate: DateTime?
        ): Bill = new(
            previousSignature = previousSignature,
            chargedAccount = chargedAccount,
            description = description,
            credit = credit,
            debt = debt,
            priority = priority,
            dueDate = dueDate,
            pending = true
        )

        fun new(
            previousSignature: String,
            chargedAccount: Account,
            description: String,
            credit: Credit,
            debt: Transaction,
            priority: Int,
            dueDate: DateTime?,
            pending: Boolean
        ): Bill {
            val creationDate: DateTime = DateTime.now().withZone(DateTimeZone.UTC)
            val id: UUID = UUID.randomUUID()

            return Bill(
                id = id,
                signature = calculateSignature(
                    previousSignature, id, creationDate, chargedAccount.id,
                    description, credit.signature, debt.signature
                ),
                creationDate = creationDate,
                account = chargedAccount,
                description = description,
                credit = credit,
                debt = debt,
                priority = priority,
                dueDate = dueDate,
                pending = pending
            )
        }
    }

    /** Returns the creditor that issued this bill.
     * @return the creditor.
     */
    fun creditor(): Agent = credit.account.owner

    /** Pays this bill.
     * @param value Amount to pay.
     * @return the paid bill.
     */
    fun paid(value: Value): Bill {
        val credit: Credit = credit.cancel(value)

        return copy(
            credit = credit,
            pending = credit.pending
        )
    }

    /** Creates a credit note for this bill.
     *
     * @param invalidDebt Invalidated transaction.
     * @param invalidCredit Invalidated credit.
     *
     * @return the cancelled bill.
     */
    fun invalidate(
        invalidDebt: Transaction = debt,
        invalidCredit: Credit = credit
    ): Bill = copy(
        credit = invalidCredit,
        debt = invalidDebt,
        pending = false
    )

    /** Indicates if this bill is cancelled.
     *
     * If a bill is cancelled, it shouldn't be used to calculate balances.
     *
     * @return true if cancelled, false otherwise.
     */
    fun cancelled(): Boolean = debt.invalid

    fun totalPaid(): Value = debt.value - credit.debt

    /** Indicates whether this bill is partially paid.
     * @return true if partially paid, false otherwise.
     */
    fun partial(): Boolean = credit.payments.isNotEmpty()

    override fun blockId(signatureVersion: Int): String = buildSignature(
        fields = listOf(credit.signature, debt.signature)
    )
}
