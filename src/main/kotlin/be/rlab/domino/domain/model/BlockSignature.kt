package be.rlab.domino.domain.model

import org.joda.time.DateTime
import java.util.*

/** Represents a signature in the blockchain.
 */
data class BlockSignature(
    val version: Int,
    val accountId: UUID,
    val creationDate: DateTime,
    val previous: String,
    val signature: String
) {
    companion object {
        const val CURRENT_VERSION: Int = 1
        const val INITIAL_SIGNATURE: String = "::FIRST"

        fun new(
            version: Int,
            accountId: UUID,
            creationDate: DateTime,
            previous: String,
            signature: String
        ): BlockSignature = BlockSignature(
            version = version,
            creationDate = creationDate,
            previous = previous,
            signature = signature,
            accountId = accountId
        )
    }

    override fun toString(): String = signature
}
