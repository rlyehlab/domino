package be.rlab.domino.domain.model

import java.util.*

/** Represents an account in the commons bank.
 */
data class Account(
    /** Unique id for the account. */
    val id: UUID,

    /** Group this account belongs to. */
    val group: String,

    /** Agent that owns this account. */
    val owner: Agent,

    /** Total amount of debt in this account. */
    val debt: Value,

    /** Account initial signature used to sign activities. It is calculated from a random seed. */
    val initialSignature: String,

    /** If the agent has more than one account, it indicates that
     * this is the default account for operations.
     */
    val default: Boolean

) {
    companion object {
        /** Creates a new account for an agent.
         * @param group Group this account belongs to.
         * @param owner Agent that owns this account.
         * @param initialSignature Initial signature to sign activities.
         * @param default Indicates whether this is the default account.
         */
        fun new(
            group: String,
            owner: Agent,
            initialSignature: String,
            default: Boolean
        ): Account = Account(
            id = UUID.randomUUID(),
            group = group,
            owner = owner,
            debt = Value.ZERO,
            initialSignature = initialSignature,
            default = default
        )
    }

    fun income(value: Value): Account = copy(
        debt = debt - value
    )

    fun outcome(value: Value): Account = copy(
        debt = debt + value
    )
}
