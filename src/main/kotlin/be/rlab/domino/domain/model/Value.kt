package be.rlab.domino.domain.model

import java.math.BigDecimal

/** Economic value is a measure of the benefit provided by a good or service to an [Agent].
 */
data class Value(
    /** Unit of measure of the value. */
    val currency: Currency,
    /** Amount of value. */
    val amount: BigDecimal
) {
    companion object {
        const val SCALE: Int = 3

        val ZERO: Value = Value(
            currency = Currency.ARS,
            amount = BigDecimal(0.00).setScale(SCALE)
        )

        fun valueOf(
            currency: Currency,
            amount: Double
        ): Value = Value(
            currency = currency,
            amount = BigDecimal.valueOf(amount).setScale(SCALE)
        )
    }

    operator fun minus(other: Value): Value {
        require(other.currency == currency)
        return Value(
            currency = currency,
            amount = amount - other.amount
        )
    }

    operator fun plus(other: Value): Value {
        require(other.currency == currency)
        return Value(
            currency = currency,
            amount = amount + other.amount
        )
    }

    operator fun unaryMinus(): Value = Value(
        currency = currency,
        amount = -amount
    )

    override fun toString(): String = "$currency $amount"
}
