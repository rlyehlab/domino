package be.rlab.domino.domain.model

import be.rlab.domino.util.SignatureUtils.buildSignature
import be.rlab.domino.util.SignatureUtils.calculateSignature
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*

/** A credit note is used to invalidate an existing [Transaction].
 */
data class CreditNote(
    override val id: UUID,
    override val signature: BlockSignature,
    override val creationDate: DateTime,
    override val account: Account,
    override val description: String,
    val transaction: Transaction
) : Activity {
    companion object {
        fun new(
            transaction: Transaction,
            description: String
        ): CreditNote {
            val creationDate: DateTime = DateTime.now().withZone(DateTimeZone.UTC)
            val id: UUID = UUID.randomUUID()

            return CreditNote(
                id = id,
                signature = calculateSignature(
                    transaction.signature.signature, id, creationDate, transaction.account.id,
                    description, transaction.id
                ),
                creationDate = creationDate,
                account = transaction.account,
                description = description,
                transaction = transaction
            )
        }
    }

    override fun blockId(signatureVersion: Int): String = buildSignature(
        fields = listOf(transaction.id)
    )
}
