package be.rlab.domino.domain.model

import java.util.*

data class Agent(
    val id: UUID,
    val priority: Int,
    val userId: Long,
    val userName: String,
    val fullName: String?
) {
    companion object {
        fun new(
            priority: Int,
            userId: Long,
            userName: String,
            fullName: String?
        ): Agent = Agent(
            id = UUID.randomUUID(),
            priority = priority,
            userName = userName,
            fullName = fullName,
            userId = userId
        )
    }
}
