package be.rlab.domino.domain.model

enum class Currency {
    ARS,
    DOLLAR,
    BITCOIN
}
