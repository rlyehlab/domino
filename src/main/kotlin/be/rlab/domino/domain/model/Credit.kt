package be.rlab.domino.domain.model

import be.rlab.domino.util.SignatureUtils.buildSignature
import be.rlab.domino.util.SignatureUtils.calculateSignature
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.math.BigDecimal
import java.util.*

data class Credit(
    override val id: UUID,
    override val signature: BlockSignature,
    override val creationDate: DateTime,
    override val account: Account,
    override val description: String,
    val dueDate: DateTime?,
    val debtor: Agent,
    val income: Transaction,
    val debt: Value,
    val payments: List<Payment>,
    val pending: Boolean
) : Activity {

    companion object {
        fun new(
            previousSignature: String,
            account: Account,
            description: String,
            debtor: Agent,
            income: Transaction,
            dueDate: DateTime? = null,
            pending: Boolean = true
        ): Credit {
            val creationDate: DateTime = DateTime.now().withZone(DateTimeZone.UTC)
            val id: UUID = UUID.randomUUID()

            return Credit(
                id = id,
                signature = calculateSignature(
                    previousSignature, id, creationDate, account.id,
                    description, debtor.id, income.signature
                ),
                creationDate = creationDate,
                account = account,
                description = description,
                debtor = debtor,
                income = income,
                debt = -income.value,
                payments = emptyList(),
                dueDate = dueDate,
                pending = pending
            )
        }
    }

    fun cancel(value: Value): Credit = copy(
        debt = debt - value,
        pending = (debt - value).amount > BigDecimal.ZERO,
        payments = payments + Payment.new(
            dateTime = DateTime.now().withZone(DateTimeZone.UTC),
            value = value
        )
    )

    /** Invalidates this credit.
     * It cancels the debt and marks the credit as no longer pending.
     * @param invalidIncome Invalidated transaction.
     */
    fun invalidate(invalidIncome: Transaction): Credit = copy(
        pending = false,
        income = invalidIncome
    )

    override fun blockId(signatureVersion: Int): String = buildSignature(
        fields = listOf(debtor.id, income.signature)
    )
}
