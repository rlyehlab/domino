package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.CreditNote
import be.rlab.domino.util.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.EntityID
import java.util.*

object CreditNotes : Activities("treasury_credit_notes")

class CreditNoteEntity(id: EntityID<UUID>) : ActivityEntity<CreditNote>(
    table = CreditNotes,
    id = id,
    type = CreditNote::class
) {
    companion object : AbstractEntityClass<CreditNote, CreditNoteEntity>(CreditNotes)

    override fun initEntity(
        entity: CreditNote
    ): CreditNote = entity.copy(
        account = account.toDomainType()
    )
}

/** DAO to manage [CreditNote]s.
 */
class CreditNoteDAO : ActivityDAO<CreditNote, CreditNoteEntity>(
    table = CreditNotes,
    entity = CreditNoteEntity
)
