package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Transaction
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.greaterEq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.lessEq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.joda.time.DateTime

/** DAO to manage [Transaction]s.
 */
class TransactionDAO : ActivityDAO<Transaction, TransactionEntity>(
    table = Transactions,
    entity = TransactionEntity
) {
    fun findValidByPeriod(
        group: String,
        from: DateTime,
        to: DateTime
    ): List<Transaction> {
        return TransactionEntity.wrapRows(
            (Transactions innerJoin Accounts).select {
                (Accounts.group eq group) and
                (Transactions.creationDate greaterEq from) and
                (Transactions.creationDate lessEq to) and
                (Transactions.invalid eq false)
            }
        ).map { activityEntity ->
            activityEntity.toDomainType()
        }
    }
}
