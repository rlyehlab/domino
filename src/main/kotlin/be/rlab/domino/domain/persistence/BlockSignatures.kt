package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.BlockSignature
import be.rlab.domino.domain.model.BlockSignature.Companion.CURRENT_VERSION
import be.rlab.domino.util.persistence.AbstractEntity
import be.rlab.domino.util.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.UUIDTable
import org.joda.time.DateTime
import java.util.*

object BlockSignatures : UUIDTable(name = "treasury_signatures") {
    val creationDate = datetime("creation_date").index()
    val previousSignature = varchar("previous_signature", length = 64)
    val signature = varchar("signature", length = 64).uniqueIndex()
    val account = reference("account", Accounts)
    val version = integer("version").default(CURRENT_VERSION)
}

class BlockSignatureEntity(id: EntityID<UUID>) : AbstractEntity<BlockSignature>(id) {
    companion object : AbstractEntityClass<BlockSignature, BlockSignatureEntity>(BlockSignatures)

    var creationDate: DateTime by BlockSignatures.creationDate
    var previousSignature: String by BlockSignatures.previousSignature
    var signature: String by BlockSignatures.signature
    var account: AccountEntity by AccountEntity referencedOn BlockSignatures.account
    var version: Int by BlockSignatures.version

    override fun create(source: BlockSignature): BlockSignatureEntity {
        creationDate = source.creationDate
        previousSignature = source.previous
        signature = source.signature
        account = AccountEntity[source.accountId]
        version = source.version

        return update(source)
    }

    override fun update(source: BlockSignature): BlockSignatureEntity {
        return this
    }

    override fun toDomainType(): BlockSignature = BlockSignature.new(
        version = version,
        accountId = account.id.value,
        creationDate = creationDate,
        previous = previousSignature,
        signature = signature
    )
}
