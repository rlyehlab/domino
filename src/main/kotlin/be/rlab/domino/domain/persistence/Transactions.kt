package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Transaction
import be.rlab.domino.util.persistence.AbstractEntityClass
import be.rlab.domino.util.persistence.EntitySerialization.serialize
import org.jetbrains.exposed.dao.EntityID
import java.util.*

object Transactions : Activities("treasury_transactions") {
    val invalid = bool("is_invalid").default(false)
}

class TransactionEntity(id: EntityID<UUID>) : ActivityEntity<Transaction>(
    table = Transactions,
    id = id,
    type = Transaction::class
) {
    companion object : AbstractEntityClass<Transaction, TransactionEntity>(Transactions)

    var invalid: Boolean by Transactions.invalid

    override fun create(source: Transaction): ActivityEntity<Transaction> {
        invalid = source.invalid
        return super.create(source)
    }

    override fun update(source: Transaction): ActivityEntity<Transaction> {
        invalid = source.invalid
        data = serialize(source)

        return super.update(source)
    }

    override fun initEntity(
        entity: Transaction
    ): Transaction = entity.copy(
        account = account.toDomainType()
    )
}
