package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Activity
import be.rlab.domino.util.SignatureUtils.calculateSignature
import be.rlab.domino.util.persistence.AbstractEntity
import be.rlab.domino.util.persistence.EntitySerialization.serialize
import be.rlab.tehanu.support.ObjectMapperFactory
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.UUIDTable
import org.joda.time.DateTime
import java.util.*
import kotlin.reflect.KClass

open class Activities(tableName: String) : UUIDTable(name = tableName) {
    val creationDate = datetime("creation_date").index()
    val signature = reference("signature", BlockSignatures)
    val account = reference("account", Accounts)
    val description = varchar("description", length = 255)
    val data = text("data")
}

abstract class ActivityEntity<T : Activity>(
    table: Activities,
    id: EntityID<UUID>,
    private val type: KClass<T>
) : AbstractEntity<T>(id) {

    var creationDate: DateTime by table.creationDate
    var signature: BlockSignatureEntity by BlockSignatureEntity referencedOn table.signature
    var account: AccountEntity by AccountEntity referencedOn table.account
    var description: String by table.description
    var data: String by table.data

    override fun create(source: T): ActivityEntity<T> {
        val resolvedAccount = AccountEntity[source.account.id]
        signature = BlockSignatureEntity.new {
            account = resolvedAccount
            creationDate = source.signature.creationDate
            previousSignature = source.signature.previous
            signature = source.signature.signature
        }

        creationDate = source.creationDate
        account = resolvedAccount
        description = source.description
        data = serialize(source)
        return this
    }

    override fun update(source: T): ActivityEntity<T> {
        return this
    }

    abstract fun initEntity(
        entity: T
    ): T

    override fun toDomainType(): T {
        val entity: T = ObjectMapperFactory.snakeCaseMapper.readValue(data, type.java)
        return verify(initEntity(entity))
    }

    private fun verify(activity: T): T {
        if (calculateSignature(activity).signature != signature.signature) {
            throw DataIntegrityViolation(
                "Activity ${activity.id} has invalid signature. " +
                "This incident should be reported."
            )
        }
        return activity
    }
}
