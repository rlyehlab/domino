package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Bill
import be.rlab.domino.util.persistence.AbstractEntityClass
import be.rlab.domino.util.persistence.EntitySerialization.serialize
import org.jetbrains.exposed.dao.EntityID
import java.util.*

object Bills : Activities("treasury_bills") {
    val pending = bool("is_pending")
    val creditor = reference("creditor", Agents)
}

class BillEntity(id: EntityID<UUID>) : ActivityEntity<Bill>(
    table = Bills,
    id = id,
    type = Bill::class
) {
    companion object : AbstractEntityClass<Bill, BillEntity>(Bills)

    var pending: Boolean by Bills.pending
    var creditor: AgentEntity by AgentEntity referencedOn Bills.creditor

    override fun create(source: Bill): ActivityEntity<Bill> {
        super.create(source)
        pending = source.pending
        creditor = AgentEntity[source.creditor().id]

        // Cascades credit.
        CreditEntity.saveOrUpdate(source.credit.id, source.credit)

        return this
    }

    override fun update(source: Bill): ActivityEntity<Bill> {
        super.update(source)
        pending = source.pending
        data = serialize(source)

        // Cascades credit.
        CreditEntity.saveOrUpdate(source.credit.id, source.credit)

        return this
    }

    override fun initEntity(
        entity: Bill
    ): Bill = entity.copy(
        account = account.toDomainType()
    )
}
