package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Bill
import org.jetbrains.exposed.sql.and
import java.util.*

/** DAO to manage [Bill]s.
 */
class BillDAO : ActivityDAO<Bill, BillEntity>(
    table = Bills,
    entity = BillEntity
) {
    /** Lists all pending bills for an account.
     * @param accountId Account to list pending bills for.
     */
    fun listPending(accountId: UUID): List<Bill> = transaction {
        list(accountId) {
            (Bills.account eq accountId) and
            (Bills.pending eq true)
        }
    }

    /** Lists all pending bills issued by an agent.
     * @param accountId Account to list pending bills for.
     * @param agentId Agent that issued the bill.
     */
    fun listPending(
        accountId: UUID,
        agentId: UUID
    ): List<Bill> = transaction {
        list(accountId) {
            (Bills.creditor eq agentId) and
            (Bills.pending eq true)
        }
    }
}
