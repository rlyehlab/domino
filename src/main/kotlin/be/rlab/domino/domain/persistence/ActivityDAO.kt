package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Activity
import be.rlab.domino.domain.model.Transaction
import be.rlab.domino.util.persistence.AbstractEntityClass
import be.rlab.domino.util.persistence.TransactionSupport
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.SqlExpressionBuilder
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.greaterEq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.lessEq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.joda.time.DateTime
import java.util.*

/** DAO to manage Activities.
 */
abstract class ActivityDAO<DomainType : Activity, EntityType : ActivityEntity<DomainType>>(
    private val table: Activities,
    private val entity: AbstractEntityClass<DomainType, EntityType>
) : TransactionSupport() {

    /** Retrieves an activity by id.
     * @param id Id of the required activity.
     * @return the required activity.
     */
    fun findById(id: UUID): DomainType = transaction {
        entity[id].toDomainType()
    }

    /** Creates or updates an activity.
     * @param activity Activity to save or update.
     * @return the activity.
     */
    fun saveOrUpdate(activity: DomainType): DomainType = transaction {
        entity.saveOrUpdate(activity.id, activity)
    }

    /** Finds activities for a specific period.
     *
     * @param accountId Id of the account that contains the activities.
     * @param from Period start date.
     * @param to Period end date.
     * @return the required activities.
     */
    fun findByPeriod(
        accountId: UUID,
        from: DateTime,
        to: DateTime
    ): List<DomainType> = transaction {
        entity.find {
            (table.account eq accountId) and
            (table.creationDate greaterEq from) and
            (table.creationDate lessEq to)
        }.map { activityEntity ->
            activityEntity.toDomainType()
        }
    }

    /** Finds activities for a group of accounts within a period.
     * @param group Accounts group.
     * @param from Period start date.
     * @param to Period end date.
     * @return the required activities.
     */
    fun findByPeriod(
        group: String,
        from: DateTime,
        to: DateTime
    ): List<DomainType> = transaction {
        entity.wrapRows(
            (table innerJoin Accounts).select {
                (Accounts.group eq group) and
                (table.creationDate greaterEq from) and
                (table.creationDate lessEq to)
            }
        ).map { activityEntity ->
            activityEntity.toDomainType()
        }
    }

    /** Lists activities and applies a where statement.
     * @param accountId Id of the account to list activities.
     * @param where Where expression.
     * @return the list of required activities.
     */
    protected fun list(
        accountId: UUID,
        where: SqlExpressionBuilder.()-> Op<Boolean>
    ): List<DomainType> = transaction {
        entity.find {
            (table.account eq accountId) and
            where()
        }.map { activityEntity ->
            activityEntity.toDomainType()
        }
    }

    /** Finds activities for a specific period and applies additional restrictions.
     *
     * @param accountId Id of the account that contains the activities.
     * @param from Period start date.
     * @param to Period end date.
     * @param where Where expression.
     * @return the required activities.
     */
    protected fun findByPeriod(
        accountId: UUID,
        from: DateTime,
        to: DateTime,
        where: SqlExpressionBuilder.()-> Op<Boolean>
    ): List<DomainType> = transaction {
        entity.find {
            (table.account eq accountId) and
            (table.creationDate greaterEq from) and
            (table.creationDate lessEq to) and
            where()
        }.map { activityEntity ->
            activityEntity.toDomainType()
        }
    }
}
