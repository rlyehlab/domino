package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Balance
import org.jetbrains.exposed.sql.and
import org.joda.time.DateTime
import java.util.*

/** DAO to manage [Balance]s.
 */
class BalanceDAO : ActivityDAO<Balance, BalanceEntity>(
    table = Balances,
    entity = BalanceEntity
) {

    /** Finds a balance for a specific period.
     * @param accountId Id of the account that owns the balance.
     * @param from Period start date.
     * @param to Period end date.
     * @return the required balance, if it exists.
     */
    fun getByPeriod(
        accountId: UUID,
        from: DateTime,
        to: DateTime
    ): Balance? {
        return BalanceEntity.find {
            (Balances.account eq accountId) and
            (Balances.from eq from) and
            (Balances.to eq to)
        }.map { balanceEntity ->
            balanceEntity.toDomainType()
        }.firstOrNull()
    }
}
