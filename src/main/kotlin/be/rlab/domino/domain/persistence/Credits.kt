package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Credit
import be.rlab.domino.util.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.EntityID
import java.util.*

object Credits : Activities("treasury_credits")

class CreditEntity(id: EntityID<UUID>) : ActivityEntity<Credit>(
    table = Credits,
    id = id,
    type = Credit::class
) {
    companion object : AbstractEntityClass<Credit, CreditEntity>(Credits)

    override fun initEntity(
        entity: Credit
    ): Credit = entity.copy(
        account = account.toDomainType()
    )
}

/** DAO to manage [Credit]s.
 */
class CreditDAO : ActivityDAO<Credit, CreditEntity>(
    table = Credits,
    entity = CreditEntity
)
