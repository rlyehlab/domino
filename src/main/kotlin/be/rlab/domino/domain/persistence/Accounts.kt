package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Currency
import be.rlab.domino.domain.model.Value
import be.rlab.domino.util.persistence.AbstractEntity
import be.rlab.domino.util.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.UUIDTable
import java.math.BigDecimal
import java.util.*

object Accounts : UUIDTable(name = "treasury_accounts") {
    val group = varchar("group", length = 100)
    val owner = reference("owner", Agents)
    val default = bool("is_default")
    val currency = enumeration("currency_name", Currency::class)
    val debt = decimal("debt", 15, Value.SCALE)
    val initialSignature = varchar("initial_signature", length = 64).uniqueIndex()
}

class AccountEntity(id: EntityID<UUID>) : AbstractEntity<Account>(id) {
    companion object : AbstractEntityClass<Account, AccountEntity>(
        Accounts
    )

    var group: String by Accounts.group
    var owner: AgentEntity by AgentEntity referencedOn Accounts.owner
    var default: Boolean by Accounts.default
    var currency: Currency by Accounts.currency
    var debt: BigDecimal by Accounts.debt
    var initialSignature: String by Accounts.initialSignature

    override fun create(source: Account): AccountEntity {
        return update(source)
    }

    override fun update(source: Account): AccountEntity {
        group = source.group
        owner = AgentEntity[source.owner.id]
        default = source.default
        currency = source.debt.currency
        debt = source.debt.amount
        initialSignature = source.initialSignature
        return this
    }

    override fun toDomainType(): Account = Account(
        id = id.value,
        initialSignature = initialSignature,
        group = group,
        owner = owner.toDomainType(),
        default = default,
        debt = Value(currency, debt)
    )
}
