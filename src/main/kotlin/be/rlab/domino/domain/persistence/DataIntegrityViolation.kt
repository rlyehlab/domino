package be.rlab.domino.domain.persistence

class DataIntegrityViolation(message: String) : RuntimeException(message)
