package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Balance
import be.rlab.domino.util.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.EntityID
import org.joda.time.DateTime
import java.util.*

object Balances : Activities("treasury_balances") {
    val from = datetime("from").index()
    val to = datetime("to").index()
}

class BalanceEntity(id: EntityID<UUID>) : ActivityEntity<Balance>(
    table = Balances,
    id = id,
    type = Balance::class
) {
    companion object : AbstractEntityClass<Balance, BalanceEntity>(Balances)

    var from: DateTime by Balances.from
    var to: DateTime by Balances.to

    override fun create(source: Balance): ActivityEntity<Balance> {
        super.create(source)
        from = source.from
        to = source.to
        return this
    }

    override fun initEntity(entity: Balance): Balance = entity.copy(
        account = account.toDomainType()
    )
}
