package be.rlab.domino.domain.persistence

import be.rlab.domino.util.persistence.TransactionSupport
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.select
import java.util.*

class BlockSignatureDAO : TransactionSupport() {

    /** Returns the last blockchain signature for the account.
     * @param accountId Id of the account to find the last signature.
     * @return the last signature, if exists. It is null only if the account
     * has no transactions.
     */
    fun lastSignature(
        accountId: UUID
    ): String = transaction {
        BlockSignatures.slice(
            BlockSignatures.signature
        ).select {
            BlockSignatures.account eq accountId
        }.orderBy(
            BlockSignatures.creationDate to SortOrder.DESC
        ).limit(1).map { resultRow ->
            resultRow[BlockSignatures.signature]
        }.first()
    }
}
