package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Agent
import be.rlab.domino.util.persistence.AbstractEntity
import be.rlab.domino.util.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.UUIDTable
import java.util.*

object Agents : UUIDTable(name = "treasury_agents") {
    val priority = integer("priority")
    val userId = long("user_id")
    val userName = varchar("user_name", 50)
    val fullName = varchar("full_name", 100).nullable()
}

class AgentEntity(id: EntityID<UUID>) : AbstractEntity<Agent>(id) {
    companion object : AbstractEntityClass<Agent, AgentEntity>(Agents)

    var priority: Int by Agents.priority
    var userId: Long by Agents.userId
    var userName: String by Agents.userName
    var fullName: String? by Agents.fullName

    override fun create(source: Agent): AgentEntity {
        return update(source)
    }

    override fun update(source: Agent): AgentEntity {
        priority = source.priority
        userName = source.userName
        userId = source.userId
        fullName = source.fullName
        return this
    }

    override fun toDomainType(): Agent = Agent.new(
        priority = priority,
        userId = userId,
        userName = userName,
        fullName = fullName
    ).copy(id = id.value)
}
