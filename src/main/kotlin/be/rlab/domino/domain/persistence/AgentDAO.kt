package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Agent
import be.rlab.domino.util.persistence.TransactionSupport
import org.jetbrains.exposed.exceptions.EntityNotFoundException
import java.util.*

class AgentDAO : TransactionSupport() {

    fun findById(id: UUID): Agent = transaction {
        AgentEntity[id].toDomainType()
    }

    fun findByUserId(userId: Long): Agent? = transaction {
        AgentEntity.find {
            Agents.userId eq userId
        }.map { agentEntity ->
            agentEntity.toDomainType()
        }.firstOrNull()
    }

    fun findByUserName(userName: String): Agent? = transaction {
        AgentEntity.find {
            Agents.userName eq userName
        }.map { agentEntity ->
            agentEntity.toDomainType()
        }.firstOrNull()
    }

    fun findByPriority(priority: Int): List<Agent> = transaction {
        AgentEntity.find {
            Agents.priority eq priority
        }.map { agentEntity ->
            agentEntity.toDomainType()
        }
    }

    fun exists(userId: Long): Boolean = transaction {
        AgentEntity.find {
            Agents.userId eq userId
        }.count() > 0
    }

    fun exists(userName: String): Boolean = transaction {
        AgentEntity.find {
            Agents.userName eq userName
        }.count() > 0
    }

    fun saveOrUpdate(agent: Agent): Agent = transaction {
        AgentEntity.saveOrUpdate(agent.id, agent)
    }
}
