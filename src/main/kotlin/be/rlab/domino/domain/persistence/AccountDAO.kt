package be.rlab.domino.domain.persistence

import be.rlab.domino.domain.model.Account
import be.rlab.domino.util.persistence.TransactionSupport
import org.jetbrains.exposed.sql.and
import java.util.*

class AccountDAO : TransactionSupport() {

    fun findByAgent(agentId: UUID): List<Account> = transaction {
        AccountEntity.find {
            Accounts.owner eq agentId
        }.map { entity ->
            entity.toDomainType()
        }
    }

    fun findDefaultByAgent(agentId: UUID): Account = transaction {
        AccountEntity.find {
            (Accounts.owner eq agentId) and
            (Accounts.default eq true)
        }.single().toDomainType()
    }

    fun findByGroup(group: String): List<Account> = transaction {
        AccountEntity.find {
            (Accounts.group eq group) and
            (Accounts.default eq true)
        }.map { accountEntity ->
            accountEntity.toDomainType()
        }
    }

    fun findById(accountId: UUID): Account = transaction {
        AccountEntity[accountId].toDomainType()
    }

    fun saveOrUpdate(account: Account): Account = transaction {
        AccountEntity.saveOrUpdate(account.id, account)
    }
}
