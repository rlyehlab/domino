package be.rlab.domino.domain

import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Balance
import be.rlab.domino.domain.model.Bill
import be.rlab.domino.domain.model.Transaction
import be.rlab.domino.domain.persistence.BalanceDAO
import be.rlab.domino.domain.persistence.BillDAO
import be.rlab.domino.domain.persistence.TransactionDAO
import be.rlab.domino.util.persistence.TransactionSupport
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

/** Service to generate accounts balances.
 */
class BalanceService(
    private val balanceDAO: BalanceDAO,
    private val transactionDAO: TransactionDAO,
    private val billDAO: BillDAO,
    private val treasuryConfig: TreasuryConfig,
    private val accountService: AccountService,
    private val balanceFactory: BalanceFactory
) : TransactionSupport() {

    /** Finds the balance for the treasury account within a period.
     *
     * This operation is not optimized, it finds all transactions for all
     * accounts in the organization within the required period.
     *
     * If the required year/month is still in progress, it calculates the
     * balance but it isn't saved. The consolidation is available once the
     * month ended.
     */
    fun findForTreasury(
        year: Int,
        month: Int
    ): Balance = transaction {
        val account: Account = treasuryAccount()
        val from = DateTime(year, month, 1, 0, 0, DateTimeZone.UTC)
        val to = from.plusMonths(1)

        balanceDAO.getByPeriod(account.id, from, to) ?: newBalance(
            year = year,
            month = month,
            from = from,
            to = to,
            account = account,
            transactions = transactionDAO.findValidByPeriod(treasuryConfig.group, from, to),
            bills = billDAO.findByPeriod(treasuryConfig.group, from, to)
        )
    }

    /** Finds a balance for an account within the specified period.
     *
     * If the required year/month is still in progress, it calculates the
     * balance but it isn't saved. The consolidation is available once the
     * month ended.
     *
     * If the balance does not exist, it creates the balance.
     */
    fun find(
        account: Account,
        year: Int,
        month: Int
    ): Balance = transaction {
        val from = DateTime(year, month, 1, 0, 0, DateTimeZone.UTC)
        val to = from.plusMonths(1)

        balanceDAO.getByPeriod(account.id, from, to) ?: newBalance(
            year = year,
            month = month,
            from = from,
            to = to,
            account = account,
            transactions = transactionDAO.findByPeriod(account.id, from, to),
            bills = billDAO.findByPeriod(account.id, from, to)
        )
    }

    private fun newBalance(
        year: Int,
        month: Int,
        from: DateTime,
        to: DateTime,
        account: Account,
        transactions: List<Transaction>,
        bills: List<Bill>
    ): Balance {
        val currentDate: DateTime = DateTime.now()
            .withDayOfMonth(1)
            .withTimeAtStartOfDay()
            .withZone(DateTimeZone.UTC)

        val previousDate: DateTime = DateTime(year, month, 1, 0, 0, DateTimeZone.UTC)
            .minusMonths(1)
            .withTimeAtStartOfDay()
        val previousBalance: Balance? = balanceDAO.getByPeriod(account.id, previousDate, previousDate.plusMonths(1))

        val balance: Balance = balanceFactory.create(
            account = account,
            from = from,
            to = to,
            description = "Balance del período $year/$month",
            previousBalance = previousBalance,
            transactions = transactions,
            bills = bills
        )

        return if (year >= currentDate.year && month >= currentDate.monthOfYear) {
            // Current month, balance is still open.
            balance
        } else {
            // Closed balance.
            balanceDAO.saveOrUpdate(balance)
        }
    }

    private fun treasuryAccount(): Account =
        accountService.findTreasuryAccount() ?:
        throw RuntimeException("The treasury account does not exist")
}
