package be.rlab.domino.domain

import be.rlab.domino.domain.model.Agent
import be.rlab.domino.domain.persistence.AgentDAO

class AgentService(
    private val agentDAO: AgentDAO
) {

    /** Finds an agent by user id.
     * @param userId Agent's user id.
     * @return the required agent.
     */
    fun findByUserId(userId: Long): Agent? {
        return agentDAO.findByUserId(userId)
    }

    /** Finds an agent by user name.
     * @param userName Agent's user name.
     * @return the required agent.
     */
    fun findByUserName(userName: String): Agent? {
        return agentDAO.findByUserName(userName)
    }

    /** Finds all agents with the same priority.
     * @param priority Required priority.
     * @return the list of matching agents.
     */
    fun findByPriority(priority: Int): List<Agent> {
        return agentDAO.findByPriority(priority)
    }

    /** Determines whether an agent exists by the user id.
     * @param userId Agent's user id.
     * @return true if it exists, false otherwise..
     */
    fun exists(userId: Long): Boolean {
        return agentDAO.exists(userId)
    }

    /** Determines whether an agent exists by the user name.
     * @param userName Agent's user name.
     * @return true if it exists, false otherwise.
     */
    fun exists(userName: String): Boolean {
        return agentDAO.exists(userName)
    }

    /** Creates an agent.
     *
     * @param priority Agent's priority for debts.
     * @param userId User related to this agent.
     * @param userName Agent's user name.
     * @param fullName Agent's full name.
     * @return the created agent.
     */
    fun create(
        priority: Int,
        userId: Long,
        userName: String,
        fullName: String?
    ): Agent {
        require(!agentDAO.exists(userId)) {
            "An agent with user id $userId already exists"
        }

        val agent: Agent = Agent.new(
            priority = priority,
            userId = userId,
            userName = userName,
            fullName = fullName
        )

        return agentDAO.saveOrUpdate(agent)
    }
}