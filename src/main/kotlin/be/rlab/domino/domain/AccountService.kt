package be.rlab.domino.domain

import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Agent
import be.rlab.domino.domain.model.Transaction
import be.rlab.domino.domain.model.Value
import be.rlab.domino.domain.persistence.AccountDAO
import be.rlab.domino.domain.persistence.TransactionDAO
import be.rlab.domino.util.SignatureUtils.buildSignature
import be.rlab.domino.util.SignatureUtils.randomSignature
import be.rlab.domino.util.persistence.TransactionSupport
import java.util.*

class AccountService(
    private val accountDAO: AccountDAO,
    private val transactionDAO: TransactionDAO,
    private val agentService: AgentService
) : TransactionSupport() {

    /** Finds an account by id.
     * @param accountId Id of the required account.
     * @return the required account.
     */
    fun findById(accountId: UUID): Account {
        return accountDAO.findById(accountId)
    }

    /** Finds the default account of an [Agent].
     * @param agentId Id of the agent that owns the account.
     * @return the required account.
     */
    fun findDefaultByAgent(agentId: UUID): Account  {
        return accountDAO.findDefaultByAgent(agentId)
    }

    /** Finds the default account of an [Agent] by its user id.
     * @param userId Agent's user id.
     * @return the required account, if exists.
     */
    fun findDefaultByUserId(userId: Long): Account?  {
        return agentService.findByUserId(userId)?.let { owner ->
            accountDAO.findDefaultByAgent(owner.id)
        }
    }

    /** Returns the treasury account, if exists.
     * @return the treasury account or null.
     */
    fun findTreasuryAccount(): Account? {
        return agentService.findByPriority(
            TreasuryConfig.TREASURY_PRIORITY
        ).firstOrNull()?.let { owner ->
            accountDAO.findDefaultByAgent(owner.id)
        }
    }

    /** Finds all accounts in the specified group.
     * @param group Required accounts group.
     * @return the required accounts.
     */
    fun findByGroup(group: String): List<Account> {
        return accountDAO.findByGroup(group)
    }

    /** Creates an account for the specified agent.
     *
     * @param group Account group.
     * @param userId User id of the owner.
     */
    fun create(
        group: String,
        userId: Long
    ): Account = transaction {
        val owner: Agent = agentService.findByUserId(userId)
            ?: throw RuntimeException("User not found: $userId")

        val treasurySignature: String = findTreasuryAccount()?.initialSignature
            ?: throw RuntimeException("Treasury account not initialized.")
        val initialSignature: String = buildSignature(listOf(treasurySignature, group, userId), hash = true)
        val account: Account = accountDAO.saveOrUpdate(
            Account.new(
                initialSignature = initialSignature,
                group = group,
                owner = owner,
                default = true
            )
        )
        transactionDAO.saveOrUpdate(
            Transaction.deposit(
                previousSignature = initialSignature,
                account = account,
                description = "Transacción de validación de cuenta",
                value = Value.ZERO
            )
        )

        accountDAO.saveOrUpdate(account)
    }

    /** Creates the treasury account.
     *
     * @param group Treasury group.
     * @param userName Treasury owner user.
     * @param fullName Treasury owner full name.
     */
    fun createTreasuryAccount(
        group: String,
        userId: Long,
        userName: String,
        fullName: String
    ): Account = transaction {

        require(findTreasuryAccount() == null) {
            "The treasury account already exists"
        }

        val owner: Agent = agentService.create(
            priority = TreasuryConfig.TREASURY_PRIORITY,
            userId = userId,
            userName = userName,
            fullName = fullName
        )

        val initialSignature: String = randomSignature(group, userId, userName, fullName)
        val account: Account = accountDAO.saveOrUpdate(
            Account.new(
                group = group,
                owner = owner,
                initialSignature = initialSignature,
                default = true
            )
        )
        transactionDAO.saveOrUpdate(
            Transaction.deposit(
                previousSignature = initialSignature,
                account = account,
                description = "Transacción de validación de cuenta",
                value = Value.ZERO
            )
        )

        accountDAO.saveOrUpdate(account)
    }

    fun saveOrUpdate(account: Account): Account {
        return accountDAO.saveOrUpdate(account)
    }
}
