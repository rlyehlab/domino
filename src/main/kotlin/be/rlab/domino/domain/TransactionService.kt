package be.rlab.domino.domain

import be.rlab.domino.domain.model.*
import be.rlab.domino.domain.persistence.*
import be.rlab.domino.util.persistence.TransactionSupport
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*

/** Service to manage accounts transactions.
 */
class TransactionService(
    private val accountDAO: AccountDAO,
    private val transactionDAO: TransactionDAO,
    private val creditDAO: CreditDAO,
    private val creditNoteDAO: CreditNoteDAO,
    private val blockSignatureDAO: BlockSignatureDAO
): TransactionSupport() {

    fun newDeposit(
        account: Account,
        description: String,
        value: Value
    ): Transaction = transaction {
        val deposit: Transaction = Transaction.deposit(
            previousSignature = lastSignature(account.id),
            account = accountDAO.saveOrUpdate(
                accountDAO.findById(account.id).income(value)
            ),
            description = description,
            value = value
        )

        transactionDAO.saveOrUpdate(deposit)
    }

    /** Transfers a value from an account to another account.
     *
     * A transfer generates a transfer transaction in the source account
     * and a transfer in the destination account with the same value.
     */
    fun newTransfer(
        sourceAccount: Account,
        destinationAccount: Account,
        description: String,
        value: Value
    ): Transaction = transaction {
        val sourceTransfer: Transaction = Transaction.transfer(
            previousSignature = lastSignature(sourceAccount.id),
            account = accountDAO.saveOrUpdate(
                accountDAO.findById(sourceAccount.id).outcome(value)
            ),
            description = "Transferencia a ${destinationAccount.owner.fullName}: $description",
            value = value
        )
        val destinationTransfer: Transaction = Transaction.transfer(
            previousSignature = lastSignature(destinationAccount.id),
            account = accountDAO.saveOrUpdate(
                accountDAO.findById(destinationAccount.id).income(value)
            ),
            description = "Transferencia de ${sourceAccount.owner.fullName}: $description",
            value = -value
        )

        transactionDAO.saveOrUpdate(destinationTransfer)
        transactionDAO.saveOrUpdate(sourceTransfer)
    }

    fun newCredit(
        account: Account,
        description: String,
        debtor: Agent,
        total: Value,
        dueDate: DateTime? = null
    ): Credit = transaction {
        val income: Transaction = transactionDAO.saveOrUpdate(
            Transaction.credit(
                previousSignature = lastSignature(account.id),
                account = accountDAO.saveOrUpdate(
                    accountDAO.findById(account.id).income(total)
                ),
                description = description,
                value = total
            )
        )
        val credit: Credit = Credit.new(
            previousSignature = lastSignature(account.id),
            account = account,
            description = description,
            debtor = debtor,
            income = income,
            dueDate = dueDate?.withZone(DateTimeZone.UTC)
        )

        creditDAO.saveOrUpdate(credit)
    }

    fun newDebt(
        account: Account,
        description: String,
        value: Value
    ): Transaction = transaction {
        val debt: Transaction = Transaction.debt(
            previousSignature = lastSignature(account.id),
            account = accountDAO.saveOrUpdate(
                accountDAO.findById(account.id).outcome(value)
            ),
            description = description,
            value = value
        )
        transactionDAO.saveOrUpdate(debt)
    }

    fun acknowledge(transaction: Transaction): Transaction = transaction {
        transactionDAO.saveOrUpdate(transaction.ack())
    }

    /** Creates a credit note and invalidates the specified transaction.
     * @param transaction Transaction to invalidate.
     * @param reason Reason why the transaction is being invalidated.
     * @return the invalid transaction.
     */
    fun invalidate(
        transaction: Transaction,
        reason: String
    ): Transaction {
        creditNoteDAO.saveOrUpdate(
            CreditNote.new(
                transaction = transaction,
                description = reason
            )
        )

        return transactionDAO.saveOrUpdate(
            transaction.invalidate()
        )
    }

    private fun lastSignature(accountId: UUID): String =
        blockSignatureDAO.lastSignature(accountId)
}