package be.rlab.domino.util.persistence

import be.rlab.tehanu.store.DataSourceConfig
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.addLogger
import java.sql.Connection.TRANSACTION_REPEATABLE_READ
import javax.inject.Inject

abstract class TransactionSupport {

    @Inject
    private lateinit var db: Database
    @Inject
    private lateinit var config: DataSourceConfig

    fun<T> transaction(
        isolationLevel: Int = TRANSACTION_REPEATABLE_READ,
        repetitionAttempts: Int = 1,
        statement: Transaction.() -> T
    ): T {
        return org.jetbrains.exposed.sql.transactions.transaction(
            transactionIsolation = isolationLevel,
            repetitionAttempts = repetitionAttempts,
            db = db
        ) {
            if (config.logStatements) {
                addLogger(StdOutSqlLogger)
            }

            statement()
        }
    }
}
