package be.rlab.domino.util

import be.rlab.domino.domain.model.Activity
import be.rlab.domino.domain.model.BlockSignature
import be.rlab.domino.domain.model.BlockSignature.Companion.CURRENT_VERSION
import be.rlab.domino.util.HashUtils.sha256
import org.joda.time.DateTime
import java.util.*

/** Utilities to manage blockchain signatures.
 */
object SignatureUtils {

    /** Calculates the signature for a transaction.
     */
    fun calculateSignature(
        previousSignature: String,
        id: UUID,
        creationDate: DateTime,
        accountId: UUID,
        description: String,
        vararg blockId: Any?
    ): BlockSignature = calculateSignature(
        CURRENT_VERSION, previousSignature, id, creationDate,
        accountId, description, *blockId
    )

    /** Calculates the signature for a transaction.
     */
    fun calculateSignature(
        signatureVersion: Int,
        previousSignature: String,
        id: UUID,
        creationDate: DateTime,
        accountId: UUID,
        description: String,
        vararg blockId: Any?
    ): BlockSignature {
        return BlockSignature.new(
            version = signatureVersion,
            accountId = accountId,
            creationDate = creationDate,
            previous = previousSignature,
            signature = buildSignature(
                fields = listOf(
                    previousSignature, id, creationDate, accountId,
                    description, buildSignature(blockId.toList())
                ),
                hash = true
            )
        )
    }

    /** Calculates the signature for activities.
     */
    fun calculateSignature(
        activity: Activity
    ): BlockSignature {
        val version: Int = activity.signature.version
        return BlockSignature.new(
            version = version,
            accountId = activity.account.id,
            creationDate = activity.creationDate,
            previous = activity.signature.previous,
            signature = buildSignature(
                fields = listOf(
                    activity.signature.previous, activity.id, activity.creationDate,
                    activity.account.id, activity.description, activity.blockId(version)
                ),
                hash = true
            )
        )
    }

    /** Builds a signature from a list of fields.
     * @param fields Fields to build the signature.
     * @param hash true to hash the signature, false otherwise.
     */
    fun buildSignature(
        fields: List<Any?>,
        hash: Boolean = false
    ): String {
        val signature: String = fields.joinToString("!#")

        return if (hash) {
            sha256(signature)
        } else {
            signature
        }
    }

    /** Generates a random signature from a list of seeds.
     * This is useful to initialize the first signature of a blockchain.
     * @param seeds Random data to use as seeds.
     * @return The generated signature.
     */
    fun randomSignature(vararg seeds: Any): String {
        val id: UUID = UUID.randomUUID()
        return buildSignature(seeds.toList() + id, hash = true)
    }
}
