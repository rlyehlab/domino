package be.rlab.domino.application.web

import be.rlab.domino.application.web.ViewController.Companion.BASE_PATH
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping(BASE_PATH)
abstract class ViewController {

    companion object {
        const val BASE_PATH: String = "/"
    }

    protected abstract val viewName: String
    protected abstract val title: String

    fun renderView(model: Model): String {
        model["title"] = title
        model["base_path"] = BASE_PATH
        return viewName
    }
}
