package be.rlab.domino.application.web

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class HomeController : ViewController() {

    override val viewName: String = "index"
    override val title: String = "Domino"

    @GetMapping("/")
    fun home(model: Model): String =
        renderView(model)
}
