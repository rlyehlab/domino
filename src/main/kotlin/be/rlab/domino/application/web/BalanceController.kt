package be.rlab.domino.application.web

import be.rlab.domino.application.model.BalanceDTO
import be.rlab.domino.domain.BalanceService
import be.rlab.domino.domain.model.Balance
import org.joda.time.DateTime
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class BalanceController(
    private val balanceService: BalanceService
) : ViewController() {

    override val viewName: String = "balance"
    override val title: String = "Balances"

    @GetMapping("/balance")
    fun balance(
        @RequestParam(required = false) year: Int?,
        @RequestParam(required = false) month: Int?,
        model: Model
    ): String {
        val now: DateTime = DateTime.now()
        val resolvedYear: Int = year ?: now.year
        val resolvedMonth: Int = month ?: now.monthOfYear
        val balance: Balance = balanceService.findForTreasury(
            year = resolvedYear,
            month = resolvedMonth
        )
        model["balance"] = BalanceDTO.from(balance)
        model["year"] = resolvedYear
        model["month"] = resolvedMonth

        return renderView(model)
    }
}
