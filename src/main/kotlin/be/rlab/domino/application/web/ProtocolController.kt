package be.rlab.domino.application.web

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class ProtocolController : ViewController() {

    override val viewName: String = "protocol"
    override val title: String = "Protocolo"

    @GetMapping("/protocol")
    fun protocol(model: Model): String =
        renderView(model)
}
