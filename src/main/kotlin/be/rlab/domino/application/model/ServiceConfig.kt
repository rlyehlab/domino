package be.rlab.domino.application.model

import java.util.*

/** Contains services configuration.
 * This configuration is linked to a service with the agent's id.
 */
data class ServiceConfig(
    val id: UUID,
    val billingMode: BillingMode
)
