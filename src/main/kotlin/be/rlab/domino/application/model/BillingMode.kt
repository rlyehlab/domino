package be.rlab.domino.application.model

/** Enumeration with the supported billing modes for services.
 */
enum class BillingMode {
    /** Every period the new also contains the pending debt. */
    SINGLE,
    /** Every period the new bill only contains debt from the period. */
    MULTIPLE
}
