package be.rlab.domino.application.model

/** Constants containing the known account groups.
 */
object AccountGroups {
    const val SUPPORTERS: String = "supporter"
    const val CREDITORS: String = "creditor"
}