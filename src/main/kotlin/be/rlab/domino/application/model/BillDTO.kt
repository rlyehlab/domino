package be.rlab.domino.application.model

import be.rlab.domino.domain.model.Bill
import be.rlab.domino.domain.model.Value
import org.joda.time.DateTime

data class BillDTO(
    val serviceName: String,
    val debt: Value,
    val total: Value,
    val dueDate: DateTime?,
    val pending: Boolean
) {
    companion object {
        fun from(
            bill: Bill
        ): BillDTO = BillDTO(
            serviceName = bill.creditor().userName,
            debt = bill.credit.debt,
            total = bill.debt.value,
            dueDate = bill.dueDate,
            pending = bill.pending
        )
    }
}
