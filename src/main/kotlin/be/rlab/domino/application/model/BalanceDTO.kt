package be.rlab.domino.application.model

import be.rlab.domino.domain.model.Balance
import be.rlab.domino.domain.model.Value

data class BalanceDTO(
    val previousBalance: Value,
    val bills: List<BillDTO>,
    val debtsTotal: Value,
    val debtsPending: Value,
    val income: Value,
    val monthlyBalance: Value,
    val totalBalance: Value
) {
    companion object {
        fun from(balance: Balance): BalanceDTO = BalanceDTO(
            previousBalance = -balance.previousBalance,
            bills = balance.bills.map(BillDTO.Companion::from),
            debtsTotal = -balance.debtsTotal,
            debtsPending = -balance.debtsPending,
            income = -balance.income,
            monthlyBalance = -balance.monthlyBalance,
            totalBalance = -balance.totalBalance
        )
    }
}
