package be.rlab.domino.application.bot.view

import be.rlab.domino.domain.model.Currency
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.model.Field
import be.rlab.tehanu.view.model.enumeration

/** Telegram field to ask for a currency type.
 *
 * It displays buttons with all fields from the [Currency] enumeration.
 */
fun UserInput.currency(description: String): Field = enumeration<Currency>(
    description,
    "La moneda no es válida, seleccioná una moneda"
)
