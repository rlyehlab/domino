package be.rlab.domino.application.bot.donations

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.bot.view.account
import be.rlab.domino.application.bot.view.value
import be.rlab.domino.application.model.AccountGroups.SUPPORTERS
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.AgentService
import be.rlab.domino.domain.TreasuryConfig
import be.rlab.domino.domain.TreasuryService
import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Agent
import be.rlab.domino.domain.model.Value
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.messages.model.ChatType

/** Reports a new donation and adds the donation to the current's user account.
 */
@MessageListener(Commands.ADD_DONATION)
class AddDonation(
    private val agentService: AgentService,
    private val accountService: AccountService,
    private val treasuryService: TreasuryService,
    private val treasuryConfig: TreasuryConfig
) {

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext = context.userInput(
        "Este comando agrega una donación y la acredita en tu cuenta"
    ) {
        val accounts: List<Account> = accountService.findByGroup(SUPPORTERS) +
                accountService.findByGroup(treasuryConfig.group)
        require(accounts.isNotEmpty()) {
            "No hay cuentas de donantes, agregá una cuenta con el comando /new_supporter"
        }

        val from: Account by account(accounts,
            "seleccioná la persona que donó, si no está en la " +
            "lista agregala con el comando /new_supporter"
        )

        val value: Value by value("cuál es el monto de la donación?")

        onSubmit {
            createDonation(context, from, value)
        }
    }

    private fun createDonation(
        context: MessageContext,
        from: Account,
        value: Value
    ) {
        val donor: Agent = from.owner
        val receiverId: Long = context.require(
            context.user?.id, "User account not found"
        )
        val receiver: Agent = context.require(
            agentService.findByUserId(receiverId),
            "Tu cuenta no existe, podés crearla usando /new_account"
        )

        treasuryService.newDonation(
            donor = donor,
            receiver = receiver,
            value = value
        )

        context.answer("listo, el dinero quedó registrado en tu cuenta")
    }
}
