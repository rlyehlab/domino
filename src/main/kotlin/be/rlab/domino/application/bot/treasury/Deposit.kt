package be.rlab.domino.application.bot.treasury

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.bot.view.value
import be.rlab.domino.domain.AgentService
import be.rlab.domino.domain.TreasuryService
import be.rlab.domino.domain.model.Agent
import be.rlab.domino.domain.model.Value
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.messages.model.ChatType

/** Makes a deposit into the current account.
 */
@MessageListener(Commands.DEPOSIT)
class Deposit(
    private val agentService: AgentService,
    private val treasuryService: TreasuryService
) {

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext = context.userInput(
        "Este comando realiza un depósito en tu cuenta."
    ) {
        val userName: String = context.require(
            context.user?.userName,
            "No tenés configurado un nombre de usuario, necesito que tengas nombre " +
            "de usuario para hacer un depósito"
        )
        val target: Agent = context.require(
            agentService.findByUserName(userName),
            "Tu cuenta no existe, podés crearla usando el comando /new_account"
        )

        val description: String by field("describí brevemente por qué hacés el depósito")
        val value: Value by value("cuál es el monto del depósito?")

        onSubmit {
            newDeposit(context, target, description, value)
        }
    }

    private fun newDeposit(
        context: MessageContext,
        target: Agent,
        description: String,
        value: Value
    ) {
        treasuryService.newDeposit(
            target = target,
            description = description,
            value = value
        )

        context.answer("listo, el dinero quedó depositado en tu cuenta")
    }
}
