package be.rlab.domino.application.bot.services

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.bot.view.account
import be.rlab.domino.application.bot.view.billingMode
import be.rlab.domino.application.model.AccountGroups.CREDITORS
import be.rlab.domino.application.model.BillingMode
import be.rlab.domino.application.model.MemorySlots.SERVICE_CONFIGURATIONS
import be.rlab.domino.application.model.ServiceConfig
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Agent
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.messages.model.ChatType
import be.rlab.tehanu.store.Memory

/** Changes a service configuration.
 */
@MessageListener(Commands.EDIT_SERVICE)
class EditService(
    private val accountService: AccountService,
    memory: Memory
) {

    private var configurations: List<ServiceConfig> by memory.slot(SERVICE_CONFIGURATIONS, emptyList<ServiceConfig>())

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext = context.userInput(
        "Este comando establece los parámetros de configuración de un servicio"
    ) {
        val serviceAccount: Account by account(accountService.findByGroup(CREDITORS),
            "elegí el servicio que querés configurar"
        )

        val billingMode: BillingMode by billingMode()

        onSubmit {
            editService(context, serviceAccount, billingMode)
        }
    }

    private fun editService(
        context: MessageContext,
        serviceAccount: Account,
        billingMode: BillingMode
    ) {
        val service: Agent = serviceAccount.owner
        val newConfig = ServiceConfig(
            id = service.id,
            billingMode = billingMode
        )

        val exists: Boolean = configurations.any { config ->
            config.id == service.id
        }

        configurations = if (exists) {
            configurations.map { config ->
                if (config.id == service.id) {
                    newConfig
                } else {
                    config
                }
            }
        } else {
            configurations + newConfig
        }

        context.answer("listo, el servicio quedó configurado")
    }
}
