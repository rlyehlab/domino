package be.rlab.domino.application.bot

object Commands {
    const val ADD_BILL: String = "/new_bill"
    const val CANCEL_BILL: String = "/cancel_bill"
    const val PAY_BILL: String = "/pay_bill"
    const val BILLS: String = "/bills"

    const val ADD_DONATION: String = "/new_donation"
    const val CREATE_SUPPORTER: String = "/new_supporter"
    const val LIST_SUPPORTERS: String = "/supporters"

    const val CREATE_SERVICE: String = "/new_service"
    const val EDIT_SERVICE: String = "/edit_service"
    const val LIST_SERVICES: String = "/services"

    const val BALANCE: String = "/balance"
    const val CREATE_ACCOUNT: String = "/new_account"
    const val DEPOSIT: String = "/new_deposit"
    const val TRANSFER: String = "/new_transfer"
    const val INIT_TREASURY: String = "/init_treasury"
}