package be.rlab.domino.application.bot.view

import be.rlab.domino.application.model.BillingMode
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.model.Field
import be.rlab.tehanu.view.model.enumeration

/** Telegram field to ask for a currency type.
 *
 * It displays buttons with all fields from the [BillingMode] enumeration.
 */
fun UserInput.billingMode(): Field = enumeration<BillingMode>(
    "Seleccioná el modo de facturación. " +
    "SINGLE significa que cada factura contiene el monto de los períodos anteriores que estén pendientes. " +
    "MULTIPLE significa que se factura cada período por separado, independientemente de la deuda.",
    "seleccioná un modo de la lista"
)
