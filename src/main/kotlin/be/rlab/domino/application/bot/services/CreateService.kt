package be.rlab.domino.application.bot.services

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.bot.view.billingMode
import be.rlab.domino.application.model.AccountGroups.CREDITORS
import be.rlab.domino.application.model.BillingMode
import be.rlab.domino.application.model.MemorySlots
import be.rlab.domino.application.model.ServiceConfig
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.AgentService
import be.rlab.domino.domain.TreasuryConfig
import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Agent
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.messages.model.ChatType
import be.rlab.tehanu.store.Memory

/** Creates an account for a creditor.
 */
@MessageListener(Commands.CREATE_SERVICE)
class CreateService(
    private val agentService: AgentService,
    private val accountService: AccountService,
    treasuryConfig: TreasuryConfig,
    memory: Memory
) {

    private val servicePriority: Int = treasuryConfig.priority + 100
    private var configurations: List<ServiceConfig> by memory.slot(MemorySlots.SERVICE_CONFIGURATIONS, emptyList<ServiceConfig>())
    private val treasuryAccount: Account?
        get() = accountService.findTreasuryAccount()

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext = context.userInput(
        "Este comando crea una cuenta para un servicio o acreedor. Cuando se cargue una " +
        "factura, te voy a pedir que la asocies a una de estas cuentas"
    ) {

        precondition(treasuryAccount != null, transition(Commands.INIT_TREASURY))

        val serviceName: String by field("decime el nombre de la empresa de servicio (edesur, aysa, etc)") {
            validator { value ->
                require(!agentService.exists(value)) {
                    "ya existe un servicio con el nombre de usuario $value"
                }
            }
        }

        val type: String by field("qué tipo de servicio es? por ejemplo: luz, internet, etc")
        val billingMode: BillingMode by billingMode()

        onSubmit {
            createAgent(context, serviceName, type, billingMode)
        }
    }

    private fun createAgent(
        context: MessageContext,
        serviceName: String,
        type: String,
        billingMode: BillingMode
    ) {
        val owner: Agent = agentService.create(
            priority = servicePriority,
            userId = System.currentTimeMillis(),
            userName = serviceName,
            fullName = "Cuenta de servicio: $type"
        )

        accountService.create(
            group = CREDITORS,
            userId = owner.userId
        )

        configurations = configurations + ServiceConfig(
            id = owner.id,
            billingMode = billingMode
        )
        context.answer("listo, creé la cuenta para el servicio: $serviceName")
    }
}
