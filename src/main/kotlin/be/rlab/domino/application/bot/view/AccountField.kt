package be.rlab.domino.application.bot.view

import be.rlab.domino.domain.model.Account
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators
import be.rlab.tehanu.view.model.Field

/** Telegram field to display and select accounts.
 *
 * @param accounts Accounts to display.
 * @param description Field description.
 */
fun UserInput.account(
    accounts: List<Account>,
    description: String
): Field = field(description) {
    keyboard {
        lineLength(4)

        accounts.forEach { account ->
            button(account.owner.userName, account)
        }
    }

    validator { Validators.required("Seleccioná una cuenta de la lista") }
}
