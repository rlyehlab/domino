package be.rlab.domino.application.bot.view

import be.rlab.domino.domain.model.Currency
import be.rlab.domino.domain.model.Value
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.model.FieldGroup
import be.rlab.tehanu.view.model.number

/** Telegram field to ask for a [Value].
 *
 * It displays buttons with all fields from the [Currency] enumeration.
 */
fun UserInput.value(description: String): FieldGroup<Value> = fieldGroup {
    val amount: Double by number(description)
    val currency: Currency by currency("elegí la moneda")

    buildValue {
        Value.valueOf(currency, amount)
    }
}
