package be.rlab.domino.application.bot.bills

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.model.BillingMode
import be.rlab.domino.application.model.MemorySlots.SERVICE_CONFIGURATIONS
import be.rlab.domino.application.model.ServiceConfig
import be.rlab.domino.domain.TreasuryService
import be.rlab.domino.domain.model.Bill
import be.rlab.tehanu.store.Memory
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.model.ChatType
import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.messages.model.TextMessage
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

/** Displays the treasury bills.
 */
@MessageListener(Commands.BILLS)
class Bills(
    private val treasuryService: TreasuryService,
    memory: Memory
) {

    private var configurations: List<ServiceConfig> by memory.slot(SERVICE_CONFIGURATIONS, emptyList<ServiceConfig>())

    companion object {
        private val formatter: DateTimeFormatter = DateTimeFormat.forPattern("YYYY-MM")
    }

    @Handler(scope = [ChatType.GROUP])
    fun handle(
        context: MessageContext,
        message: Message
    ): MessageContext {
        val currentDate: DateTime = DateTime.now()
            .withZone(DateTimeZone.UTC)
            .withDayOfMonth(1)
            .withTimeAtStartOfDay()
        val date: DateTime = try {
            require(message is TextMessage)
            formatter.parseDateTime(message.text.substringAfter(" "))
                .withZone(DateTimeZone.UTC)
                .withTimeAtStartOfDay()
        } catch(cause: Exception) {
            currentDate
        }
        val bills: List<Bill> = treasuryService.listBills(date.year, date.monthOfYear)
        val paidBills: List<Bill> = bills.filter { bill ->
            val serviceConfig: ServiceConfig = configurations.find { serviceConfig ->
                serviceConfig.id == bill.creditor().id
            } ?: throw RuntimeException("Cannot find service with id: ${bill.creditor().id}")
            val hasPending: Boolean = bills.any { pendingBill ->
                pendingBill.pending && pendingBill.creditor() == bill.creditor()
            }

            !bill.pending && (serviceConfig.billingMode == BillingMode.MULTIPLE ||
                (serviceConfig.billingMode == BillingMode.SINGLE && !hasPending))
        }
        val paidBillsMessage: String = paidBills.joinToString("\n") { bill ->
            "[${bill.shortId()}] ${bill.creditor().userName}: ${bill.debt.value}"
        }

        // If a specified period is specified, it shows the pending bills from
        // that period, otherwise it displays all pending bills.
        val pendingBills: List<Bill> = if (date == currentDate) {
            treasuryService.listPendingBills()
        } else {
            bills.filter { bill ->
                !bill.pending
            }
        }
        val pendingBillsMessage: String = pendingBills.joinToString("\n") { bill ->
            val dueDate: String? = bill.dueDate?.toString(DateTimeFormat.forPattern("dd/MM/YYYY"))
            "[${bill.shortId()}] ${bill.creditor().userName}: ${bill.credit.debt} (vence $dueDate)"
        }

        return if (paidBills.isNotEmpty() || pendingBills.isNotEmpty()) {
            context.talk("# Facturas pendientes\n$pendingBillsMessage\n\n# Facturas pagas\n$paidBillsMessage")
        } else {
            context.talk("No tengo facturas para mostrar del período ${date.year}/${date.monthOfYear}")
        }
    }
}
