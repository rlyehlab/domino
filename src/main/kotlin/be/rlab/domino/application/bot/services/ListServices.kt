package be.rlab.domino.application.bot.services

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.model.AccountGroups.CREDITORS
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.model.Account
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.messages.model.ChatType

/** Displays the list of services.
 */
@MessageListener(Commands.LIST_SERVICES)
class ListServices(
    private val accountService: AccountService
) {

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext {
        val creditorsAccounts: List<Account> = accountService.findByGroup(CREDITORS)
        val serviceNames: String = creditorsAccounts.joinToString("\n") { account ->
            "${account.owner.userName} - ${account.owner.fullName}"
        }
        return context.talk(serviceNames)
    }
}
