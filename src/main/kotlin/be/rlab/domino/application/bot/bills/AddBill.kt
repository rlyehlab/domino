package be.rlab.domino.application.bot.bills

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.bot.view.account
import be.rlab.domino.application.bot.view.value
import be.rlab.domino.application.model.AccountGroups.CREDITORS
import be.rlab.domino.application.model.BillingMode
import be.rlab.domino.application.model.MemorySlots.SERVICE_CONFIGURATIONS
import be.rlab.domino.application.model.ServiceConfig
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.TreasuryService
import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Bill
import be.rlab.domino.domain.model.Value
import be.rlab.tehanu.store.Memory
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.model.ChatType
import be.rlab.tehanu.view.model.dateTime
import org.joda.time.DateTime

/** Charges a new bill to the treasury account.
 */
@MessageListener(Commands.ADD_BILL)
class AddBill(
    private val accountService: AccountService,
    private val treasuryService: TreasuryService,
    memory: Memory
) {

    private val configurations: List<ServiceConfig> by memory.slot(SERVICE_CONFIGURATIONS, emptyList<ServiceConfig>())
    private val creditorsAccounts: List<Account>
        get() = accountService.findByGroup(CREDITORS)

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext = context.userInput(
        "Este comando carga una nueva factura a la cuenta de la organización"
    ) {
        precondition(creditorsAccounts.isNotEmpty(), transition(Commands.CREATE_SERVICE))

        form {
            val serviceAccount: Account by account(creditorsAccounts,
                "Elegí el servicio para el que vas a cargar la factura. Si el servicio no existe " +
                "podés crearlo con /new_service"
            )

            val description: String by field("Escribí una descripcion de la factura")

            val dueDate: DateTime by dateTime(
                "cuál es la fecha de vencimiento de la factura? Escribila con el " +
                "siguiente formato: 17/09/2019"
            )

            val value: Value by value("cuál es el monto de la factura? ingresá un valor numérico")

            onSubmit {
                createBill(context, serviceAccount, description, dueDate, value)
            }
        }
    }

    private fun createBill(
        context: MessageContext,
        serviceAccount: Account,
        description: String,
        dueDate: DateTime,
        value: Value
    ) {
        val config: ServiceConfig = context.require(
            configurations.find { config ->
                config.id == serviceAccount.owner.id
            },
            "el servicio no está configurado, usá /configure_service para " +
            "establecer los parámetros de configuración"
        )

        val newBill: Bill = treasuryService.newBill(
            service = serviceAccount.owner,
            description = description,
            value = value,
            dueDate = dueDate,
            replace = config.billingMode == BillingMode.SINGLE
        )

        if (newBill.debt.value != value) {
            context.answer(
                "ya existe una factura anterior que está parcialmente paga, a la nueva " +
                "factura le resté el monto que ya pagaste y quedó cargada con un monto de ${newBill.debt.value}"
            )
        } else {
            context.answer("listo, la factura fue cargada en la cuenta del tesoro")
        }
    }
}
