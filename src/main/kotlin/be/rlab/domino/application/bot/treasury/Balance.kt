package be.rlab.domino.application.bot.treasury

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.model.BalanceDTO
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.BalanceService
import be.rlab.domino.domain.TreasuryConfig
import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Balance
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.messages.model.ChatType
import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.messages.model.TextMessage
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

/** Displays the treasury balance by account.
 */
@MessageListener(Commands.BALANCE)
class Balance(
    private val balanceService: BalanceService,
    private val accountService: AccountService,
    private val treasuryConfig: TreasuryConfig
) {

    companion object {
        private val formatter: DateTimeFormatter = DateTimeFormat.forPattern("YYYY-MM")
    }

    @Handler(scope = [ChatType.GROUP])
    fun handle(
        context: MessageContext,
        message: Message
    ): MessageContext {
        val date: DateTime = try {
            require(message is TextMessage)
            formatter.parseDateTime(message.text.substringAfter(" "))
                .withZone(DateTimeZone.UTC)
                .withTimeAtStartOfDay()
        } catch(cause: Exception) {
            DateTime.now()
                .withZone(DateTimeZone.UTC)
                .withDayOfMonth(1)
                .withTimeAtStartOfDay()
        }
        val balance: Balance = balanceService.findForTreasury(
            year = date.year,
            month = date.monthOfYear
        )
        val result: BalanceDTO = BalanceDTO.from(balance)

        context.talk("""
            # Balance de período ${date.year}/${date.monthOfYear}

            Total de deudas: ${result.debtsTotal}
            Deudas pendientes: ${result.debtsPending}
            Total de ingresos: ${result.income}

            Balance anterior: ${result.previousBalance}
            Balance del mes: ${result.monthlyBalance}
            Balance total: ${result.totalBalance}
        """.trimIndent())

        context.talk("# Balance actual de cuentas\n${accountsBalance()}")
        context.talk("https://domino.rlab.be/balance?year=${date.year}&month=${date.monthOfYear}")

        return context
    }

    private fun accountsBalance(): String {
        val accounts: List<Account> = accountService.findByGroup(treasuryConfig.group)
        return accounts.joinToString("\n") { account ->
            "${account.owner.userName}: ${-account.debt}"
        }
    }
}
