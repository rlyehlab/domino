package be.rlab.domino.application.bot.bills

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.bot.view.bill
import be.rlab.domino.domain.TreasuryService
import be.rlab.domino.domain.model.Bill
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.model.ChatType

/** Cancels an existing bill.
 */
@MessageListener(Commands.CANCEL_BILL)
class CancelBill(private val treasuryService: TreasuryService) {

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext = context.userInput(
        "Este comando cancela una factura pendiente y crea una nota de " +
        "crédito en la cuenta de la organización"
    ) {
        val pendingBills: List<Bill> = treasuryService.listPendingBills()
        require(pendingBills.isNotEmpty()) { "No hay facturas pendientes" }

        val selectedBill: Bill by bill(pendingBills, "seleccioná la factura que querés cancelar")

        onSubmit {
            cancelBill(context, selectedBill)
        }
    }

    private fun cancelBill(
        context: MessageContext,
        bill: Bill
    ) {
        if (bill.credit.debt == -bill.credit.income.value) {
            treasuryService.cancelBill(bill)
            context.answer("listo, cancelé la factura")
        } else {
            context.answer("no podés cancelar una factura que ya está parcialmente paga")
        }
    }
}
