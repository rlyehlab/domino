package be.rlab.domino.application.bot.donations

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.model.AccountGroups.SUPPORTERS
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.AgentService
import be.rlab.domino.domain.TreasuryConfig
import be.rlab.domino.domain.model.Agent
import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.messages.model.ChatType

/** Creates an account in the organization for an external agent.
 */
@MessageListener(Commands.CREATE_SUPPORTER)
class CreateSupporter(
    private val agentService: AgentService,
    private val accountService: AccountService,
    private val accessControl: AccessControl,
    private val treasuryConfig: TreasuryConfig
) {

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext = context.userInput(
        "Las cuentas de donantes las usamos para llevar un registro de las donaciones y para " +
        "comunicarnos con les donantes pero no forman parte del grupo del lab."
    ) {
        val userName: String by field(
            "Decime el nombre de usuario de la nueva cuenta (si está en telegram, nombre de usuario de telegram)"
        ) {
            validator { userName ->
                val resolvedName: String = userName.substringAfter("@")
                require(!agentService.exists(resolvedName)) {
                    "ya existe una persona con el nombre de usuario @${resolvedName}, elegí otro nombre"
                }
            }
        }

        onSubmit {
            createAgent(context, userName.substringAfter("@"))
        }
    }

    private fun createAgent(
        context: MessageContext,
        userName: String
    ) {
        val userId: Long = accessControl.findUserByName(userName)?.id
            ?: System.currentTimeMillis()

        val owner: Agent = agentService.findByUserId(
            userId = userId
        ) ?: agentService.create(
            priority = treasuryConfig.priority,
            userId = userId,
            userName = userName,
            fullName = userName
        )

        accountService.create(
            group = SUPPORTERS,
            userId = owner.userId
        )

        context.answer("listo, creé la cuenta de @$userName")
    }
}
