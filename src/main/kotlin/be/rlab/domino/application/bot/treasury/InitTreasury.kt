package be.rlab.domino.application.bot.treasury

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.TreasuryConfig
import be.rlab.domino.domain.model.Account
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.messages.model.ChatType

/** Initializes the treasury account if it does not exist.
 */
@MessageListener(Commands.INIT_TREASURY)
class InitTreasury(
    private val accountService: AccountService,
    private val treasuryConfig: TreasuryConfig
) {

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext = context.userInput(
        "Este comando inicializa la cuenta principal del tesoro"
    ) {

        val treasuryAccount: Account? = accountService.findTreasuryAccount()

        require(treasuryAccount == null) { createInfoMessage(treasuryAccount!!) }

        val userName: String by field("elegí un nombre de usuaria para la cuenta del tesoro")
        val description: String by field("escribí una breve descripción de la cuenta del tesoro")

        onSubmit {
            createAccount(context, userName, description)
        }
    }

    private fun createAccount(
        context: MessageContext,
        userName: String,
        description: String
    ) {
        context.talk(
            createInfoMessage(accountService.createTreasuryAccount(
                group = treasuryConfig.group,
                userId = System.currentTimeMillis(),
                userName = userName,
                fullName = description
            ))
        )
    }

    private fun createInfoMessage(account: Account): String {
        return """
            La cuenta de la organización tiene los siguientes datos:

            owner: ${account.owner.fullName} (@${account.owner.userName})
            account_id: ${account.id}
            group: ${treasuryConfig.group}
            initial_signature: ${account.initialSignature}
        """.trimIndent()
    }
}
