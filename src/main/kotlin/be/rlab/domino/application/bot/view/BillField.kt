package be.rlab.domino.application.bot.view

import be.rlab.domino.domain.model.Bill
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators
import be.rlab.tehanu.view.model.Field

/** Telegram field to display and select bills.
 *
 * @param bills Bills to display.
 * @param description Field description.
 */
fun UserInput.bill(
    bills: List<Bill>,
    description: String
): Field = field(description) {
    keyboard {
        lineLength(1)
        bills.forEach { pendingBill ->
            button("${pendingBill.creditor().userName} - ${pendingBill.description}", pendingBill)
        }
    }

    validator {
        Validators.required("Seleccioná una factura de la lista")
    }
}
