package be.rlab.domino.application.bot.treasury

import be.rlab.domino.application.bot.view.account
import be.rlab.domino.application.bot.view.value
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.TreasuryConfig
import be.rlab.domino.domain.TreasuryService
import be.rlab.domino.domain.model.Account
import be.rlab.domino.domain.model.Value
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.messages.model.ChatType

/** Makes transfers from a source account to a destination account.
 */
class Transfer(
    private val accountService: AccountService,
    private val treasuryService: TreasuryService,
    private val treasuryConfig: TreasuryConfig
) {

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext = context.userInput(
        "Este comando hace una transferencia entre dos cuentas"
    ) {
        val accounts: List<Account> = accountService.findByGroup(treasuryConfig.group)
        require(accounts.isNotEmpty()) {
            "No hay cuentas en la organización, para crear tu cuenta usá el comando /new_account"
        }

        val sourceAccount: Account by account(accounts, "elegí la cuenta de origen")
        val targetAccount: Account by account(accounts, "elegí la cuenta de destino")
        val value: Value by value("qué monto querés transferir?")

        onSubmit {
            newTransfer(context, sourceAccount, targetAccount, value)
        }
    }

    private fun newTransfer(
        context: MessageContext,
        sourceAccount: Account,
        targetAccount: Account,
        value: Value
    ) {
        treasuryService.newTransfer(
            source = sourceAccount.owner,
            destination = targetAccount.owner,
            value = value
        )

        context.answer("listo, el dinero quedó transferido a la cuenta de @${targetAccount.owner.userName}")
    }
}
