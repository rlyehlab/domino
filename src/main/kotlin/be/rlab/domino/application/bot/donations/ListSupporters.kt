package be.rlab.domino.application.bot.donations

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.model.AccountGroups.SUPPORTERS
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.model.Account
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.messages.model.ChatType

/** Displays the list of supporters.
 */
@MessageListener(Commands.LIST_SUPPORTERS)
class ListSupporters(
    private val accountService: AccountService
) {

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext {
        val creditorsAccounts: List<Account> = accountService.findByGroup(SUPPORTERS)
        val serviceNames: String = creditorsAccounts.joinToString(", ") { account ->
            account.owner.userName
        }
        return context.talk(serviceNames)
    }
}
