package be.rlab.domino.application.bot.treasury

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.domain.AccountService
import be.rlab.domino.domain.AgentService
import be.rlab.domino.domain.TreasuryConfig
import be.rlab.domino.domain.model.Agent
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.model.ChatType
import be.rlab.tehanu.messages.model.User

/** Creates an account in the organization.
 *
 * It creates the account for the user issuing the command.
 */
@MessageListener(Commands.CREATE_ACCOUNT)
class CreateAccount(
    private val agentService: AgentService,
    private val accountService: AccountService,
    private val treasuryConfig: TreasuryConfig
) {

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext {
        return context.user?.let { user ->
            accountService.findDefaultByUserId(user.id)?.let {
                context.answer("tu cuenta ya existe!")
            } ?: createAccount(context, user)
        } ?: context.answer("no puedo obtener información de tu usuario de Telegram para crear la cuenta")
    }

    private fun createAccount(
        context: MessageContext,
        user: User
    ): MessageContext {
        val owner: Agent = agentService.findByUserId(
            userId = user.id
        ) ?: agentService.create(
            priority = treasuryConfig.priority,
            userId = user.id,
            userName = user.userName ?: "minion",
            fullName = "${user.firstName} ${user.lastName}"
        )

        accountService.create(
            group = treasuryConfig.group,
            userId = owner.userId
        )

        return context.answer("listo, creé tu cuenta, ya podés operar, buena suerte!")
    }
}
