package be.rlab.domino.application.bot.bills

import be.rlab.domino.application.bot.Commands
import be.rlab.domino.application.bot.view.bill
import be.rlab.domino.domain.AgentService
import be.rlab.domino.domain.TreasuryService
import be.rlab.domino.domain.model.Agent
import be.rlab.domino.domain.model.Bill
import be.rlab.domino.domain.model.Value
import be.rlab.tehanu.messages.MessageContext
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.MessageListener
import be.rlab.tehanu.messages.model.ChatType
import be.rlab.tehanu.view.model.number

/** Pays a pending bill from the user's account.
 */
@MessageListener(Commands.PAY_BILL)
class PayBill(
    private val agentService: AgentService,
    private val treasuryService: TreasuryService
) {

    @Handler(scope = [ChatType.GROUP])
    fun handle(context: MessageContext): MessageContext = context.userInput {
        val pendingBills: List<Bill> = treasuryService.listPendingBills()
        require(pendingBills.isNotEmpty()) { "No tengo facturas pendientes :O" }

        val selectedBill: Bill by bill(pendingBills, "seleccioná la factura que querés pagar")
        val amount: Double by number(
            "qué monto de la factura pagaste?",
            minValue = 1
        ).apply {
            keyboard {
                button("Pagué el total", -1)
            }
        }

        onSubmit {
            payBill(context, selectedBill, Value.valueOf(selectedBill.debt.value.currency, amount))
        }
    }

    private fun payBill(
        context: MessageContext,
        bill: Bill,
        value: Value
    ) {
        val userName: String = context.require(
            context.user?.userName,
            "No tenés configurado un nombre de usuario, necesito que tengas nombre " +
            "de usuario para pagar una factura"
        )
        val payer: Agent = context.require(
            agentService.findByUserName(userName),
            "Tu cuenta no existe, podés crearla usando /new_account"
        )

        if (!bill.pending) {
            context.answer("la factura ${bill.id} ya está paga")
        } else {
            val resolvedValue: Value = if (value.amount.toInt() == -1) {
                bill.debt.value
            } else {
                value
            }

            treasuryService.payBill(
                payer = payer,
                bill = bill,
                value = resolvedValue
            )

            context.answer("listo, la factura quedó pagada")
        }
    }
}
