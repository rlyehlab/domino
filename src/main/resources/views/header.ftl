<nav>
    <div class="nav-wrapper">
        <a href="${base_path}/" class="brand-logo"><img src="/static/images/domino_logo.png">${title}</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li <#if title == "Domino">class="active"</#if>><a href="/">Inicio</a></li>
            <li <#if title == "Balances">class="active"</#if>><a href="/balance">Balances</a></li>
            <li <#if title == "Protocolo">class="active"</#if>><a href="/protocol">Protocolo</a></li>
        </ul>
    </div>
</nav>
