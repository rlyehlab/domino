<!DOCTYPE html>
<html>
    <#include "top.ftl">
    <body>
        <div class="js-container main">
            <#include "header.ftl" >

            <div class="content">
                <p>
                    Las finanzas del lab se gestionan de forma distribuída. La contabilidad la realiza
                    <a href="https://git.rlab.be/seykron/domino">Domino</a>, las personas solamente debemos realizar
                    tareas de gestión. Las tareas manuales de gestión son las siguientes:
                </p>
                <ul class="browser-default">
                    <li>Recibir donaciones</li>
                    <li>Contar el dinero del tarro</li>
                    <li>Pagar facturas de servicios</li>
                    <li>Comprar artículos necesarios para el espacio</li>
                </ul>
                <h3>Reglas generales</h3>
                <ul class="browser-default">
                    <li>Todas las personas que realizan tareas de gestión deben estar en el grupo de Telegram de finanzas</li>
                    <li>Los roles son rotativos, cualquiera puede hacer cualquier cosa y cuantas más personas estén
                        involucradas en la gestión menos tareas le tocará a cada una</li>
                    <li>Nadie realiza operaciones que requieren dineros sin tener el dinero</li>
                    <li>Cualquier duda o inconveniente se debe consultar con la coordinadora</li>
                </ul>

                <h3>Coordinadora</h3>
                <p>
                    Existe una persona responsable de coordinar todas las tareas. La coordinadora tiene las siguientes
                    responsabilidades:
                </p>
                <ul class="browser-default">
                    <li>Delegar las tareas mencionadas arriba a otras personas</li>
                    <li>Garantizar que no quedan deudas en el mes</li>
                    <li>Publicar los balances mensuales en las redes sociales</li>
                    <li>Resolver cualquier inconveniente con el pago o facturación de servicios</li>
                </ul>

                <h3>Recibir donaciones</h3>
                <p>
                    El protocolo es el siguiente:
                </p>
                <ol class="browser-default">
                    <li>Recibir el dinero de la donante</li>
                    <li>Informar a Domino en el canal de finanzas usando el comando /new_donation</li>
                    <li>Si la persona donante no está cargada, hay que darla de alta con el comando /new_supporter</li>
                </ol>
                <p>
                    El dinero queda cargado en la cuenta de la persona que recibe la donación. Si la donación quedó en
                    el tarro, hay que hacer un paso adicional:
                </p>
                <ul class="browser-default">
                    <li>Transferir el dinero a la cuenta del tarro con el comando /new_transfer</li>
                </ul>


                <h3>Contar el dinero del tarro</h3>
                <p>
                    El dinero del tarro cuenta como donaciones anónimas. Hay un usuario <em>tarro</em> cargado en
                    Domino para reportar las donaciones del tarro. El protocolo es el siguiente:
                </p>
                <ol>
                    <li>Contar y sacar el dinero del tarro</li>
                    <li>Cargar una donación del usario <em>tarro</em> usando el comando /new_donation</li>
                </ol>
                <p>
                    El dinero queda cargado en la cuenta de la persona que recibe la donación.
                </p>

                <h3>Pagar facturas de servicios</h3>
                <p>
                    Asumimos que los servicios ya están cargados en Domino. El protocolo es el siguiente:
                </p>
                <ol>
                    <li>Asegurarse que la factura no está cargada y no está paga usando el comando /bills</li>
                    <li>Cargar la factura en Domino usando el comando /new_bill y mandar foto de la factura al grupo de finanzas</li>
                    <li>Si es necesario, pedirle a la coordinadora que le gire el dinero para pagar la factura</li>
                    <li>Pagar la factura por cualquier medio de pago</li>
                    <li>Informarle el pago a Domino usando el comando /pay_bill</li>
                </ol>
                <p>
                    Las facturas deben cargarse siempre con la fecha del primer vencimiento.
                </p>
                <p>
                    Para verificar si una factura está cargada usando el comando /bills se debe observar el nombre
                    del servicio, el monto de la factura y la fecha de vencimiento.
                </p>
            </div>
        </div>

        <#include "footer.ftl">
    </body>
</html>
