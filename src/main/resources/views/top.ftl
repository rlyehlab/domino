<head>
    <title>${title} - Domino - sistema de finanzas de R'lyeh basado en la desconfianza</title>
    <link rel="stylesheet" href="/webjars/materializecss/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="/static/css/fonts.css">
    <link rel="stylesheet" href="/static/css/domino.css">
</head>
