<!DOCTYPE html>
<html>
    <#include "top.ftl">
    <body>
        <div class="js-container main">
            <#include "header.ftl" >

            <div class="content">
                <p>
                    Los balances los cierro el último día del mes y los calculo tomando todas las transacciones
                    que se hicieron en las cuentas de la organización durante el período. Todas las facturas que carguen
                    y transacciones que realicen después del último día del mes las voy a considerar para el mes siguiente.
                </p>
                <table>
                    <tbody>
                    <tr class="balance-period">
                        <td>
                            <div class="row">
                                <form class="col s12" action="/balance" method="get">
                                    <span>Período:</span>
                                    <div class="input-field inline">
                                        <select name="year">
                                            <option value="2019" <#if year == 2019>selected</#if>>2019</option>
                                            <option value="2020" <#if year == 2020>selected</#if>>2020</option>
                                        </select>
                                        <label>Año</label>
                                    </div>
                                    <div class="input-field inline">
                                        <select name="month">
                                            <option value="1" <#if month == 1>selected</#if>>Enero</option>
                                            <option value="2" <#if month == 2>selected</#if>>Febrero</option>
                                            <option value="3" <#if month == 3>selected</#if>>Marzo</option>
                                            <option value="4" <#if month == 4>selected</#if>>Abril</option>
                                            <option value="5" <#if month == 5>selected</#if>>Mayo</option>
                                            <option value="6" <#if month == 6>selected</#if>>Junio</option>
                                            <option value="7" <#if month == 7>selected</#if>>Julio</option>
                                            <option value="8" <#if month == 8>selected</#if>>Agosto</option>
                                            <option value="9" <#if month == 9>selected</#if>>Septiembre</option>
                                            <option value="10" <#if month == 10>selected</#if>>Octubre</option>
                                            <option value="11" <#if month == 11>selected</#if>>Noviembre</option>
                                            <option value="12" <#if month == 12>selected</#if>>Diciembre</option>
                                        </select>
                                        <label>Mes</label>
                                    </div>
                                    <button class="btn waves-effect waves-light inline" type="submit">Ver Balance
                                        <i class="material-icons right"><!-- --></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Total de deudas</td>
                        <td>${balance.debtsTotal}</td>
                    </tr>
                    <tr>
                        <td>
                            <table class="services striped">
                                <thead>
                                    <tr>
                                        <th>Servicio</th>
                                        <th>Total</th>
                                        <th>Pendiente</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <#list balance.bills as bill>
                                <tr class="<#if bill.pending>red lighten-4</#if>">
                                    <td>${bill.serviceName}</td>
                                    <td>${bill.total}</td>
                                    <td>${bill.debt}</td>
                                </tr>
                                </#list>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>Deudas pendientes</td>
                        <td>${balance.debtsPending}</td>
                    </tr>
                    <tr>
                        <td>Total de ingresos</td>
                        <td>${balance.income}</td>
                    </tr>
                    <tr>
                        <td>Balance anterior</td>
                        <td>${balance.previousBalance}</td>
                    </tr>
                    <tr>
                        <td>Balance del mes</td>
                        <td>${balance.monthlyBalance}</td>
                    </tr>
                    <tr class="blue-grey lighten-5">
                        <td>Balance total</td>
                        <td>${balance.totalBalance}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <#include "footer.ftl">

        <script type="text/javascript" nonce="VKjLAp03xTourLza6Zs6mA==">
            jQuery(() => {
                let balance = new Balance(jQuery(".js-container"));
                balance.render();
            });
        </script>
    </body>
</html>
