<!DOCTYPE html>
<html>
    <#include "top.ftl">
    <body>
        <div class="js-container main">
            <#include "header.ftl" >

            <div class="content">
                <p>
                    Hola! Soy <a href="https://git.rlab.be/seykron/domino">Domino</a>, y mi poder es la gestión
                    financiera distribuida basada en la desconfianza. Yo me encargo de toda la parte contable para que
                    las personas humanas solamente realicen pequeñas tareas de gestión.
                </p>
                <h3>¿Por qué desconfiar?</h3>
                <p>
                    Un proverbio ruso dice que hay que <em>confiar desconfiando (Doveryai, no proveryai)</em>. Por más
                    buena voluntad que tengamos las personas, cometemos errores, y los errores en una organización son
                    una causa de conflicto. Si los errores se pueden identificar, auditar y resolver, se convierten en
                    una instancia de aprendizaje. Nadie quiere administrar las finanzas de una organización. Nadie
                    quiere administrar finanzas. Normalmente se le delega la responsabilidad a una o dos personas y
                    nadie audita lo que pasa hasta que hay un error, y entonces empiezan los conflictos.
                </p>
                <p>
                    La respuesta a la desconfianza es la transaparencia. Los mecanismos de transparencia no solamente
                    hacen que los errores sean auditables, sino que también le da la seguridad a las personas con
                    responsabilidad para que pueden equivocarse sin ser juzgadas de mala fe por ello. Así se transforman
                    las relaciones de competencia en relaciones de colaboración.
                </p>
                <h3>Las finanzas de Cthulhu</h3>
                <p>
                    Las tareas más relevantes de gestión del hacklab son recibir donaciones, pagar facturas y comprar
                    artículos corrientes. Para que ninguna persona se canse y termine odiando a Cthulhu las tareas de
                    gestión son rotativas y distribuídas. Tratamos de que haya varias personas involucradas en cada
                    tarea para que la responsabilidad también esté distribuída y cada una haga pequeñas partes de la
                    tarea.
                </p>
                <p>
                    Si querés saber más cómo es el proceso de gestión financiera del hacklab, podés leer el
                    <a href="/protocol">Protocolo de los Dineros de Cthulhu</a>.
                </p>
            </div>
        </div>

        <#include "footer.ftl">
    </body>
</html>
