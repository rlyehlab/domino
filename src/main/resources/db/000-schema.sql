CREATE TABLE IF NOT EXISTS `memory_slots` (
  `id` binary(16) NOT NULL,
  `slot_name` varchar(50) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `treasury_agents` (
  `id` binary(16) NOT NULL,
  `priority` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `treasury_accounts` (
  `id` binary(16) NOT NULL,
  `group` varchar(100) NOT NULL,
  `owner` binary(16) NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `currency_name` int(11) NOT NULL,
  `debt` decimal(15,3) NOT NULL,
  `initial_signature` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `treasury_accounts_initial_signature_unique` (`initial_signature`),
  CONSTRAINT `fk_treasury_accounts_owner_id` FOREIGN KEY (`owner`) REFERENCES `treasury_agents` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `treasury_signatures` (
  `id` binary(16) NOT NULL,
  `creation_date` timestamp(6) NOT NULL,
  `previous_signature` varchar(64) NOT NULL,
  `signature` varchar(64) NOT NULL,
  `account` binary(16) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `treasury_signatures_signature_unique` (`signature`),
  CONSTRAINT `fk_treasury_signatures_account_id` FOREIGN KEY (`account`) REFERENCES `treasury_accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `treasury_balances` (
  `id` binary(16) NOT NULL,
  `creation_date` timestamp(6) NOT NULL,
  `signature` binary(16) NOT NULL,
  `account` binary(16) NOT NULL,
  `description` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `from` timestamp(6) NOT NULL,
  `to` timestamp(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_treasury_balances_signature_id` (`signature`),
  KEY `fk_treasury_balances_account_id` (`account`),
  KEY `treasury_balances_creation_date` (`creation_date`),
  KEY `treasury_balances_from` (`from`),
  KEY `treasury_balances_to` (`to`),
  CONSTRAINT `fk_treasury_balances_account_id` FOREIGN KEY (`account`) REFERENCES `treasury_accounts` (`id`),
  CONSTRAINT `fk_treasury_balances_signature_id` FOREIGN KEY (`signature`) REFERENCES `treasury_signatures` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `treasury_bills` (
  `id` binary(16) NOT NULL,
  `creation_date` timestamp(6) NOT NULL,
  `signature` binary(16) NOT NULL,
  `account` binary(16) NOT NULL,
  `description` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `is_pending` tinyint(1) NOT NULL,
  `creditor` binary(16) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_treasury_bills_signature_id` (`signature`),
  KEY `fk_treasury_bills_account_id` (`account`),
  KEY `fk_treasury_bills_creditor_id` (`creditor`),
  KEY `treasury_bills_creation_date` (`creation_date`),
  CONSTRAINT `fk_treasury_bills_account_id` FOREIGN KEY (`account`) REFERENCES `treasury_accounts` (`id`),
  CONSTRAINT `fk_treasury_bills_creditor_id` FOREIGN KEY (`creditor`) REFERENCES `treasury_agents` (`id`),
  CONSTRAINT `fk_treasury_bills_signature_id` FOREIGN KEY (`signature`) REFERENCES `treasury_signatures` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `treasury_credits` (
  `id` binary(16) NOT NULL,
  `creation_date` timestamp(6) NOT NULL,
  `signature` binary(16) NOT NULL,
  `account` binary(16) NOT NULL,
  `description` varchar(255) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_treasury_credits_signature_id` (`signature`),
  KEY `fk_treasury_credits_account_id` (`account`),
  KEY `treasury_credits_creation_date` (`creation_date`),
  CONSTRAINT `fk_treasury_credits_account_id` FOREIGN KEY (`account`) REFERENCES `treasury_accounts` (`id`),
  CONSTRAINT `fk_treasury_credits_signature_id` FOREIGN KEY (`signature`) REFERENCES `treasury_signatures` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `treasury_transactions` (
  `id` binary(16) NOT NULL,
  `creation_date` timestamp(6) NOT NULL,
  `signature` binary(16) NOT NULL,
  `account` binary(16) NOT NULL,
  `description` varchar(255) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_treasury_transactions_signature_id` (`signature`),
  KEY `fk_treasury_transactions_account_id` (`account`),
  KEY `treasury_transactions_creation_date` (`creation_date`),
  CONSTRAINT `fk_treasury_transactions_account_id` FOREIGN KEY (`account`) REFERENCES `treasury_accounts` (`id`),
  CONSTRAINT `fk_treasury_transactions_signature_id` FOREIGN KEY (`signature`) REFERENCES `treasury_signatures` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
