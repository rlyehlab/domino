CREATE TABLE IF NOT EXISTS `treasury_credit_notes` (
  `id` binary(16) NOT NULL,
  `creation_date` timestamp(6) NOT NULL,
  `signature` binary(16) NOT NULL,
  `account` binary(16) NOT NULL,
  `description` varchar(255) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_treasury_credit_notes_signature_id` (`signature`),
  KEY `fk_treasury_credit_notes_account_id` (`account`),
  KEY `treasury_credits_creation_date` (`creation_date`),
  CONSTRAINT `fk_treasury_credit_notes_account_id` FOREIGN KEY (`account`) REFERENCES `treasury_accounts` (`id`),
  CONSTRAINT `fk_treasury_credit_notes_signature_id` FOREIGN KEY (`signature`) REFERENCES `treasury_signatures` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE treasury_transactions ADD COLUMN IF NOT EXISTS `is_invalid` tinyint(1) NOT NULL DEFAULT 0;
