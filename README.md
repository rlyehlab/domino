# Domino

*Porque las finanzas no son un superpoder*

![Domino](domino.jpg)

Domino es un sistema colaborativo de gestión de dineros basado en la desconfianza.

## Inicio rápido

Primero setear las siguientes variables en el `.env`:
```
bot_access_token=access token del bot de Telegram que provee @BotFather
bot_handle=@HandleDelBot
bot_default_admin=id numérico de usuarie de telegram admin del bot (preguntar al bot @userinfobot)
```

Segundo compilar la aplicación y buildear la imagen de docker con:

`mvn clean install`

Tercero levantar el compose:

`docker-compose up`

Finalmente navegar a http://localhost:8080/
e invitar al bot a un grupo y ejecutar alguno de sus comandos (por ejemplo `/init_treasury`)

## ¿Por qué desconfiar?

Un proverbio ruso dice que *hay que confiar desconfiando* (*Doveryai, no proveryai*). Por más buena voluntad que
tengamos las personas, cometemos errores, y los errores en una organización son una causa de conflicto. Si los errores
se pueden identificar, auditar y resolver, se convierten en una instancia de aprendizaje.

Nadie quiere administrar las finanzas de una organización. Nadie quiere administrar finanzas. Normalmente se le delega
la responsabilidad a una o dos personas y nadie audita lo que pasa hasta que hay un error, y entonces empiezan los
conflictos.

La respuesta a la desconfianza es la transaparencia. Los mecanismos de transparencia no solamente hacen que los errores
sean auditables, sino que también le da la seguridad a las personas con responsabilidad que pueden equivocarse sin
ser juzgadas de mala fe por ello. Así se transforman las relaciones de *competencia* en relaciones de *colaboración*.
 
## Arquitectura de Domino

Domino cuenta con los siguientes componentes:

* Un **sistema contable** que permite el asiento de todas las transacciones que se realizan dentro y fuera de
la organización.
* Un **bot de Telegram** (Dominah) que hace de front-end para operar el sistema contable.
* Un frontend para ver los balances de cada período.

### Sistema contable

El sistema contable permite el manejo distribuído de dineros. Todas las personas (físicas y jurídicas) son
[agentes](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/domain/model/Agent.kt) dentro
del sistema. Los agentes tienen
[cuentas](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/domain/model/Account.kt), y
cualquier
[transacción](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/domain/model/Transaction.kt)
que se realice en el sistema queda asociada a una cuenta.

#### Cuentas

Todas las [cuentas](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/domain/model/Account.kt)
pertenecen a un grupo. Los grupos representan a un conjunto de cuentas que comparten el mismo balance. Los grupos que
usamos en el sistema contable del lab son los siguientes:

* La organización (*rlyeh*). Este grupo es obligatorio y forma parte de la
[configuración estática](https://git.rlab.be/seykron/domino/blob/master/src/main/resources/application.conf)
de la aplicación.
* Donantes (*supporters*). Personas que hacen aportes económicos a la organización.
* Servicios (*expenses*). Servicios públicos como agua, luz, etc.

Por conveniencia todas las cuentas mantienen actualizado su balance. Cada transacción que se realiza actualiza
el balance de la cuenta afectada. Para calcular el
[balance total](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/Balance.kt)
de un grupo simplemente se debe sumar el balance de todas las cuentas del mismo grupo.

Un agente puede tener múltiples cuentas, por eso las cuentas tienen una marca para indicar si es la cuenta
predeterminada para un agente. En el modelo contable del hacklab no utilizamos múltiples cuentas por agente.

#### Transacciones

El [servicio de transacciones](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/domain/TransactionService.kt) 
permite realizar las siguientes transacciones:

|   Tipo    | Transacciones |
|-----------|---------------|
| Ingreso   | Depósito, Transferencia, Crédito
| Egreso    | Deuda

La suma del valor de todas las transacciones constituye el balance de una cuenta. Para evitar que las transacciones
sean adulteradas (por ejemplo, por un acceso ilegítimo a la base de datos), tienen una firma que se calcula utilizando
los datos de la transacción y la firma de la transacción anterior. De este modo es posible verificar la integridad de
una cuenta tomando un conjunto arbitrario de transacciones y calculando nuevamente la firma de cada una.

En la misma línea, cuando se lee una transacción de la base de datos se verifica la integridad de la misma. De este modo
si se adulteró una transacción específica el error no se propaga a las siguientes transacciones. La firma
[se calcula](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/util/SignatureUtils.kt)
concatenando los siguientes datos de la transacción y generando un hash SHA-256:

```
sha256(previousSignature!#creationDate!#accountId!#description!#type!#value)
```

Las transacciones tienen un mecanismo de *peer review*. Cuando se crean quedan pendientes de confirmación y un tercero
debe confirmarlas. Esto es útil para validar que los valores que se reportan son correctos y por lo tanto la
consolidación de los activos disponibles sea consistente. Por ejemplo, si A le hace una transferencia de dineros a B,
entonces C tiene que confirmar la transferencia preguntandole a B si efectivamente recibió esos dineros.

#### Créditos

Los [créditos](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/domain/model/Credit.kt)
representan [cuentas por cobrar](https://efxto.com/diccionario/cuentas-por-cobrar-ar). Por ejemplo, cuando los servicios
como luz, agua, etc., emiten una factura, esto genera un ingreso para la empresa en forma de crédito y una deuda en la
cuenta cliente.

Otro caso común de crédito es cuando una persona presta dinero para pagar alguna deuda de la organización. Esto
genera una deuda en la cuenta de la organización y un crédito en la cuenta de la persona que prestó el dinero. Los
créditos cuentan como *ingresos* en las cuentas, independientemente de cuándo se reciba el pago de los mismos.

En este sistema contable todos los créditos son *parcialmente cancelables*, es decir, que del monto total del crédito
se puede pagar en cualquier cantidad de veces y sin montos fijos.

#### Facturas

Las [facturas](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/domain/model/Bill.kt)
son créditos a favor de las cuentas de las empresas de servicio y deudas para la organización. Para cargar una factura
primero se tiene que dar de alta la cuenta de la empresa de servicio.

Al pagar una factura desde una cuenta del grupo de la organización, esto genera una transferencia del monto pagado a la
cuenta de la organización, y cancela el monto pagado en el crédito de la empresa de servicio. Cuando se termina de pagar
el monto total de la factura, la deuda que se creó en la cuenta de la organización queda saldada, y el crédito de la
empresa de servicio queda cancelado.

Los servicios que emiten facturas tienen dos modos de facturación:

* Total: cada factura tiene el monto de toda la deuda pendiente
* Mensual: cada factura tiene el monto del último período de facturación

Las facturas *totales* siempre reemplazan a la factura anterior del mismo servicio. Si existe una factura anterior con
un pago parcial, se asume que la nueva factura va a tener el monto de la factura anterior entonces sólo se carga la
diferencia entre el monto nuevo y el pago parcial existente.

Las facturas *mensuales* siempre generan una nueva factura cada vez que se cargan.

#### Notas de crédito

Las [Notas de Crédito](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/domain/model/CreditNote.kt)
se generan cuando se debe invalidar una transacción. Las transacciones nunca se borran, pero se pueden invalidar. Las
transacciones inválidas no se computan en los balances.

Actualmente solamente se generan notas de crédito para cancelar una factura mal cargada.

### Bot de Telegram

Decidimos utilizar un bot de Telegram porque es lo más simple y accesible para todas las personas que vamos a usar
este sistema. Las interfaces gráficas pueden ser útiles para visualizar datos, pero para las operaciones del día a día
son poco prácticas porque nos fuerzan a mantener abierto un sitio web en una solapa del browser, y lidiar con todos
los problemas de seguridad que eso implica. En el caso de las interfaces desktop, hay que estar cerca de una computadora
desktop, cosa que no ocurre siempre.

*Domina* es un bot de Telegram basado en [Tehanu](https://git.rlab.be/seykron/tehanu/). Los comandos que reciben
input lo esperan en formato YAML ya que es fácil de escribir por una persona. Los comandos tienen ayuda contextual al
momento de usarlos y guian a las usuarias para utilizarlos correctamente. Al momento, *Domina* soporta los siguientes
comandos:

[/init_treasury](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/InitTreasury.kt):
Crea y configura la cuenta de la organización. El grupo para las cuentas de la organización es parte de la configuración
estática de la aplicación. Este es el primer comando que debería ejecutarse. Las siguientes llamadas muestra información
de la cuenta de la organización.

[/new_account](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/CreateAccount.kt):
Si no existe, crea la cuenta de la usuaria de Telegram que use el comando.

[/new_service](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/CreateService.kt)
Si no existe, crea la cuenta de un servicio o acreedor. Las cuentas de servicios pertenecen al grupo *creditor*.

[/edit_service](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/EditService.kt)
Cambia el modo de facturación de un servicio. El modo de facturación define si las facturas acumulan toda la deuda
anterior o se emite solamente el monto de deuda para el período de la factura.

[/new_supporter](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/CreateSupporter.kt)
Si no existe, crea la cuenta de un donante. Las cuentas de donantes pertenecen al grupo *supporter*.

[/new_donation](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/AddDonation.kt):
Registra una donación y deposita el dinero en la cuenta de la usuaria que usa el comando.

[/new_transfer](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/Transfer.kt):
Realiza una transferencia de dineros entre dos cuentas existentes.

[/new_deposit](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/Deposit.kt):
Realiza un depósito en la cuenta de la persona que lo use.

[/new_bill](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/AddBill.kt)
Carga una nueva factura de servicio a la cuenta del tesoro.

[/pay_bill](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/PayBill.kt)
Paga una factura pendiente.

[/bills](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/Bills.kt)
Muestra la lista de facturas pendientes.

[/balance](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/command/Balance.kt):
Muestra el balance de todas las cuentas de la organización.

## Desarrollo

El sistema está programado en Kotlin, y utiliza las siguientes herramientas:

* Base de datos: mariadb para ambientes productivos y H2 para desarrollo
* Acceso a datos: [exposed](https://github.com/JetBrains/Exposed), una librería liviana de acceso a datos
* Web: spring mvc
* Telegram: [tehanu](https://git.rlab.be/seykron/tehanu), un bot de telegram extensible
* Configuración: [typesafe](https://github.com/lightbend/config)
* Rendering de vistas: [freemarker](https://freemarker.apache.org/)

### Organización del código

La aplicación utiliza maven como sistema de build. Como criterio general de desarrollo se está utilizando
[Domain Driven Design](https://es.wikipedia.org/wiki/Dise%C3%B1o_guiado_por_el_dominio), por lo que la aplicación
está separada en dos grandes capas: domain layer y application layer. El sistema contable es parte del domain layer. El
frontend y el bot son parte del application layer.

El código está organizando en los siguientes packages:

* *be.rlab.domino.application*: todo lo relacionado al application layer
* *be.rlab.domino.application.model*: modelo común a todo el application layer
* *be.rlab.domino.application.bot*: comandos del bot de Telegram
* *be.rlab.domino.application.web*: controllers de Spring MVC
* *be.rlab.domino.domain*: todo lo relacionado al domain layer
* *be.rlab.domino.domain.model*: entidades del dominio
* *be.rlab.domino.domain.persistence*: definición y mapeo de los objetos de exposed para persistencia
* *be.rlab.domino.config*: componentes para configurar e inicializar toda la aplicación
* *be.rlab.domino.util*: componentes de soporte para varias tareas

Los recursos están organizados en los siguientes packages:

* /db: scripts para inicializar la base de datos
* /public: contenido que se expone públicamente en la web
* /views: vistas de freemarker

### Ejecutar la aplicación

Domino tiene un servidor [netty](https://github.com/reactor/reactor-netty) embebido y corre como una aplicación
standalone. Para correrlo dentro del IDE:

* Setear las siguientes variables de entorno en tu IDE:

```.env
bot_access_token=access token del bot de Telegram que provee @BotFather
bot_handle=@HandleDelBot
bot_default_admin=id de ususario de telegram que es admin del bot
```

* Ejecutar la clase que inicializa la aplicación:
[be.rlab.domino.ApplicationKt](https://git.rlab.be/seykron/domino/blob/master/src/main/kotlin/be/rlab/domino/Application.kt)

Cuando se corre dentro del IDE la aplicación automáticamente crea la base de datos y las tablas en una base de datos H2.
La base de datos queda guardada en disco, por lo que los datos se mantienen en cada reinicio.

Para ejecutar la aplicación en el sistema operativo se utiliza [docker-compose](https://docs.docker.com/compose). Primero
hay que setear las mismas variables de entorno que se mencionan arriba pero en el
[archivo de configuración de ambiente](https://git.rlab.be/seykron/domino/blob/master/.env) de docker-compose. Luego de
buildear la aplicación se puede utilizar docker-compose para ejecutarla con una base de datos mariadb:

```shell script
$ mvn clean install
$ docker-compose up
```
